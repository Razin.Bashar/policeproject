package role;


public class RoleDTO
{
	public String roleName = "";
    public String description = "";
    public long ID;
    public boolean isDeleted;
    public long lastModificationTime;       
}