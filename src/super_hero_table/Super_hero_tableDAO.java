package super_hero_table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class Super_hero_tableDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"super_hero_table");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"super_hero_table");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(Super_hero_tableDTO super_hero_tableDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = super_hero_tableDTO.iD;
		// userDTO.userName = super_hero_tableDTO.email;
		// userDTO.fullName = super_hero_tableDTO.name;
		// userDTO.password = super_hero_tableDTO.password;
		// userDTO.phoneNo = super_hero_tableDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = super_hero_tableDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(Super_hero_tableDTO super_hero_tableDTO, UserDTO userDTO)
	{
		// userDTO.ID = super_hero_tableDTO.iD;
		// userDTO.fullName = super_hero_tableDTO.name;
		// userDTO.phoneNo = super_hero_tableDTO.phone;
		// userDTO.mailAddress = super_hero_tableDTO.email;

		return userDTO;
	}
		
		
	
	public void addSuper_hero_table(Super_hero_tableDTO super_hero_tableDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			super_hero_tableDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Super_hero_table");

			String sql = "INSERT INTO super_hero_table";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_Details";
			sql += ", ";
			sql += "image";
			sql += ", ";
			sql += "bio_data_file";
			sql += ", ";
			sql += "other_database";
			sql += ", ";
			sql += "education";
			sql += ", ";
			sql += "passing_year";
			sql += ", ";
			sql += "is_strong";
			sql += ", ";
			sql += "eye_color";
			sql += ", ";
			sql += "has_a_radio";
			sql += ", ";
			sql += "precise_weight";
			sql += ", ";
			sql += "mail_richtext";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(super_hero_tableDTO));
			// super_hero_tableDTO.iD = userDAO.getUserDTOByUsername(super_hero_tableDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + super_hero_tableDTO.iD + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.iD);
			//System.out.println("Setting object" + super_hero_tableDTO.password + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.password);
			//System.out.println("Setting object" + super_hero_tableDTO.firstName + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.firstName);
			//System.out.println("Setting object" + super_hero_tableDTO.lastName + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.lastName);
			//System.out.println("Setting object" + super_hero_tableDTO.gender + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.gender);
			//System.out.println("Setting object" + super_hero_tableDTO.religion + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.religion);
			//System.out.println("Setting object" + super_hero_tableDTO.dateOfBirth + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.dateOfBirth);
			//System.out.println("Setting object" + super_hero_tableDTO.address + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.address);
			//System.out.println("Setting object" + super_hero_tableDTO.addressDetails + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.addressDetails);
			//System.out.println("Setting object" + super_hero_tableDTO.image + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.image);
			//System.out.println("Setting object" + super_hero_tableDTO.bioDataFile + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.bioDataFile);
			//System.out.println("Setting object" + super_hero_tableDTO.otherDatabase + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.otherDatabase);
			//System.out.println("Setting object" + super_hero_tableDTO.education + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.education);
			//System.out.println("Setting object" + super_hero_tableDTO.passingYear + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.passingYear);
			//System.out.println("Setting object" + super_hero_tableDTO.isStrong + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.isStrong);
			//System.out.println("Setting object" + super_hero_tableDTO.eyeColor + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.eyeColor);
			//System.out.println("Setting object" + super_hero_tableDTO.hasARadio + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.hasARadio);
			//System.out.println("Setting object" + super_hero_tableDTO.preciseWeight + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.preciseWeight);
			//System.out.println("Setting object" + super_hero_tableDTO.mailRichtext + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.mailRichtext);
			//System.out.println("Setting object" + super_hero_tableDTO.isDeleted + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Super_hero_tableRepository.getInstance().reload(false);		
	}
	
	//need another getter for repository
	public Super_hero_tableDTO getSuper_hero_tableDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Super_hero_tableDTO super_hero_tableDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_Details";
			sql += ", ";
			sql += "image";
			sql += ", ";
			sql += "bio_data_file";
			sql += ", ";
			sql += "other_database";
			sql += ", ";
			sql += "education";
			sql += ", ";
			sql += "passing_year";
			sql += ", ";
			sql += "is_strong";
			sql += ", ";
			sql += "eye_color";
			sql += ", ";
			sql += "has_a_radio";
			sql += ", ";
			sql += "precise_weight";
			sql += ", ";
			sql += "mail_richtext";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM super_hero_table";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				super_hero_tableDTO = new Super_hero_tableDTO();

				super_hero_tableDTO.iD = rs.getLong("ID");
				super_hero_tableDTO.password = rs.getString("password");
				super_hero_tableDTO.firstName = rs.getString("first_name");
				super_hero_tableDTO.lastName = rs.getString("last_name");
				super_hero_tableDTO.gender = rs.getString("gender");
				super_hero_tableDTO.religion = rs.getString("religion");
				super_hero_tableDTO.dateOfBirth = rs.getLong("date_of_birth");
				super_hero_tableDTO.address = rs.getString("address");
				super_hero_tableDTO.addressDetails = rs.getString("address_Details");
				super_hero_tableDTO.image = rs.getString("image");
				super_hero_tableDTO.bioDataFile = rs.getString("bio_data_file");
				super_hero_tableDTO.otherDatabase = rs.getString("other_database");
				super_hero_tableDTO.education = rs.getString("education");
				super_hero_tableDTO.passingYear = rs.getString("passing_year");
				super_hero_tableDTO.isStrong = rs.getBoolean("is_strong");
				super_hero_tableDTO.eyeColor = rs.getString("eye_color");
				super_hero_tableDTO.hasARadio = rs.getBoolean("has_a_radio");
				super_hero_tableDTO.preciseWeight = rs.getDouble("precise_weight");
				super_hero_tableDTO.mailRichtext = rs.getString("mail_richtext");
				super_hero_tableDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return super_hero_tableDTO;
	}
	
	public void updateSuper_hero_table(Super_hero_tableDTO super_hero_tableDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE super_hero_table";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "password=?";
			sql += ", ";
			sql += "first_name=?";
			sql += ", ";
			sql += "last_name=?";
			sql += ", ";
			sql += "gender=?";
			sql += ", ";
			sql += "religion=?";
			sql += ", ";
			sql += "date_of_birth=?";
			sql += ", ";
			sql += "address=?";
			sql += ", ";
			sql += "address_Details=?";
			sql += ", ";
			sql += "image=?";
			sql += ", ";
			sql += "bio_data_file=?";
			sql += ", ";
			sql += "other_database=?";
			sql += ", ";
			sql += "education=?";
			sql += ", ";
			sql += "passing_year=?";
			sql += ", ";
			sql += "is_strong=?";
			sql += ", ";
			sql += "eye_color=?";
			sql += ", ";
			sql += "has_a_radio=?";
			sql += ", ";
			sql += "precise_weight=?";
			sql += ", ";
			sql += "mail_richtext=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + super_hero_tableDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + super_hero_tableDTO.iD + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.iD);
			//System.out.println("Setting object" + super_hero_tableDTO.password + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.password);
			//System.out.println("Setting object" + super_hero_tableDTO.firstName + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.firstName);
			//System.out.println("Setting object" + super_hero_tableDTO.lastName + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.lastName);
			//System.out.println("Setting object" + super_hero_tableDTO.gender + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.gender);
			//System.out.println("Setting object" + super_hero_tableDTO.religion + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.religion);
			//System.out.println("Setting object" + super_hero_tableDTO.dateOfBirth + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.dateOfBirth);
			//System.out.println("Setting object" + super_hero_tableDTO.address + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.address);
			//System.out.println("Setting object" + super_hero_tableDTO.addressDetails + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.addressDetails);
			//System.out.println("Setting object" + super_hero_tableDTO.image + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.image);
			//System.out.println("Setting object" + super_hero_tableDTO.bioDataFile + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.bioDataFile);
			//System.out.println("Setting object" + super_hero_tableDTO.otherDatabase + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.otherDatabase);
			//System.out.println("Setting object" + super_hero_tableDTO.education + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.education);
			//System.out.println("Setting object" + super_hero_tableDTO.passingYear + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.passingYear);
			//System.out.println("Setting object" + super_hero_tableDTO.isStrong + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.isStrong);
			//System.out.println("Setting object" + super_hero_tableDTO.eyeColor + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.eyeColor);
			//System.out.println("Setting object" + super_hero_tableDTO.hasARadio + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.hasARadio);
			//System.out.println("Setting object" + super_hero_tableDTO.preciseWeight + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.preciseWeight);
			//System.out.println("Setting object" + super_hero_tableDTO.mailRichtext + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.mailRichtext);
			//System.out.println("Setting object" + super_hero_tableDTO.isDeleted + " in index " + index);
			ps.setObject(index++,super_hero_tableDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(super_hero_tableDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(super_hero_tableDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Super_hero_tableRepository.getInstance().reload(false);
	}
	
	public void deleteSuper_hero_tableByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE super_hero_table";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<Super_hero_tableDTO> getSuper_hero_tableDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Super_hero_tableDTO super_hero_tableDTO = null;
		List<Super_hero_tableDTO> super_hero_tableDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_Details";
			sql += ", ";
			sql += "image";
			sql += ", ";
			sql += "bio_data_file";
			sql += ", ";
			sql += "other_database";
			sql += ", ";
			sql += "education";
			sql += ", ";
			sql += "passing_year";
			sql += ", ";
			sql += "is_strong";
			sql += ", ";
			sql += "eye_color";
			sql += ", ";
			sql += "has_a_radio";
			sql += ", ";
			sql += "precise_weight";
			sql += ", ";
			sql += "mail_richtext";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM super_hero_table";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			while(rs.next()){
				super_hero_tableDTO = new Super_hero_tableDTO();
				super_hero_tableDTO.iD = rs.getLong("ID");
				super_hero_tableDTO.password = rs.getString("password");
				super_hero_tableDTO.firstName = rs.getString("first_name");
				super_hero_tableDTO.lastName = rs.getString("last_name");
				super_hero_tableDTO.gender = rs.getString("gender");
				super_hero_tableDTO.religion = rs.getString("religion");
				super_hero_tableDTO.dateOfBirth = rs.getLong("date_of_birth");
				super_hero_tableDTO.address = rs.getString("address");
				super_hero_tableDTO.addressDetails = rs.getString("address_Details");
				super_hero_tableDTO.image = rs.getString("image");
				super_hero_tableDTO.bioDataFile = rs.getString("bio_data_file");
				super_hero_tableDTO.otherDatabase = rs.getString("other_database");
				super_hero_tableDTO.education = rs.getString("education");
				super_hero_tableDTO.passingYear = rs.getString("passing_year");
				super_hero_tableDTO.isStrong = rs.getBoolean("is_strong");
				super_hero_tableDTO.eyeColor = rs.getString("eye_color");
				super_hero_tableDTO.hasARadio = rs.getBoolean("has_a_radio");
				super_hero_tableDTO.preciseWeight = rs.getDouble("precise_weight");
				super_hero_tableDTO.mailRichtext = rs.getString("mail_richtext");
				super_hero_tableDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = super_hero_tableDTO.iD;
				while(i < super_hero_tableDTOList.size() && super_hero_tableDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				super_hero_tableDTOList.add(i,  super_hero_tableDTO);
				//super_hero_tableDTOList.add(super_hero_tableDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return super_hero_tableDTOList;
	}
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getSuper_hero_tableDTOByColumn(filter);
	}
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getSuper_hero_tableDTOByColumn(filter);
	}
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getSuper_hero_tableDTOByColumn(filter);
	}
		
	
	
	public List<Super_hero_tableDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Super_hero_tableDTO super_hero_tableDTO = null;
		List<Super_hero_tableDTO> super_hero_tableDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return super_hero_tableDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_Details";
			sql += ", ";
			sql += "image";
			sql += ", ";
			sql += "bio_data_file";
			sql += ", ";
			sql += "other_database";
			sql += ", ";
			sql += "education";
			sql += ", ";
			sql += "passing_year";
			sql += ", ";
			sql += "is_strong";
			sql += ", ";
			sql += "eye_color";
			sql += ", ";
			sql += "has_a_radio";
			sql += ", ";
			sql += "precise_weight";
			sql += ", ";
			sql += "mail_richtext";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM super_hero_table";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				super_hero_tableDTO = new Super_hero_tableDTO();
				super_hero_tableDTO.iD = rs.getLong("ID");
				super_hero_tableDTO.password = rs.getString("password");
				super_hero_tableDTO.firstName = rs.getString("first_name");
				super_hero_tableDTO.lastName = rs.getString("last_name");
				super_hero_tableDTO.gender = rs.getString("gender");
				super_hero_tableDTO.religion = rs.getString("religion");
				super_hero_tableDTO.dateOfBirth = rs.getLong("date_of_birth");
				super_hero_tableDTO.address = rs.getString("address");
				super_hero_tableDTO.addressDetails = rs.getString("address_Details");
				super_hero_tableDTO.image = rs.getString("image");
				super_hero_tableDTO.bioDataFile = rs.getString("bio_data_file");
				super_hero_tableDTO.otherDatabase = rs.getString("other_database");
				super_hero_tableDTO.education = rs.getString("education");
				super_hero_tableDTO.passingYear = rs.getString("passing_year");
				super_hero_tableDTO.isStrong = rs.getBoolean("is_strong");
				super_hero_tableDTO.eyeColor = rs.getString("eye_color");
				super_hero_tableDTO.hasARadio = rs.getBoolean("has_a_radio");
				super_hero_tableDTO.preciseWeight = rs.getDouble("precise_weight");
				super_hero_tableDTO.mailRichtext = rs.getString("mail_richtext");
				super_hero_tableDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + super_hero_tableDTO);
				//super_hero_tableDTOList.add(super_hero_tableDTO);
				int i = 0;
				long primaryKey = super_hero_tableDTO.iD;
				while(i < super_hero_tableDTOList.size() && super_hero_tableDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				super_hero_tableDTOList.add(i,  super_hero_tableDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return super_hero_tableDTOList;
	
	}
	
	

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM super_hero_table";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<Super_hero_tableDTO> getAllSuper_hero_table (boolean isFirstReload)
    {
		List<Super_hero_tableDTO> super_hero_tableDTOList = new ArrayList<>();

		String sql = "SELECT * FROM super_hero_table";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Super_hero_tableDTO super_hero_tableDTO = new Super_hero_tableDTO();
				super_hero_tableDTO.iD = rs.getLong("ID");
				super_hero_tableDTO.password = rs.getString("password");
				super_hero_tableDTO.firstName = rs.getString("first_name");
				super_hero_tableDTO.lastName = rs.getString("last_name");
				super_hero_tableDTO.gender = rs.getString("gender");
				super_hero_tableDTO.religion = rs.getString("religion");
				super_hero_tableDTO.dateOfBirth = rs.getLong("date_of_birth");
				super_hero_tableDTO.address = rs.getString("address");
				super_hero_tableDTO.addressDetails = rs.getString("address_Details");
				super_hero_tableDTO.image = rs.getString("image");
				super_hero_tableDTO.bioDataFile = rs.getString("bio_data_file");
				super_hero_tableDTO.otherDatabase = rs.getString("other_database");
				super_hero_tableDTO.education = rs.getString("education");
				super_hero_tableDTO.passingYear = rs.getString("passing_year");
				super_hero_tableDTO.isStrong = rs.getBoolean("is_strong");
				super_hero_tableDTO.eyeColor = rs.getString("eye_color");
				super_hero_tableDTO.hasARadio = rs.getBoolean("has_a_radio");
				super_hero_tableDTO.preciseWeight = rs.getDouble("precise_weight");
				super_hero_tableDTO.mailRichtext = rs.getString("mail_richtext");
				super_hero_tableDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = super_hero_tableDTO.iD;
				while(i < super_hero_tableDTOList.size() && super_hero_tableDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				super_hero_tableDTOList.add(i,  super_hero_tableDTO);
				//super_hero_tableDTOList.add(super_hero_tableDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return super_hero_tableDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM super_hero_table";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		try{

			String sql = "SELECT ID FROM super_hero_table";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Super_hero_tableMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception 
	{
		
		ArrayList<long[]> idList = (ArrayList<long[]>)recordIDs;
		Connection connection = null;
		PreparedStatement ps = null;
		Super_hero_tableDTO super_hero_tableDTO = null;
		List<Super_hero_tableDTO> super_hero_tableDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return super_hero_tableDTOList;
		}

		try{

			String sql = "SELECT ";
			Iterator it = Super_hero_tableMAPS.GetInstance().java_SQL_map.entrySet().iterator();
			int i = 0;
			while(it.hasNext())
			{
				if( i > 0)
	        	{
					sql+= " ,  ";
	        	}
				Map.Entry pair = (Map.Entry)it.next();
				sql+= pair.getKey();
				i ++;
			}		
			sql += " FROM ";
			
			for(i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" inner join ");
				}
				sql += table_names.get(i) + " ";
			}
			
			sql += " WHERE ";
			
			for(i = 0; i < idList.size(); i ++)
			{
				long[] ids = idList.get(i);
				if(i > 0)
				{
					sql += (" OR ");
				}
				sql += " ( ";
				for(int j = 0; j < table_names.size(); j ++)
				{
					if(j > 0)
					{
						sql += (" AND ");
					}
					sql += table_names.get(j) + ".ID = " + ids[j] + " AND " + table_names.get(j) + ".isDeleted = false";
				}
				sql += " ) ";
			}
			
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				super_hero_tableDTO = new Super_hero_tableDTO();
				super_hero_tableDTO.iD = rs.getLong("ID");
				super_hero_tableDTO.password = rs.getString("password");
				super_hero_tableDTO.firstName = rs.getString("first_name");
				super_hero_tableDTO.lastName = rs.getString("last_name");
				super_hero_tableDTO.gender = rs.getString("gender");
				super_hero_tableDTO.religion = rs.getString("religion");
				super_hero_tableDTO.dateOfBirth = rs.getLong("date_of_birth");
				super_hero_tableDTO.address = rs.getString("address");
				super_hero_tableDTO.addressDetails = rs.getString("address_Details");
				super_hero_tableDTO.image = rs.getString("image");
				super_hero_tableDTO.bioDataFile = rs.getString("bio_data_file");
				super_hero_tableDTO.otherDatabase = rs.getString("other_database");
				super_hero_tableDTO.education = rs.getString("education");
				super_hero_tableDTO.passingYear = rs.getString("passing_year");
				super_hero_tableDTO.isStrong = rs.getBoolean("is_strong");
				super_hero_tableDTO.eyeColor = rs.getString("eye_color");
				super_hero_tableDTO.hasARadio = rs.getBoolean("has_a_radio");
				super_hero_tableDTO.preciseWeight = rs.getDouble("precise_weight");
				super_hero_tableDTO.mailRichtext = rs.getString("mail_richtext");
				super_hero_tableDTO.isDeleted = rs.getBoolean("isDeleted");
				i = 0;
				long primaryKey = super_hero_tableDTO.iD;
				while(i < super_hero_tableDTOList.size() && super_hero_tableDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				super_hero_tableDTOList.add(i,  super_hero_tableDTO);

			}		
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return super_hero_tableDTOList;
	}
	
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		long[] ids = new long[table_names.size()];
		List<long[]> idList = new ArrayList<long[]>();
		Connection connection = null;
		PreparedStatement ps = null;
		

		try{

			String sql = "SELECT ";
			for(int i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" , ");
				}
				sql += table_names.get(i) + ".ID as " + table_names.get(i)+ "_ID";
			}
			sql +=  table_names.get(0) + ".ID ";
			sql += " FROM ";
			
			for(int i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" INNER JOIN ");
				}
				sql += table_names.get(i) + " ";
				sql += "ON " + table_names.get(0) + "." + table_names.get(i) + "_ID" +  " = " + table_names.get(i) + ".ID";
			}
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Super_hero_tableMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Super_hero_tableMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			
			for(int j = 0; j < table_names.size(); j ++)
			{
				if(j > 0)
				{
					sql += (" AND ");
				}
				sql += table_names.get(j) + ".isDeleted = false";
			}				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				for(int j = 0; j < table_names.size(); j ++)
				{
					ids[j] = rs.getLong(table_names.get(j) + "_ID");
				}
				idList.add(ids);
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
		
}
	