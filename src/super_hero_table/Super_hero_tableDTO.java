package super_hero_table;
import java.util.*; 


public class Super_hero_tableDTO {

	public long iD = 0;
    public String password = "";
    public String firstName = "";
    public String lastName = "";
    public String gender = "";
    public String religion = "";
	public long dateOfBirth = 0;
    public String address = "";
    public String addressDetails = "";
    public String image = "";
    public String bioDataFile = "";
    public String otherDatabase = "";
    public String education = "";
    public String passingYear = "";
	public boolean isStrong = false;
    public String eyeColor = "";
	public boolean hasARadio = false;
	public double preciseWeight = 0;
    public String mailRichtext = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$Super_hero_tableDTO[" +
            " iD = " + iD +
            " password = " + password +
            " firstName = " + firstName +
            " lastName = " + lastName +
            " gender = " + gender +
            " religion = " + religion +
            " dateOfBirth = " + dateOfBirth +
            " address = " + address +
            " addressDetails = " + addressDetails +
            " image = " + image +
            " bioDataFile = " + bioDataFile +
            " otherDatabase = " + otherDatabase +
            " education = " + education +
            " passingYear = " + passingYear +
            " isStrong = " + isStrong +
            " eyeColor = " + eyeColor +
            " hasARadio = " + hasARadio +
            " preciseWeight = " + preciseWeight +
            " mailRichtext = " + mailRichtext +
            " isDeleted = " + isDeleted +
            "]";
    }

}