package super_hero_table;
import java.util.*; 


public class Super_hero_tableMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	
	private static Super_hero_tableMAPS self = null;
	
	private Super_hero_tableMAPS()
	{
		
		java_type_map.put("first_name".toLowerCase(), "String");
		java_type_map.put("last_name".toLowerCase(), "String");
		java_type_map.put("gender".toLowerCase(), "String");
		java_type_map.put("religion".toLowerCase(), "String");
		java_type_map.put("date_of_birth".toLowerCase(), "Long");
		java_type_map.put("address".toLowerCase(), "String");
		java_type_map.put("address_Details".toLowerCase(), "String");
		java_type_map.put("image".toLowerCase(), "String");
		java_type_map.put("bio_data_file".toLowerCase(), "String");
		java_type_map.put("other_database".toLowerCase(), "String");
		java_type_map.put("education".toLowerCase(), "String");
		java_type_map.put("passing_year".toLowerCase(), "String");
		java_type_map.put("is_strong".toLowerCase(), "Boolean");
		java_type_map.put("eye_color".toLowerCase(), "String");
		java_type_map.put("has_a_radio".toLowerCase(), "Boolean");
		java_type_map.put("precise_weight".toLowerCase(), "Double");
		java_type_map.put("mail_richtext".toLowerCase(), "String");

		java_custom_search_map.put("first_name".toLowerCase(), "String");
		java_custom_search_map.put("last_name".toLowerCase(), "String");
		java_custom_search_map.put("gender".toLowerCase(), "String");
		java_custom_search_map.put("religion".toLowerCase(), "String");
		java_custom_search_map.put("date_of_birth".toLowerCase(), "Long");
		java_custom_search_map.put("address".toLowerCase(), "String");
		java_custom_search_map.put("address_Details".toLowerCase(), "String");
		java_custom_search_map.put("image".toLowerCase(), "String");
		java_custom_search_map.put("bio_data_file".toLowerCase(), "String");
		java_custom_search_map.put("other_database".toLowerCase(), "String");
		java_custom_search_map.put("education".toLowerCase(), "String");
		java_custom_search_map.put("passing_year".toLowerCase(), "String");
		java_custom_search_map.put("is_strong".toLowerCase(), "Boolean");
		java_custom_search_map.put("eye_color".toLowerCase(), "String");
		java_custom_search_map.put("has_a_radio".toLowerCase(), "Boolean");
		java_custom_search_map.put("precise_weight".toLowerCase(), "Double");
		java_custom_search_map.put("mail_richtext".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("password".toLowerCase(), "password".toLowerCase());
		java_DTO_map.put("firstName".toLowerCase(), "firstName".toLowerCase());
		java_DTO_map.put("lastName".toLowerCase(), "lastName".toLowerCase());
		java_DTO_map.put("gender".toLowerCase(), "gender".toLowerCase());
		java_DTO_map.put("religion".toLowerCase(), "religion".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("address".toLowerCase(), "address".toLowerCase());
		java_DTO_map.put("addressDetails".toLowerCase(), "addressDetails".toLowerCase());
		java_DTO_map.put("image".toLowerCase(), "image".toLowerCase());
		java_DTO_map.put("bioDataFile".toLowerCase(), "bioDataFile".toLowerCase());
		java_DTO_map.put("otherDatabase".toLowerCase(), "otherDatabase".toLowerCase());
		java_DTO_map.put("education".toLowerCase(), "education".toLowerCase());
		java_DTO_map.put("passingYear".toLowerCase(), "passingYear".toLowerCase());
		java_DTO_map.put("isStrong".toLowerCase(), "isStrong".toLowerCase());
		java_DTO_map.put("eyeColor".toLowerCase(), "eyeColor".toLowerCase());
		java_DTO_map.put("hasARadio".toLowerCase(), "hasARadio".toLowerCase());
		java_DTO_map.put("preciseWeight".toLowerCase(), "preciseWeight".toLowerCase());
		java_DTO_map.put("mailRichtext".toLowerCase(), "mailRichtext".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("first_name".toLowerCase(), "firstName".toLowerCase());
		java_SQL_map.put("last_name".toLowerCase(), "lastName".toLowerCase());
		java_SQL_map.put("gender".toLowerCase(), "gender".toLowerCase());
		java_SQL_map.put("religion".toLowerCase(), "religion".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("address".toLowerCase(), "address".toLowerCase());
		java_SQL_map.put("address_Details".toLowerCase(), "addressDetails".toLowerCase());
		java_SQL_map.put("image".toLowerCase(), "image".toLowerCase());
		java_SQL_map.put("bio_data_file".toLowerCase(), "bioDataFile".toLowerCase());
		java_SQL_map.put("other_database".toLowerCase(), "otherDatabase".toLowerCase());
		java_SQL_map.put("education".toLowerCase(), "education".toLowerCase());
		java_SQL_map.put("passing_year".toLowerCase(), "passingYear".toLowerCase());
		java_SQL_map.put("is_strong".toLowerCase(), "isStrong".toLowerCase());
		java_SQL_map.put("eye_color".toLowerCase(), "eyeColor".toLowerCase());
		java_SQL_map.put("has_a_radio".toLowerCase(), "hasARadio".toLowerCase());
		java_SQL_map.put("precise_weight".toLowerCase(), "preciseWeight".toLowerCase());
		java_SQL_map.put("mail_richtext".toLowerCase(), "mailRichtext".toLowerCase());
			
	}
	
	public static Super_hero_tableMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Super_hero_tableMAPS ();
		}
		return self;
	}
	

}