package super_hero_table;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import java.io.*;
import javax.servlet.http.*;
import java.util.UUID;

import super_hero_table.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;


/**
 * Servlet implementation class Super_hero_tableServlet
 */
@WebServlet("/Super_hero_tableServlet")
@MultipartConfig
public class Super_hero_tableServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Super_hero_tableServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Super_hero_tableServlet() 
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO.userID);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_UPDATE))
				{
					getSuper_hero_table(request, response);
				}
				else
				{
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
				
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_SEARCH))
				{
					searchSuper_hero_table(request, response);
				}
				else
				{
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
				
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("super_hero_table/super_hero_tableEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}
	
	public void ReadXLs(HttpServletRequest request, String fileName) throws IOException 
	{
		String path = getServletContext().getRealPath("/img2/");
	    File excelFile = new File(path + File.separator
                + fileName);
	    FileInputStream fis = new FileInputStream(excelFile);


	    XSSFWorkbook workbook = new XSSFWorkbook(fis);
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    Iterator<Row> rowIt = sheet.iterator();	    
	    ArrayList<String> Rows = new ArrayList<String>();
	    
	    
	    Super_hero_tableDTO super_hero_tableDTO;

		String failureMessage = "";
	    int i = 0;
	    while(rowIt.hasNext()) 
	    {
			Row row = rowIt.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			super_hero_tableDTO = new Super_hero_tableDTO();

			int j = 0;
			
			try
			{
				while (cellIterator.hasNext()) 
				{
					Cell cell = cellIterator.next();
					System.out.print("CELLNAME: " + cell.toString() + " ");
					
					
					if(i == 0)
					{
						Rows.add(cell.toString());
					}
					else
					{
						String rowName = Rows.get(j);
						if(rowName == null || rowName.equalsIgnoreCase(""))
						{
							break;
						}
						if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("iD"))
						{
							super_hero_tableDTO.iD = (long)Double.parseDouble(cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("password"))
						{
							super_hero_tableDTO.password = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("firstName"))
						{
							super_hero_tableDTO.firstName = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("lastName"))
						{
							super_hero_tableDTO.lastName = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("gender"))
						{
							super_hero_tableDTO.gender = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("religion"))
						{
							super_hero_tableDTO.religion = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("dateOfBirth"))
						{
							super_hero_tableDTO.dateOfBirth = (long)Double.parseDouble(cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("address"))
						{
							super_hero_tableDTO.address = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("addressDetails"))
						{
							super_hero_tableDTO.addressDetails = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("image"))
						{
							super_hero_tableDTO.image = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("bioDataFile"))
						{
							super_hero_tableDTO.bioDataFile = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("otherDatabase"))
						{
							super_hero_tableDTO.otherDatabase = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("education"))
						{
							super_hero_tableDTO.education = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("passingYear"))
						{
							super_hero_tableDTO.passingYear = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("isStrong"))
						{
							super_hero_tableDTO.isStrong = Boolean.parseBoolean(cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("eyeColor"))
						{
							super_hero_tableDTO.eyeColor = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("hasARadio"))
						{
							super_hero_tableDTO.hasARadio = Boolean.parseBoolean(cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("preciseWeight"))
						{
							super_hero_tableDTO.preciseWeight = (double)Double.parseDouble(cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("mailRichtext"))
						{
							super_hero_tableDTO.mailRichtext = (cell.toString());
						}
						else if(Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName) != null && Super_hero_tableMAPS.GetInstance().java_DTO_map.get(rowName).equalsIgnoreCase("isDeleted"))
						{
							super_hero_tableDTO.isDeleted = Boolean.parseBoolean(cell.toString());
						}
					}
					j ++;
						
				  }
				  
			  
				  if(i != 0)
				  {
					  System.out.println("INSERTING: " + super_hero_tableDTO);
					  Super_hero_tableDAO super_hero_tableDAO = new Super_hero_tableDAO();
					  try 
					  {
						super_hero_tableDAO.addSuper_hero_table(super_hero_tableDTO);
						
					  }
					  catch (Exception e) 
					  {
							e.printStackTrace();			
					  }
				  }
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				failureMessage += (i + 1) + " ";
			}
			i ++;

			System.out.println();
	    }
		if(failureMessage.equalsIgnoreCase(""))
		{
			failureMessage = " Successfully parsed all rows";
		}
		else
		{
			failureMessage = " Failed on rows: " + failureMessage;
		}
		request.setAttribute("failureMessage", failureMessage);

	    workbook.close();
	    fis.close();
	  }
	
	private void UploadFile(HttpServletRequest request, HttpServletResponse response, Part filePart, String fileName)
	{

	    OutputStream out = null;
	    InputStream filecontent = null;

	    String path = getServletContext().getRealPath("/img2/");
	    
	    File dir=new File(path);
	    
	    if(!dir.exists())
        {
	    	dir.mkdir();
	    	System.out.println("created directory " + path);
        }

	    try 
		{
	        out = new FileOutputStream(new File(path + File.separator
	                + fileName));
	        filecontent = filePart.getInputStream();

	        int read = 0;
	        final byte[] bytes = new byte[1024];

	        while ((read = filecontent.read(bytes)) != -1) 
			{
	            out.write(bytes, 0, read);
	        }
	        System.out.println("New file " + fileName + " created at " + path);

	    }
		catch (IOException fne) 
		{
	    	System.out.println("You either did not specify a file to upload or are "
	                + "trying to upload a file to a protected or nonexistent "
	                + "location.");
	    	System.out.println("ERROR: " + fne.getMessage());
	    }
		finally 
		{
	        if (out != null) 
			{
	            try 
				{
					out.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
	        }
	        if (filecontent != null) 
			{
	            try 
				{
					filecontent.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
	        }	       
	    }		  
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO.userID);
		System.out.println("doPost");
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_ADD))
				{
					System.out.println("going to  addSuper_hero_table ");
					addSuper_hero_table(request, response, true);
				}
				else
				{
					System.out.println("Not going to  addSuper_hero_table ");
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_UPDATE))
				{
					addSuper_hero_table(request, response, false);
				}
				else
				{
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteSuper_hero_table(request, response);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_HERO_TABLE_SEARCH))
				{
					searchSuper_hero_table(request, response);
				}
				else
				{
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void addSuper_hero_table(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addSuper_hero_table");
			Super_hero_tableDAO super_hero_tableDAO = new Super_hero_tableDAO();
			Super_hero_tableDTO super_hero_tableDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				super_hero_tableDTO = new Super_hero_tableDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				super_hero_tableDTO = super_hero_tableDAO.getSuper_hero_tableDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("iD");
			System.out.println("iD = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.iD = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("password");
			System.out.println("password = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.password = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("firstName");
			System.out.println("firstName = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.firstName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("lastName");
			System.out.println("lastName = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.lastName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("gender");
			System.out.println("gender = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.gender = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("religion");
			System.out.println("religion = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.religion = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("dateOfBirth");
			System.out.println("dateOfBirth = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.dateOfBirth = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("address");
			System.out.println("address = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				
				{
					StringTokenizer tok3=new StringTokenizer(Value, ":");
					int i = 0;
					String addressDetails = "", address_id = "";
					while(tok3.hasMoreElements())
					{
						if(i == 0)
						{
							address_id = tok3.nextElement() + "";
						}
						else if(i == 1)
						{
							addressDetails = tok3.nextElement() + "";
						}
						i ++;
					}
					super_hero_tableDTO.address = address_id + ":" + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id))
					+ ":" + addressDetails;
				}
				
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("addressDetails");
			System.out.println("addressDetails = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.addressDetails = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Part filePart_image =  request.getPart("image");
			Value = getFileName(filePart_image);
			System.out.println("image = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value.toLowerCase().endsWith(".jpg") || Value.toLowerCase().endsWith(".png") 
					|| Value.toLowerCase().endsWith(".gif") || Value.toLowerCase().endsWith(".bmp") || Value.toLowerCase().endsWith(".ico"))
				{
					String FileName = FileNamePrefix + "_" + "image" + "_" + Value;
					super_hero_tableDTO.image = (FileName);
					UploadFile(request, response, filePart_image, FileName);
				}
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Part filePart_bioDataFile =  request.getPart("bioDataFile");
			Value = getFileName(filePart_bioDataFile);
			System.out.println("bioDataFile = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{					
					String FileName = FileNamePrefix + "_" + "bioDataFile" + "_" + Value;
					super_hero_tableDTO.bioDataFile = (FileName);
					UploadFile(request, response, filePart_bioDataFile, FileName);
				}	
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Part filePart_otherDatabase =  request.getPart("otherDatabase");
			Value = getFileName(filePart_otherDatabase);
			System.out.println("otherDatabase = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{					
					String FileName = FileNamePrefix + "_" + "otherDatabase" + "_" + Value;
					super_hero_tableDTO.otherDatabase = (FileName);
					UploadFile(request, response, filePart_otherDatabase, FileName);
					ReadXLs(request, FileName);
				}	
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("education");
			System.out.println("education = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.education = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("passingYear");
			System.out.println("passingYear = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.passingYear = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("isStrong");
			System.out.println("isStrong = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.isStrong = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("eyeColor");
			System.out.println("eyeColor = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.eyeColor = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("hasARadio");
			System.out.println("hasARadio = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.hasARadio = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("preciseWeight");
			System.out.println("preciseWeight = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.preciseWeight = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("mailRichtext");
			System.out.println("mailRichtext = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.mailRichtext = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("isDeleted");
			System.out.println("isDeleted = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				super_hero_tableDTO.isDeleted = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addSuper_hero_table dto = " + super_hero_tableDTO);
			
			if(addFlag == true)
			{
				super_hero_tableDAO.addSuper_hero_table(super_hero_tableDTO);
			}
			else
			{
				super_hero_tableDAO.updateSuper_hero_table(super_hero_tableDTO);
			}
			
			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getSuper_hero_table(request, response);
			}
			else
			{
				response.sendRedirect("Super_hero_tableServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			logger.debug(e);
		}
	}

	private void deleteSuper_hero_table(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("###DELETING " + IDsToDelete[i]);				
				new Super_hero_tableDAO().deleteSuper_hero_tableByID(id);
			}			
		}
		catch (Exception ex) 
		{
			logger.debug(ex);
		}
		response.sendRedirect("Super_hero_tableServlet?actionType=search");
	}

	private void getSuper_hero_table(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in getSuper_hero_table");
		Super_hero_tableDTO super_hero_tableDTO = null;
		try 
		{
			super_hero_tableDTO = new Super_hero_tableDAO().getSuper_hero_tableDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", super_hero_tableDTO.iD);
			request.setAttribute("super_hero_tableDTO",super_hero_tableDTO);
			
			String URL= "";
			
			String inPlaceEdit = (String)request.getParameter("inplaceedit");
			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "super_hero_table/super_hero_tableInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "super_hero_table/super_hero_tableSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				URL = "super_hero_table/super_hero_tableEdit.jsp?actionType=edit";
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchSuper_hero_table(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in  searchSuper_hero_table 1");
        Super_hero_tableDAO super_hero_tableDAO = new Super_hero_tableDAO();
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		String ajax = (String)request.getParameter("ajax");
		boolean hasAjax = false;
		if(ajax != null && !ajax.equalsIgnoreCase(""))
		{
			hasAjax = true;
		}
		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_SUPER_HERO_TABLE, request, super_hero_tableDAO, SessionConstants.VIEW_SUPER_HERO_TABLE, SessionConstants.SEARCH_SUPER_HERO_TABLE);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to super_hero_table/super_hero_tableSearch.jsp");
        	rd = request.getRequestDispatcher("super_hero_table/super_hero_tableSearch.jsp");
        }
        else
        {
        	System.out.println("Going to super_hero_table/super_hero_tableSearchForm.jsp");
        	rd = request.getRequestDispatcher("super_hero_table/super_hero_tableSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

