package super_hero_table;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Super_hero_tableRepository implements Repository {
	Super_hero_tableDAO super_hero_tableDAO = new Super_hero_tableDAO();
	
	
	static Logger logger = Logger.getLogger(Super_hero_tableRepository.class);
	Map<Long, Super_hero_tableDTO>mapOfSuper_hero_tableDTOToiD;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTopassword;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTofirstName;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTolastName;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTogender;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToreligion;
	Map<Long, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTodateOfBirth;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToaddress;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToaddressDetails;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToimage;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTobioDataFile;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTootherDatabase;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToeducation;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTopassingYear;
	Map<Boolean, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToisStrong;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOToeyeColor;
	Map<Boolean, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTohasARadio;
	Map<Double, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTopreciseWeight;
	Map<String, Set<Super_hero_tableDTO> >mapOfSuper_hero_tableDTOTomailRichtext;


	static Super_hero_tableRepository instance = null;  
	private Super_hero_tableRepository(){
		mapOfSuper_hero_tableDTOToiD = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTopassword = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTofirstName = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTolastName = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTogender = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToreligion = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToaddress = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToaddressDetails = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToimage = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTobioDataFile = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTootherDatabase = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToeducation = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTopassingYear = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToisStrong = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOToeyeColor = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTohasARadio = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTopreciseWeight = new ConcurrentHashMap<>();
		mapOfSuper_hero_tableDTOTomailRichtext = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Super_hero_tableRepository getInstance(){
		if (instance == null){
			instance = new Super_hero_tableRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Super_hero_tableDTO> super_hero_tableDTOs = super_hero_tableDAO.getAllSuper_hero_table(reloadAll);
			for(Super_hero_tableDTO super_hero_tableDTO : super_hero_tableDTOs) {
				Super_hero_tableDTO oldSuper_hero_tableDTO = getSuper_hero_tableDTOByID(super_hero_tableDTO.iD);
				if( oldSuper_hero_tableDTO != null ) {
					mapOfSuper_hero_tableDTOToiD.remove(oldSuper_hero_tableDTO.iD);
				
					if(mapOfSuper_hero_tableDTOTopassword.containsKey(oldSuper_hero_tableDTO.password)) {
						mapOfSuper_hero_tableDTOTopassword.get(oldSuper_hero_tableDTO.password).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTopassword.get(oldSuper_hero_tableDTO.password).isEmpty()) {
						mapOfSuper_hero_tableDTOTopassword.remove(oldSuper_hero_tableDTO.password);
					}
					
					if(mapOfSuper_hero_tableDTOTofirstName.containsKey(oldSuper_hero_tableDTO.firstName)) {
						mapOfSuper_hero_tableDTOTofirstName.get(oldSuper_hero_tableDTO.firstName).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTofirstName.get(oldSuper_hero_tableDTO.firstName).isEmpty()) {
						mapOfSuper_hero_tableDTOTofirstName.remove(oldSuper_hero_tableDTO.firstName);
					}
					
					if(mapOfSuper_hero_tableDTOTolastName.containsKey(oldSuper_hero_tableDTO.lastName)) {
						mapOfSuper_hero_tableDTOTolastName.get(oldSuper_hero_tableDTO.lastName).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTolastName.get(oldSuper_hero_tableDTO.lastName).isEmpty()) {
						mapOfSuper_hero_tableDTOTolastName.remove(oldSuper_hero_tableDTO.lastName);
					}
					
					if(mapOfSuper_hero_tableDTOTogender.containsKey(oldSuper_hero_tableDTO.gender)) {
						mapOfSuper_hero_tableDTOTogender.get(oldSuper_hero_tableDTO.gender).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTogender.get(oldSuper_hero_tableDTO.gender).isEmpty()) {
						mapOfSuper_hero_tableDTOTogender.remove(oldSuper_hero_tableDTO.gender);
					}
					
					if(mapOfSuper_hero_tableDTOToreligion.containsKey(oldSuper_hero_tableDTO.religion)) {
						mapOfSuper_hero_tableDTOToreligion.get(oldSuper_hero_tableDTO.religion).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToreligion.get(oldSuper_hero_tableDTO.religion).isEmpty()) {
						mapOfSuper_hero_tableDTOToreligion.remove(oldSuper_hero_tableDTO.religion);
					}
					
					if(mapOfSuper_hero_tableDTOTodateOfBirth.containsKey(oldSuper_hero_tableDTO.dateOfBirth)) {
						mapOfSuper_hero_tableDTOTodateOfBirth.get(oldSuper_hero_tableDTO.dateOfBirth).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTodateOfBirth.get(oldSuper_hero_tableDTO.dateOfBirth).isEmpty()) {
						mapOfSuper_hero_tableDTOTodateOfBirth.remove(oldSuper_hero_tableDTO.dateOfBirth);
					}
					
					if(mapOfSuper_hero_tableDTOToaddress.containsKey(oldSuper_hero_tableDTO.address)) {
						mapOfSuper_hero_tableDTOToaddress.get(oldSuper_hero_tableDTO.address).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToaddress.get(oldSuper_hero_tableDTO.address).isEmpty()) {
						mapOfSuper_hero_tableDTOToaddress.remove(oldSuper_hero_tableDTO.address);
					}
					
					if(mapOfSuper_hero_tableDTOToaddressDetails.containsKey(oldSuper_hero_tableDTO.addressDetails)) {
						mapOfSuper_hero_tableDTOToaddressDetails.get(oldSuper_hero_tableDTO.addressDetails).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToaddressDetails.get(oldSuper_hero_tableDTO.addressDetails).isEmpty()) {
						mapOfSuper_hero_tableDTOToaddressDetails.remove(oldSuper_hero_tableDTO.addressDetails);
					}
					
					if(mapOfSuper_hero_tableDTOToimage.containsKey(oldSuper_hero_tableDTO.image)) {
						mapOfSuper_hero_tableDTOToimage.get(oldSuper_hero_tableDTO.image).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToimage.get(oldSuper_hero_tableDTO.image).isEmpty()) {
						mapOfSuper_hero_tableDTOToimage.remove(oldSuper_hero_tableDTO.image);
					}
					
					if(mapOfSuper_hero_tableDTOTobioDataFile.containsKey(oldSuper_hero_tableDTO.bioDataFile)) {
						mapOfSuper_hero_tableDTOTobioDataFile.get(oldSuper_hero_tableDTO.bioDataFile).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTobioDataFile.get(oldSuper_hero_tableDTO.bioDataFile).isEmpty()) {
						mapOfSuper_hero_tableDTOTobioDataFile.remove(oldSuper_hero_tableDTO.bioDataFile);
					}
					
					if(mapOfSuper_hero_tableDTOTootherDatabase.containsKey(oldSuper_hero_tableDTO.otherDatabase)) {
						mapOfSuper_hero_tableDTOTootherDatabase.get(oldSuper_hero_tableDTO.otherDatabase).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTootherDatabase.get(oldSuper_hero_tableDTO.otherDatabase).isEmpty()) {
						mapOfSuper_hero_tableDTOTootherDatabase.remove(oldSuper_hero_tableDTO.otherDatabase);
					}
					
					if(mapOfSuper_hero_tableDTOToeducation.containsKey(oldSuper_hero_tableDTO.education)) {
						mapOfSuper_hero_tableDTOToeducation.get(oldSuper_hero_tableDTO.education).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToeducation.get(oldSuper_hero_tableDTO.education).isEmpty()) {
						mapOfSuper_hero_tableDTOToeducation.remove(oldSuper_hero_tableDTO.education);
					}
					
					if(mapOfSuper_hero_tableDTOTopassingYear.containsKey(oldSuper_hero_tableDTO.passingYear)) {
						mapOfSuper_hero_tableDTOTopassingYear.get(oldSuper_hero_tableDTO.passingYear).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTopassingYear.get(oldSuper_hero_tableDTO.passingYear).isEmpty()) {
						mapOfSuper_hero_tableDTOTopassingYear.remove(oldSuper_hero_tableDTO.passingYear);
					}
					
					if(mapOfSuper_hero_tableDTOToisStrong.containsKey(oldSuper_hero_tableDTO.isStrong)) {
						mapOfSuper_hero_tableDTOToisStrong.get(oldSuper_hero_tableDTO.isStrong).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToisStrong.get(oldSuper_hero_tableDTO.isStrong).isEmpty()) {
						mapOfSuper_hero_tableDTOToisStrong.remove(oldSuper_hero_tableDTO.isStrong);
					}
					
					if(mapOfSuper_hero_tableDTOToeyeColor.containsKey(oldSuper_hero_tableDTO.eyeColor)) {
						mapOfSuper_hero_tableDTOToeyeColor.get(oldSuper_hero_tableDTO.eyeColor).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOToeyeColor.get(oldSuper_hero_tableDTO.eyeColor).isEmpty()) {
						mapOfSuper_hero_tableDTOToeyeColor.remove(oldSuper_hero_tableDTO.eyeColor);
					}
					
					if(mapOfSuper_hero_tableDTOTohasARadio.containsKey(oldSuper_hero_tableDTO.hasARadio)) {
						mapOfSuper_hero_tableDTOTohasARadio.get(oldSuper_hero_tableDTO.hasARadio).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTohasARadio.get(oldSuper_hero_tableDTO.hasARadio).isEmpty()) {
						mapOfSuper_hero_tableDTOTohasARadio.remove(oldSuper_hero_tableDTO.hasARadio);
					}
					
					if(mapOfSuper_hero_tableDTOTopreciseWeight.containsKey(oldSuper_hero_tableDTO.preciseWeight)) {
						mapOfSuper_hero_tableDTOTopreciseWeight.get(oldSuper_hero_tableDTO.preciseWeight).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTopreciseWeight.get(oldSuper_hero_tableDTO.preciseWeight).isEmpty()) {
						mapOfSuper_hero_tableDTOTopreciseWeight.remove(oldSuper_hero_tableDTO.preciseWeight);
					}
					
					if(mapOfSuper_hero_tableDTOTomailRichtext.containsKey(oldSuper_hero_tableDTO.mailRichtext)) {
						mapOfSuper_hero_tableDTOTomailRichtext.get(oldSuper_hero_tableDTO.mailRichtext).remove(oldSuper_hero_tableDTO);
					}
					if(mapOfSuper_hero_tableDTOTomailRichtext.get(oldSuper_hero_tableDTO.mailRichtext).isEmpty()) {
						mapOfSuper_hero_tableDTOTomailRichtext.remove(oldSuper_hero_tableDTO.mailRichtext);
					}
					
					
				}
				if(super_hero_tableDTO.isDeleted == false) 
				{
					
					mapOfSuper_hero_tableDTOToiD.put(super_hero_tableDTO.iD, super_hero_tableDTO);
				
					if( ! mapOfSuper_hero_tableDTOTopassword.containsKey(super_hero_tableDTO.password)) {
						mapOfSuper_hero_tableDTOTopassword.put(super_hero_tableDTO.password, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTopassword.get(super_hero_tableDTO.password).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTofirstName.containsKey(super_hero_tableDTO.firstName)) {
						mapOfSuper_hero_tableDTOTofirstName.put(super_hero_tableDTO.firstName, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTofirstName.get(super_hero_tableDTO.firstName).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTolastName.containsKey(super_hero_tableDTO.lastName)) {
						mapOfSuper_hero_tableDTOTolastName.put(super_hero_tableDTO.lastName, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTolastName.get(super_hero_tableDTO.lastName).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTogender.containsKey(super_hero_tableDTO.gender)) {
						mapOfSuper_hero_tableDTOTogender.put(super_hero_tableDTO.gender, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTogender.get(super_hero_tableDTO.gender).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToreligion.containsKey(super_hero_tableDTO.religion)) {
						mapOfSuper_hero_tableDTOToreligion.put(super_hero_tableDTO.religion, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToreligion.get(super_hero_tableDTO.religion).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTodateOfBirth.containsKey(super_hero_tableDTO.dateOfBirth)) {
						mapOfSuper_hero_tableDTOTodateOfBirth.put(super_hero_tableDTO.dateOfBirth, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTodateOfBirth.get(super_hero_tableDTO.dateOfBirth).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToaddress.containsKey(super_hero_tableDTO.address)) {
						mapOfSuper_hero_tableDTOToaddress.put(super_hero_tableDTO.address, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToaddress.get(super_hero_tableDTO.address).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToaddressDetails.containsKey(super_hero_tableDTO.addressDetails)) {
						mapOfSuper_hero_tableDTOToaddressDetails.put(super_hero_tableDTO.addressDetails, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToaddressDetails.get(super_hero_tableDTO.addressDetails).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToimage.containsKey(super_hero_tableDTO.image)) {
						mapOfSuper_hero_tableDTOToimage.put(super_hero_tableDTO.image, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToimage.get(super_hero_tableDTO.image).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTobioDataFile.containsKey(super_hero_tableDTO.bioDataFile)) {
						mapOfSuper_hero_tableDTOTobioDataFile.put(super_hero_tableDTO.bioDataFile, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTobioDataFile.get(super_hero_tableDTO.bioDataFile).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTootherDatabase.containsKey(super_hero_tableDTO.otherDatabase)) {
						mapOfSuper_hero_tableDTOTootherDatabase.put(super_hero_tableDTO.otherDatabase, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTootherDatabase.get(super_hero_tableDTO.otherDatabase).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToeducation.containsKey(super_hero_tableDTO.education)) {
						mapOfSuper_hero_tableDTOToeducation.put(super_hero_tableDTO.education, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToeducation.get(super_hero_tableDTO.education).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTopassingYear.containsKey(super_hero_tableDTO.passingYear)) {
						mapOfSuper_hero_tableDTOTopassingYear.put(super_hero_tableDTO.passingYear, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTopassingYear.get(super_hero_tableDTO.passingYear).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToisStrong.containsKey(super_hero_tableDTO.isStrong)) {
						mapOfSuper_hero_tableDTOToisStrong.put(super_hero_tableDTO.isStrong, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToisStrong.get(super_hero_tableDTO.isStrong).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOToeyeColor.containsKey(super_hero_tableDTO.eyeColor)) {
						mapOfSuper_hero_tableDTOToeyeColor.put(super_hero_tableDTO.eyeColor, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOToeyeColor.get(super_hero_tableDTO.eyeColor).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTohasARadio.containsKey(super_hero_tableDTO.hasARadio)) {
						mapOfSuper_hero_tableDTOTohasARadio.put(super_hero_tableDTO.hasARadio, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTohasARadio.get(super_hero_tableDTO.hasARadio).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTopreciseWeight.containsKey(super_hero_tableDTO.preciseWeight)) {
						mapOfSuper_hero_tableDTOTopreciseWeight.put(super_hero_tableDTO.preciseWeight, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTopreciseWeight.get(super_hero_tableDTO.preciseWeight).add(super_hero_tableDTO);
					
					if( ! mapOfSuper_hero_tableDTOTomailRichtext.containsKey(super_hero_tableDTO.mailRichtext)) {
						mapOfSuper_hero_tableDTOTomailRichtext.put(super_hero_tableDTO.mailRichtext, new HashSet<>());
					}
					mapOfSuper_hero_tableDTOTomailRichtext.get(super_hero_tableDTO.mailRichtext).add(super_hero_tableDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Super_hero_tableDTO> getSuper_hero_tableList() {
		List <Super_hero_tableDTO> super_hero_tables = new ArrayList<Super_hero_tableDTO>(this.mapOfSuper_hero_tableDTOToiD.values());
		return super_hero_tables;
	}
	
	
	public Super_hero_tableDTO getSuper_hero_tableDTOByID( long ID){
		return mapOfSuper_hero_tableDTOToiD.get(ID);
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBypassword(String password) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTopassword.getOrDefault(password,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByfirst_name(String first_name) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTofirstName.getOrDefault(first_name,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBylast_name(String last_name) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTolastName.getOrDefault(last_name,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBygender(String gender) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTogender.getOrDefault(gender,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByreligion(String religion) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToreligion.getOrDefault(religion,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByaddress(String address) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToaddress.getOrDefault(address,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByaddress_Details(String address_Details) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToaddressDetails.getOrDefault(address_Details,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByimage(String image) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToimage.getOrDefault(image,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBybio_data_file(String bio_data_file) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTobioDataFile.getOrDefault(bio_data_file,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByother_database(String other_database) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTootherDatabase.getOrDefault(other_database,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByeducation(String education) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToeducation.getOrDefault(education,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBypassing_year(String passing_year) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTopassingYear.getOrDefault(passing_year,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByis_strong(boolean is_strong) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToisStrong.getOrDefault(is_strong,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByeye_color(String eye_color) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOToeyeColor.getOrDefault(eye_color,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByhas_a_radio(boolean has_a_radio) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTohasARadio.getOrDefault(has_a_radio,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOByprecise_weight(double precise_weight) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTopreciseWeight.getOrDefault(precise_weight,new HashSet<>()));
	}
	
	
	public List<Super_hero_tableDTO> getSuper_hero_tableDTOBymail_richtext(String mail_richtext) {
		return new ArrayList<>( mapOfSuper_hero_tableDTOTomailRichtext.getOrDefault(mail_richtext,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "super_hero_table";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


