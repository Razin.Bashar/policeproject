package comment_image;
import java.util.*; 


public class Comment_imageMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static Comment_imageMAPS self = null;
	
	private Comment_imageMAPS()
	{
		
		java_type_map.put("comment_id".toLowerCase(), "Long");
		java_type_map.put("name".toLowerCase(), "String");

		java_custom_search_map.put("comment_id".toLowerCase(), "Long");
		java_custom_search_map.put("name".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("commentId".toLowerCase(), "commentId".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Comment_imageMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Comment_imageMAPS ();
		}
		return self;
	}
	

}