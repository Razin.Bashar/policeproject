package comment_image;
import java.util.*; 


public class Comment_imageDTO {

	public long iD = 0;
	public long commentId = 0;
    public String name = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$Comment_imageDTO[" +
            " iD = " + iD +
            " commentId = " + commentId +
            " name = " + name +
            " isDeleted = " + isDeleted +
            "]";
    }

}