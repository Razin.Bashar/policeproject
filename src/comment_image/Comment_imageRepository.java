package comment_image;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Comment_imageRepository implements Repository {
	Comment_imageDAO comment_imageDAO = new Comment_imageDAO();
	
	
	static Logger logger = Logger.getLogger(Comment_imageRepository.class);
	Map<Long, Comment_imageDTO>mapOfComment_imageDTOToiD;
	Map<Long, Set<Comment_imageDTO> >mapOfComment_imageDTOTocommentId;
	Map<String, Set<Comment_imageDTO> >mapOfComment_imageDTOToname;


	static Comment_imageRepository instance = null;  
	private Comment_imageRepository(){
		mapOfComment_imageDTOToiD = new ConcurrentHashMap<>();
		mapOfComment_imageDTOTocommentId = new ConcurrentHashMap<>();
		mapOfComment_imageDTOToname = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Comment_imageRepository getInstance(){
		if (instance == null){
			instance = new Comment_imageRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Comment_imageDTO> comment_imageDTOs = comment_imageDAO.getAllComment_image(reloadAll);
			for(Comment_imageDTO comment_imageDTO : comment_imageDTOs) {
				Comment_imageDTO oldComment_imageDTO = getComment_imageDTOByID(comment_imageDTO.iD);
				if( oldComment_imageDTO != null ) {
					mapOfComment_imageDTOToiD.remove(oldComment_imageDTO.iD);
				
					if(mapOfComment_imageDTOTocommentId.containsKey(oldComment_imageDTO.commentId)) {
						mapOfComment_imageDTOTocommentId.get(oldComment_imageDTO.commentId).remove(oldComment_imageDTO);
					}
					if(mapOfComment_imageDTOTocommentId.get(oldComment_imageDTO.commentId).isEmpty()) {
						mapOfComment_imageDTOTocommentId.remove(oldComment_imageDTO.commentId);
					}
					
					if(mapOfComment_imageDTOToname.containsKey(oldComment_imageDTO.name)) {
						mapOfComment_imageDTOToname.get(oldComment_imageDTO.name).remove(oldComment_imageDTO);
					}
					if(mapOfComment_imageDTOToname.get(oldComment_imageDTO.name).isEmpty()) {
						mapOfComment_imageDTOToname.remove(oldComment_imageDTO.name);
					}
					
					
				}
				if(comment_imageDTO.isDeleted == false) 
				{
					
					mapOfComment_imageDTOToiD.put(comment_imageDTO.iD, comment_imageDTO);
				
					if( ! mapOfComment_imageDTOTocommentId.containsKey(comment_imageDTO.commentId)) {
						mapOfComment_imageDTOTocommentId.put(comment_imageDTO.commentId, new HashSet<>());
					}
					mapOfComment_imageDTOTocommentId.get(comment_imageDTO.commentId).add(comment_imageDTO);
					
					if( ! mapOfComment_imageDTOToname.containsKey(comment_imageDTO.name)) {
						mapOfComment_imageDTOToname.put(comment_imageDTO.name, new HashSet<>());
					}
					mapOfComment_imageDTOToname.get(comment_imageDTO.name).add(comment_imageDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Comment_imageDTO> getComment_imageList() {
		List <Comment_imageDTO> comment_images = new ArrayList<Comment_imageDTO>(this.mapOfComment_imageDTOToiD.values());
		return comment_images;
	}
	
	
	public Comment_imageDTO getComment_imageDTOByID( long ID){
		return mapOfComment_imageDTOToiD.get(ID);
	}
	
	
	public List<Comment_imageDTO> getComment_imageDTOBycomment_id(long comment_id) {
		return new ArrayList<>( mapOfComment_imageDTOTocommentId.getOrDefault(comment_id,new HashSet<>()));
	}
	
	
	public List<Comment_imageDTO> getComment_imageDTOByname(String name) {
		return new ArrayList<>( mapOfComment_imageDTOToname.getOrDefault(name,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "comment_image";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


