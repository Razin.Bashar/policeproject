package super_hero_table_report;

import annotation.ColumnName;
import pbReport.*;



public class Super_hero_table_report_Descriptor 
{
	@ColumnName("super_hero_table.first_name")
	public String firstName;
	@ColumnName("super_hero_table.gender")
	public String gender;
	@ColumnName("super_hero_table.religion")
	public String religion;
	@ColumnName("super_hero_table.last_name")
	public String lastName;
	@ColumnName("super_hero_table.address")
	@Display(GeoConvertor.class)
	public String address;
	@ColumnName("strength.strength_name")
	public String strengthName;
}