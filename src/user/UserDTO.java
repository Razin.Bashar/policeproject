package user;

import java.util.ArrayList;

import annotation.ColumnName;
import annotation.PrimaryKey;
import annotation.TableName;

/**
 * @author Kayesh Parvez
 *
 */
@TableName("user")
public class UserDTO {
	@PrimaryKey
	@ColumnName("user.ID")
	public long ID;
	@ColumnName("user.username")
	public String userName = "";
	public String password = "";
	public int userType = 1;
	public long roleID = 0;
	public int languageID = 1;
	public boolean isDeleted;
	public String mailAddress = "";
	public String fullName = "";
	public String phoneNo = "";
	
	public String roleName = "";
	public String userTypeName = "";
	public ArrayList<String> loginIPs = new ArrayList<>();
	
	public boolean otpSMS;
	public boolean otpEmail;
	public boolean otpPushNotification;
	
	@Override
	public String toString() {
		return "UserDTO [ID=" + ID + ", userName=" + userName + ", userType=" + userType + ", roleID=" + roleID
				+ ", languageID=" + languageID + ", isDeleted=" + isDeleted + ", mailAddress=" + mailAddress
				+ ", fullName=" + fullName + ", phoneNo=" + phoneNo + ", roleName=" + roleName + ", userTypeName="
				+ userTypeName + ", loginIPs=" + loginIPs + "]";
	}
	


}
