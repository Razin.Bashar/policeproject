package user;
/**
 * @author Kayesh Parvez
 *
 */
public class UserTypeDTO {
	public int ID;
	public String name_en;
	public String name_bn;
	public String dashboard;
	@Override
	public String toString() {
		return "UserTypeDTO [ID=" + ID + ", name_en=" + name_en + ", name_bn=" + name_bn + ", dashboard=" + dashboard
				+ "]";
	}

	
	
}
