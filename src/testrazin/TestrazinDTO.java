package testrazin;
import java.util.*; 


public class TestrazinDTO {

	public long iD = 0;
    public String password = "";
    public String firstName = "";
    public String lastName = "";
    public String gender = "";
    public String religion = "";
	public long dateOfBirth = 0;
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$TestrazinDTO[" +
            " iD = " + iD +
            " password = " + password +
            " firstName = " + firstName +
            " lastName = " + lastName +
            " gender = " + gender +
            " religion = " + religion +
            " dateOfBirth = " + dateOfBirth +
            " isDeleted = " + isDeleted +
            "]";
    }

}