package testrazin;
import java.util.*; 


public class TestrazinMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static TestrazinMAPS self = null;
	
	private TestrazinMAPS()
	{
		
		java_type_map.put("first_name".toLowerCase(), "String");
		java_type_map.put("last_name".toLowerCase(), "String");
		java_type_map.put("gender".toLowerCase(), "String");
		java_type_map.put("religion".toLowerCase(), "String");
		java_type_map.put("date_of_birth".toLowerCase(), "Long");

		java_custom_search_map.put("first_name".toLowerCase(), "String");
		java_custom_search_map.put("last_name".toLowerCase(), "String");
		java_custom_search_map.put("gender".toLowerCase(), "String");
		java_custom_search_map.put("religion".toLowerCase(), "String");
		java_custom_search_map.put("date_of_birth".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("password".toLowerCase(), "password".toLowerCase());
		java_DTO_map.put("firstName".toLowerCase(), "firstName".toLowerCase());
		java_DTO_map.put("lastName".toLowerCase(), "lastName".toLowerCase());
		java_DTO_map.put("gender".toLowerCase(), "gender".toLowerCase());
		java_DTO_map.put("religion".toLowerCase(), "religion".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static TestrazinMAPS GetInstance()
	{
		if(self == null)
		{
			self = new TestrazinMAPS ();
		}
		return self;
	}
	

}