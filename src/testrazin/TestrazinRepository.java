package testrazin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class TestrazinRepository implements Repository {
	TestrazinDAO testrazinDAO = new TestrazinDAO();
	
	
	static Logger logger = Logger.getLogger(TestrazinRepository.class);
	Map<Long, TestrazinDTO>mapOfTestrazinDTOToiD;
	Map<String, Set<TestrazinDTO> >mapOfTestrazinDTOTopassword;
	Map<String, Set<TestrazinDTO> >mapOfTestrazinDTOTofirstName;
	Map<String, Set<TestrazinDTO> >mapOfTestrazinDTOTolastName;
	Map<String, Set<TestrazinDTO> >mapOfTestrazinDTOTogender;
	Map<String, Set<TestrazinDTO> >mapOfTestrazinDTOToreligion;
	Map<Long, Set<TestrazinDTO> >mapOfTestrazinDTOTodateOfBirth;


	static TestrazinRepository instance = null;  
	private TestrazinRepository(){
		mapOfTestrazinDTOToiD = new ConcurrentHashMap<>();
		mapOfTestrazinDTOTopassword = new ConcurrentHashMap<>();
		mapOfTestrazinDTOTofirstName = new ConcurrentHashMap<>();
		mapOfTestrazinDTOTolastName = new ConcurrentHashMap<>();
		mapOfTestrazinDTOTogender = new ConcurrentHashMap<>();
		mapOfTestrazinDTOToreligion = new ConcurrentHashMap<>();
		mapOfTestrazinDTOTodateOfBirth = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static TestrazinRepository getInstance(){
		if (instance == null){
			instance = new TestrazinRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<TestrazinDTO> testrazinDTOs = testrazinDAO.getAllTestrazin(reloadAll);
			for(TestrazinDTO testrazinDTO : testrazinDTOs) {
				TestrazinDTO oldTestrazinDTO = getTestrazinDTOByID(testrazinDTO.iD);
				if( oldTestrazinDTO != null ) {
					mapOfTestrazinDTOToiD.remove(oldTestrazinDTO.iD);
				
					if(mapOfTestrazinDTOTopassword.containsKey(oldTestrazinDTO.password)) {
						mapOfTestrazinDTOTopassword.get(oldTestrazinDTO.password).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOTopassword.get(oldTestrazinDTO.password).isEmpty()) {
						mapOfTestrazinDTOTopassword.remove(oldTestrazinDTO.password);
					}
					
					if(mapOfTestrazinDTOTofirstName.containsKey(oldTestrazinDTO.firstName)) {
						mapOfTestrazinDTOTofirstName.get(oldTestrazinDTO.firstName).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOTofirstName.get(oldTestrazinDTO.firstName).isEmpty()) {
						mapOfTestrazinDTOTofirstName.remove(oldTestrazinDTO.firstName);
					}
					
					if(mapOfTestrazinDTOTolastName.containsKey(oldTestrazinDTO.lastName)) {
						mapOfTestrazinDTOTolastName.get(oldTestrazinDTO.lastName).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOTolastName.get(oldTestrazinDTO.lastName).isEmpty()) {
						mapOfTestrazinDTOTolastName.remove(oldTestrazinDTO.lastName);
					}
					
					if(mapOfTestrazinDTOTogender.containsKey(oldTestrazinDTO.gender)) {
						mapOfTestrazinDTOTogender.get(oldTestrazinDTO.gender).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOTogender.get(oldTestrazinDTO.gender).isEmpty()) {
						mapOfTestrazinDTOTogender.remove(oldTestrazinDTO.gender);
					}
					
					if(mapOfTestrazinDTOToreligion.containsKey(oldTestrazinDTO.religion)) {
						mapOfTestrazinDTOToreligion.get(oldTestrazinDTO.religion).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOToreligion.get(oldTestrazinDTO.religion).isEmpty()) {
						mapOfTestrazinDTOToreligion.remove(oldTestrazinDTO.religion);
					}
					
					if(mapOfTestrazinDTOTodateOfBirth.containsKey(oldTestrazinDTO.dateOfBirth)) {
						mapOfTestrazinDTOTodateOfBirth.get(oldTestrazinDTO.dateOfBirth).remove(oldTestrazinDTO);
					}
					if(mapOfTestrazinDTOTodateOfBirth.get(oldTestrazinDTO.dateOfBirth).isEmpty()) {
						mapOfTestrazinDTOTodateOfBirth.remove(oldTestrazinDTO.dateOfBirth);
					}
					
					
				}
				if(testrazinDTO.isDeleted == false) 
				{
					
					mapOfTestrazinDTOToiD.put(testrazinDTO.iD, testrazinDTO);
				
					if( ! mapOfTestrazinDTOTopassword.containsKey(testrazinDTO.password)) {
						mapOfTestrazinDTOTopassword.put(testrazinDTO.password, new HashSet<>());
					}
					mapOfTestrazinDTOTopassword.get(testrazinDTO.password).add(testrazinDTO);
					
					if( ! mapOfTestrazinDTOTofirstName.containsKey(testrazinDTO.firstName)) {
						mapOfTestrazinDTOTofirstName.put(testrazinDTO.firstName, new HashSet<>());
					}
					mapOfTestrazinDTOTofirstName.get(testrazinDTO.firstName).add(testrazinDTO);
					
					if( ! mapOfTestrazinDTOTolastName.containsKey(testrazinDTO.lastName)) {
						mapOfTestrazinDTOTolastName.put(testrazinDTO.lastName, new HashSet<>());
					}
					mapOfTestrazinDTOTolastName.get(testrazinDTO.lastName).add(testrazinDTO);
					
					if( ! mapOfTestrazinDTOTogender.containsKey(testrazinDTO.gender)) {
						mapOfTestrazinDTOTogender.put(testrazinDTO.gender, new HashSet<>());
					}
					mapOfTestrazinDTOTogender.get(testrazinDTO.gender).add(testrazinDTO);
					
					if( ! mapOfTestrazinDTOToreligion.containsKey(testrazinDTO.religion)) {
						mapOfTestrazinDTOToreligion.put(testrazinDTO.religion, new HashSet<>());
					}
					mapOfTestrazinDTOToreligion.get(testrazinDTO.religion).add(testrazinDTO);
					
					if( ! mapOfTestrazinDTOTodateOfBirth.containsKey(testrazinDTO.dateOfBirth)) {
						mapOfTestrazinDTOTodateOfBirth.put(testrazinDTO.dateOfBirth, new HashSet<>());
					}
					mapOfTestrazinDTOTodateOfBirth.get(testrazinDTO.dateOfBirth).add(testrazinDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<TestrazinDTO> getTestrazinList() {
		List <TestrazinDTO> testrazins = new ArrayList<TestrazinDTO>(this.mapOfTestrazinDTOToiD.values());
		return testrazins;
	}
	
	
	public TestrazinDTO getTestrazinDTOByID( long ID){
		return mapOfTestrazinDTOToiD.get(ID);
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOBypassword(String password) {
		return new ArrayList<>( mapOfTestrazinDTOTopassword.getOrDefault(password,new HashSet<>()));
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOByfirst_name(String first_name) {
		return new ArrayList<>( mapOfTestrazinDTOTofirstName.getOrDefault(first_name,new HashSet<>()));
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOBylast_name(String last_name) {
		return new ArrayList<>( mapOfTestrazinDTOTolastName.getOrDefault(last_name,new HashSet<>()));
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOBygender(String gender) {
		return new ArrayList<>( mapOfTestrazinDTOTogender.getOrDefault(gender,new HashSet<>()));
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOByreligion(String religion) {
		return new ArrayList<>( mapOfTestrazinDTOToreligion.getOrDefault(religion,new HashSet<>()));
	}
	
	
	public List<TestrazinDTO> getTestrazinDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfTestrazinDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "testrazin";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


