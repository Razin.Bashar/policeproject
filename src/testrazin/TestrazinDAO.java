package testrazin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class TestrazinDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"testrazin");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"testrazin");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(TestrazinDTO testrazinDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = testrazinDTO.iD;
		// userDTO.userName = testrazinDTO.email;
		// userDTO.fullName = testrazinDTO.name;
		// userDTO.password = testrazinDTO.password;
		// userDTO.phoneNo = testrazinDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = testrazinDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(TestrazinDTO testrazinDTO, UserDTO userDTO)
	{
		// userDTO.ID = testrazinDTO.iD;
		// userDTO.fullName = testrazinDTO.name;
		// userDTO.phoneNo = testrazinDTO.phone;
		// userDTO.mailAddress = testrazinDTO.email;

		return userDTO;
	}
		
		
	
	public void addTestrazin(TestrazinDTO testrazinDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			testrazinDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Testrazin");

			String sql = "INSERT INTO testrazin";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(testrazinDTO));
			// testrazinDTO.iD = userDAO.getUserDTOByUsername(testrazinDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + testrazinDTO.iD + " in index " + index);
			ps.setObject(index++,testrazinDTO.iD);
			//System.out.println("Setting object" + testrazinDTO.password + " in index " + index);
			ps.setObject(index++,testrazinDTO.password);
			//System.out.println("Setting object" + testrazinDTO.firstName + " in index " + index);
			ps.setObject(index++,testrazinDTO.firstName);
			//System.out.println("Setting object" + testrazinDTO.lastName + " in index " + index);
			ps.setObject(index++,testrazinDTO.lastName);
			//System.out.println("Setting object" + testrazinDTO.gender + " in index " + index);
			ps.setObject(index++,testrazinDTO.gender);
			//System.out.println("Setting object" + testrazinDTO.religion + " in index " + index);
			ps.setObject(index++,testrazinDTO.religion);
			//System.out.println("Setting object" + testrazinDTO.dateOfBirth + " in index " + index);
			ps.setObject(index++,testrazinDTO.dateOfBirth);
			//System.out.println("Setting object" + testrazinDTO.isDeleted + " in index " + index);
			ps.setObject(index++,testrazinDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//TestrazinRepository.getInstance().reload(false);		
	}
	
	//need another getter for repository
	public TestrazinDTO getTestrazinDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		TestrazinDTO testrazinDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM testrazin";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				testrazinDTO = new TestrazinDTO();

				testrazinDTO.iD = rs.getLong("ID");
				testrazinDTO.password = rs.getString("password");
				testrazinDTO.firstName = rs.getString("first_name");
				testrazinDTO.lastName = rs.getString("last_name");
				testrazinDTO.gender = rs.getString("gender");
				testrazinDTO.religion = rs.getString("religion");
				testrazinDTO.dateOfBirth = rs.getLong("date_of_birth");
				testrazinDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return testrazinDTO;
	}
	
	public void updateTestrazin(TestrazinDTO testrazinDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE testrazin";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "password=?";
			sql += ", ";
			sql += "first_name=?";
			sql += ", ";
			sql += "last_name=?";
			sql += ", ";
			sql += "gender=?";
			sql += ", ";
			sql += "religion=?";
			sql += ", ";
			sql += "date_of_birth=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + testrazinDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + testrazinDTO.iD + " in index " + index);
			ps.setObject(index++,testrazinDTO.iD);
			//System.out.println("Setting object" + testrazinDTO.password + " in index " + index);
			ps.setObject(index++,testrazinDTO.password);
			//System.out.println("Setting object" + testrazinDTO.firstName + " in index " + index);
			ps.setObject(index++,testrazinDTO.firstName);
			//System.out.println("Setting object" + testrazinDTO.lastName + " in index " + index);
			ps.setObject(index++,testrazinDTO.lastName);
			//System.out.println("Setting object" + testrazinDTO.gender + " in index " + index);
			ps.setObject(index++,testrazinDTO.gender);
			//System.out.println("Setting object" + testrazinDTO.religion + " in index " + index);
			ps.setObject(index++,testrazinDTO.religion);
			//System.out.println("Setting object" + testrazinDTO.dateOfBirth + " in index " + index);
			ps.setObject(index++,testrazinDTO.dateOfBirth);
			//System.out.println("Setting object" + testrazinDTO.isDeleted + " in index " + index);
			ps.setObject(index++,testrazinDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(testrazinDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(testrazinDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//TestrazinRepository.getInstance().reload(false);
	}
	
	public void deleteTestrazinByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE testrazin";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<TestrazinDTO> getTestrazinDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		TestrazinDTO testrazinDTO = null;
		List<TestrazinDTO> testrazinDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM testrazin";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			if(rs.next()){
				testrazinDTO = new TestrazinDTO();
				testrazinDTO.iD = rs.getLong("ID");
				testrazinDTO.password = rs.getString("password");
				testrazinDTO.firstName = rs.getString("first_name");
				testrazinDTO.lastName = rs.getString("last_name");
				testrazinDTO.gender = rs.getString("gender");
				testrazinDTO.religion = rs.getString("religion");
				testrazinDTO.dateOfBirth = rs.getLong("date_of_birth");
				testrazinDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = testrazinDTO.iD;
				while(i < testrazinDTOList.size() && testrazinDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				testrazinDTOList.add(i,  testrazinDTO);
				//testrazinDTOList.add(testrazinDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return testrazinDTOList;
	}
	
	public List<TestrazinDTO> getTestrazinDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getTestrazinDTOByColumn(filter);
	}
	
	public List<TestrazinDTO> getTestrazinDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getTestrazinDTOByColumn(filter);
	}
	
	public List<TestrazinDTO> getTestrazinDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getTestrazinDTOByColumn(filter);
	}
		
	
	
	public List<TestrazinDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		TestrazinDTO testrazinDTO = null;
		List<TestrazinDTO> testrazinDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return testrazinDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "password";
			sql += ", ";
			sql += "first_name";
			sql += ", ";
			sql += "last_name";
			sql += ", ";
			sql += "gender";
			sql += ", ";
			sql += "religion";
			sql += ", ";
			sql += "date_of_birth";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM testrazin";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				testrazinDTO = new TestrazinDTO();
				testrazinDTO.iD = rs.getLong("ID");
				testrazinDTO.password = rs.getString("password");
				testrazinDTO.firstName = rs.getString("first_name");
				testrazinDTO.lastName = rs.getString("last_name");
				testrazinDTO.gender = rs.getString("gender");
				testrazinDTO.religion = rs.getString("religion");
				testrazinDTO.dateOfBirth = rs.getLong("date_of_birth");
				testrazinDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + testrazinDTO);
				//testrazinDTOList.add(testrazinDTO);
				int i = 0;
				long primaryKey = testrazinDTO.iD;
				while(i < testrazinDTOList.size() && testrazinDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				testrazinDTOList.add(i,  testrazinDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return testrazinDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM testrazin";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<TestrazinDTO> getAllTestrazin (boolean isFirstReload)
    {
		List<TestrazinDTO> testrazinDTOList = new ArrayList<>();

		String sql = "SELECT * FROM testrazin";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				TestrazinDTO testrazinDTO = new TestrazinDTO();
				testrazinDTO.iD = rs.getLong("ID");
				testrazinDTO.password = rs.getString("password");
				testrazinDTO.firstName = rs.getString("first_name");
				testrazinDTO.lastName = rs.getString("last_name");
				testrazinDTO.gender = rs.getString("gender");
				testrazinDTO.religion = rs.getString("religion");
				testrazinDTO.dateOfBirth = rs.getLong("date_of_birth");
				testrazinDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = testrazinDTO.iD;
				while(i < testrazinDTOList.size() && testrazinDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				testrazinDTOList.add(i,  testrazinDTO);
				//testrazinDTOList.add(testrazinDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return testrazinDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM testrazin";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		TestrazinDTO testrazinDTO = new TestrazinDTO();
		try{

			String sql = "SELECT ID FROM testrazin";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = TestrazinMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(TestrazinMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !TestrazinMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(TestrazinMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	