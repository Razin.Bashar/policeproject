DELETE FROM language_text WHERE menuID = 621001;
DELETE FROM language_text WHERE menuID = 621002;
DELETE FROM language_text WHERE menuID = 621003;
DELETE FROM language_text WHERE languageConstantPrefix = 'VEHICLE_TYPE_ADD';
DELETE FROM language_text WHERE languageConstantPrefix = 'VEHICLE_TYPE_EDIT';
DELETE FROM language_text WHERE languageConstantPrefix = 'VEHICLE_TYPE_SEARCH';
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502042','621001','ID ','আইডি ','VEHICLE_TYPE_ADD','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502043','621001','Name Bn ','নাম অজানা ','VEHICLE_TYPE_ADD','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502044','621001','Name En ','নাম অজানা ','VEHICLE_TYPE_ADD','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502045','621001','IsDeleted ','অজানা ','VEHICLE_TYPE_ADD','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502046','621002','ID ','আইডি ','VEHICLE_TYPE_EDIT','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502047','621002','Name Bn ','নাম অজানা ','VEHICLE_TYPE_EDIT','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502048','621002','Name En ','নাম অজানা ','VEHICLE_TYPE_EDIT','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502049','621002','IsDeleted ','অজানা ','VEHICLE_TYPE_EDIT','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502050','621003','ID ','আইডি ','VEHICLE_TYPE_SEARCH','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502051','621003','Name Bn ','নাম অজানা ','VEHICLE_TYPE_SEARCH','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502052','621003','Name En ','নাম অজানা ','VEHICLE_TYPE_SEARCH','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502053','621003','IsDeleted ','অজানা ','VEHICLE_TYPE_SEARCH','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502054','621001','VEHICLE TYPE  ADD','যান টাইপ করা  যোগ করুন','VEHICLE_TYPE_ADD','VEHICLE_TYPE_ADD_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502055','621001','ADD','যোগ করুন','VEHICLE_TYPE_ADD','VEHICLE_TYPE_ADD_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502056','621001','SUBMIT','সাবমিট করুন','VEHICLE_TYPE_ADD','VEHICLE_TYPE_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502057','621001','CANCEL','বাতিল করুন','VEHICLE_TYPE_ADD','VEHICLE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502058','621002','VEHICLE TYPE  EDIT','যান টাইপ করা  সম্পাদনা করুন','VEHICLE_TYPE_EDIT','VEHICLE_TYPE_EDIT_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502059','621002','SUBMIT','সাবমিট করুন','VEHICLE_TYPE_EDIT','VEHICLE_TYPE_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502060','621002','CANCEL','বাতিল করুন','VEHICLE_TYPE_EDIT','VEHICLE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502061','621002','English','Bangla','VEHICLE_TYPE_EDIT','LANGUAGE');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502062','621003','VEHICLE TYPE  SEARCH','যান টাইপ করা  খুঁজুন','VEHICLE_TYPE_SEARCH','VEHICLE_TYPE_SEARCH_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502063','621003','SEARCH','খুঁজুন','VEHICLE_TYPE_SEARCH','VEHICLE_TYPE_SEARCH_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502064','621003','DELETE','ডিলিট করুন','VEHICLE_TYPE_SEARCH','VEHICLE_TYPE_DELETE_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502065','621003','EDIT','সম্পাদনা করুন','VEHICLE_TYPE_SEARCH','VEHICLE_TYPE_EDIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502066','621003','CANCEL','বাতিল করুন','VEHICLE_TYPE_SEARCH','VEHICLE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('502067','621003','Any Field','যেকোন ক্ষেত্র','VEHICLE_TYPE_SEARCH','ANYFIELD');
