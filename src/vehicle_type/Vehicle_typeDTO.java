package vehicle_type;
import java.util.*; 


public class Vehicle_typeDTO {

	public long iD = 0;
    public String nameBn = "";
    public String nameEn = "";
    public String logo = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$Vehicle_typeDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " isDeleted = " + isDeleted +
            "]";
    }

}