package vehicle_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class Vehicle_typeDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"vehicle_type");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"vehicle_type");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(Vehicle_typeDTO vehicle_typeDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = vehicle_typeDTO.iD;
		// userDTO.userName = vehicle_typeDTO.email;
		// userDTO.fullName = vehicle_typeDTO.name;
		// userDTO.password = vehicle_typeDTO.password;
		// userDTO.phoneNo = vehicle_typeDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = vehicle_typeDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(Vehicle_typeDTO vehicle_typeDTO, UserDTO userDTO)
	{
		// userDTO.ID = vehicle_typeDTO.iD;
		// userDTO.fullName = vehicle_typeDTO.name;
		// userDTO.phoneNo = vehicle_typeDTO.phone;
		// userDTO.mailAddress = vehicle_typeDTO.email;

		return userDTO;
	}
		
		
	
	public void addVehicle_type(Vehicle_typeDTO vehicle_typeDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			vehicle_typeDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Vehicle_type");

			String sql = "INSERT INTO vehicle_type";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(vehicle_typeDTO));
			// vehicle_typeDTO.iD = userDAO.getUserDTOByUsername(vehicle_typeDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + vehicle_typeDTO.iD + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.iD);
			//System.out.println("Setting object" + vehicle_typeDTO.nameBn + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.nameBn);
			//System.out.println("Setting object" + vehicle_typeDTO.nameEn + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.nameEn);
			//System.out.println("Setting object" + vehicle_typeDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Vehicle_typeRepository.getInstance().reload(false);		
	}
	
	//need another getter for repository
	public Vehicle_typeDTO getVehicle_typeDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_typeDTO vehicle_typeDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_type";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				vehicle_typeDTO = new Vehicle_typeDTO();

				vehicle_typeDTO.iD = rs.getLong("ID");
				vehicle_typeDTO.nameBn = rs.getString("name_bn");
				vehicle_typeDTO.nameEn = rs.getString("name_en");
				vehicle_typeDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_typeDTO;
	}
	
	public void updateVehicle_type(Vehicle_typeDTO vehicle_typeDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE vehicle_type";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "name_bn=?";
			sql += ", ";
			sql += "name_en=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + vehicle_typeDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + vehicle_typeDTO.iD + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.iD);
			//System.out.println("Setting object" + vehicle_typeDTO.nameBn + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.nameBn);
			//System.out.println("Setting object" + vehicle_typeDTO.nameEn + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.nameEn);
			//System.out.println("Setting object" + vehicle_typeDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicle_typeDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(vehicle_typeDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(vehicle_typeDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Vehicle_typeRepository.getInstance().reload(false);
	}
	
	public void deleteVehicle_typeByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE vehicle_type";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<Vehicle_typeDTO> getVehicle_typeDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_typeDTO vehicle_typeDTO = null;
		List<Vehicle_typeDTO> vehicle_typeDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_type";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			if(rs.next()){
				vehicle_typeDTO = new Vehicle_typeDTO();
				vehicle_typeDTO.iD = rs.getLong("ID");
				vehicle_typeDTO.nameBn = rs.getString("name_bn");
				vehicle_typeDTO.nameEn = rs.getString("name_en");
				vehicle_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicle_typeDTO.iD;
				while(i < vehicle_typeDTOList.size() && vehicle_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_typeDTOList.add(i,  vehicle_typeDTO);
				//vehicle_typeDTOList.add(vehicle_typeDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_typeDTOList;
	}
	
	public List<Vehicle_typeDTO> getVehicle_typeDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getVehicle_typeDTOByColumn(filter);
	}
	
	public List<Vehicle_typeDTO> getVehicle_typeDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getVehicle_typeDTOByColumn(filter);
	}
	
	public List<Vehicle_typeDTO> getVehicle_typeDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getVehicle_typeDTOByColumn(filter);
	}
		
	
	
	public List<Vehicle_typeDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_typeDTO vehicle_typeDTO = null;
		List<Vehicle_typeDTO> vehicle_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return vehicle_typeDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_type";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				vehicle_typeDTO = new Vehicle_typeDTO();
				vehicle_typeDTO.iD = rs.getLong("ID");
				vehicle_typeDTO.nameBn = rs.getString("name_bn");
				vehicle_typeDTO.nameEn = rs.getString("name_en");
				vehicle_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + vehicle_typeDTO);
				//vehicle_typeDTOList.add(vehicle_typeDTO);
				int i = 0;
				long primaryKey = vehicle_typeDTO.iD;
				while(i < vehicle_typeDTOList.size() && vehicle_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_typeDTOList.add(i,  vehicle_typeDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_typeDTOList;
	
	}
	
	

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM vehicle_type";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<Vehicle_typeDTO> getAllVehicle_type (boolean isFirstReload)
    {
		List<Vehicle_typeDTO> vehicle_typeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM vehicle_type";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Vehicle_typeDTO vehicle_typeDTO = new Vehicle_typeDTO();
				vehicle_typeDTO.iD = rs.getLong("ID");
				vehicle_typeDTO.nameBn = rs.getString("name_bn");
				vehicle_typeDTO.nameEn = rs.getString("name_en");
				vehicle_typeDTO.logo = rs.getString("logo");
				vehicle_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicle_typeDTO.iD;
				while(i < vehicle_typeDTOList.size() && vehicle_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_typeDTOList.add(i,  vehicle_typeDTO);
				//vehicle_typeDTOList.add(vehicle_typeDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return vehicle_typeDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM vehicle_type";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		try{

			String sql = "SELECT ID FROM vehicle_type";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Vehicle_typeMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception 
	{
		
		ArrayList<long[]> idList = (ArrayList<long[]>)recordIDs;
		Connection connection = null;
		PreparedStatement ps = null;
		Vehicle_typeDTO vehicle_typeDTO = null;
		List<Vehicle_typeDTO> vehicle_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return vehicle_typeDTOList;
		}

		try{

			String sql = "SELECT ";
			Iterator it = Vehicle_typeMAPS.GetInstance().java_SQL_map.entrySet().iterator();
			int i = 0;
			while(it.hasNext())
			{
				if( i > 0)
	        	{
					sql+= " ,  ";
	        	}
				Map.Entry pair = (Map.Entry)it.next();
				sql+= pair.getKey();
				i ++;
			}		
			sql += " FROM ";
			
			for(i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" inner join ");
				}
				sql += table_names.get(i) + " ";
			}
			
			sql += " WHERE ";
			
			for(i = 0; i < idList.size(); i ++)
			{
				long[] ids = idList.get(i);
				if(i > 0)
				{
					sql += (" OR ");
				}
				sql += " ( ";
				for(int j = 0; j < table_names.size(); j ++)
				{
					if(j > 0)
					{
						sql += (" AND ");
					}
					sql += table_names.get(j) + ".ID = " + ids[j] + " AND " + table_names.get(j) + ".isDeleted = false";
				}
				sql += " ) ";
			}
			
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				vehicle_typeDTO = new Vehicle_typeDTO();
				vehicle_typeDTO.iD = rs.getLong("ID");
				vehicle_typeDTO.nameBn = rs.getString("name_bn");
				vehicle_typeDTO.nameEn = rs.getString("name_en");
				vehicle_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				i = 0;
				long primaryKey = vehicle_typeDTO.iD;
				while(i < vehicle_typeDTOList.size() && vehicle_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_typeDTOList.add(i,  vehicle_typeDTO);

			}		
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return vehicle_typeDTOList;
	}
	
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		long[] ids = new long[table_names.size()];
		List<long[]> idList = new ArrayList<long[]>();
		Connection connection = null;
		PreparedStatement ps = null;
		

		try{

			String sql = "SELECT ";
			for(int i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" , ");
				}
				sql += table_names.get(i) + ".ID as " + table_names.get(i)+ "_ID";
			}
			sql +=  " ID ";
			sql += " FROM ";
			
			for(int i = 0; i < table_names.size(); i ++)
			{
				if(i > 0)
				{
					sql += (" inner join ");
				}
				sql += table_names.get(i) + " ";
			}
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Vehicle_typeMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Vehicle_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			
			for(int j = 0; j < table_names.size(); j ++)
			{
				if(j > 0)
				{
					sql += (" AND ");
				}
				sql += table_names.get(j) + ".isDeleted = false";
			}				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				for(int j = 0; j < table_names.size(); j ++)
				{
					ids[j] = rs.getLong(table_names.get(j) + "_ID");
				}
				idList.add(ids);
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
		
}
	