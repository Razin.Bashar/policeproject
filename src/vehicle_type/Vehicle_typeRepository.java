package vehicle_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vehicle_typeRepository implements Repository {
	Vehicle_typeDAO vehicle_typeDAO = new Vehicle_typeDAO();
	
	
	static Logger logger = Logger.getLogger(Vehicle_typeRepository.class);
	Map<Long, Vehicle_typeDTO>mapOfVehicle_typeDTOToiD;
	Map<String, Set<Vehicle_typeDTO> >mapOfVehicle_typeDTOTonameBn;
	Map<String, Set<Vehicle_typeDTO> >mapOfVehicle_typeDTOTonameEn;


	static Vehicle_typeRepository instance = null;  
	private Vehicle_typeRepository(){
		mapOfVehicle_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfVehicle_typeDTOTonameBn = new ConcurrentHashMap<>();
		mapOfVehicle_typeDTOTonameEn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vehicle_typeRepository getInstance(){
		if (instance == null){
			instance = new Vehicle_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Vehicle_typeDTO> vehicle_typeDTOs = vehicle_typeDAO.getAllVehicle_type(reloadAll);
			for(Vehicle_typeDTO vehicle_typeDTO : vehicle_typeDTOs) {
				Vehicle_typeDTO oldVehicle_typeDTO = getVehicle_typeDTOByID(vehicle_typeDTO.iD);
				if( oldVehicle_typeDTO != null ) {
					mapOfVehicle_typeDTOToiD.remove(oldVehicle_typeDTO.iD);
				
					if(mapOfVehicle_typeDTOTonameBn.containsKey(oldVehicle_typeDTO.nameBn)) {
						mapOfVehicle_typeDTOTonameBn.get(oldVehicle_typeDTO.nameBn).remove(oldVehicle_typeDTO);
					}
					if(mapOfVehicle_typeDTOTonameBn.get(oldVehicle_typeDTO.nameBn).isEmpty()) {
						mapOfVehicle_typeDTOTonameBn.remove(oldVehicle_typeDTO.nameBn);
					}
					
					if(mapOfVehicle_typeDTOTonameEn.containsKey(oldVehicle_typeDTO.nameEn)) {
						mapOfVehicle_typeDTOTonameEn.get(oldVehicle_typeDTO.nameEn).remove(oldVehicle_typeDTO);
					}
					if(mapOfVehicle_typeDTOTonameEn.get(oldVehicle_typeDTO.nameEn).isEmpty()) {
						mapOfVehicle_typeDTOTonameEn.remove(oldVehicle_typeDTO.nameEn);
					}
					
					
				}
				if(vehicle_typeDTO.isDeleted == false) 
				{
					
					mapOfVehicle_typeDTOToiD.put(vehicle_typeDTO.iD, vehicle_typeDTO);
				
					if( ! mapOfVehicle_typeDTOTonameBn.containsKey(vehicle_typeDTO.nameBn)) {
						mapOfVehicle_typeDTOTonameBn.put(vehicle_typeDTO.nameBn, new HashSet<>());
					}
					mapOfVehicle_typeDTOTonameBn.get(vehicle_typeDTO.nameBn).add(vehicle_typeDTO);
					
					if( ! mapOfVehicle_typeDTOTonameEn.containsKey(vehicle_typeDTO.nameEn)) {
						mapOfVehicle_typeDTOTonameEn.put(vehicle_typeDTO.nameEn, new HashSet<>());
					}
					mapOfVehicle_typeDTOTonameEn.get(vehicle_typeDTO.nameEn).add(vehicle_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vehicle_typeDTO> getVehicle_typeList() {
		List <Vehicle_typeDTO> vehicle_types = new ArrayList<Vehicle_typeDTO>(this.mapOfVehicle_typeDTOToiD.values());
		return vehicle_types;
	}
	
	
	public Vehicle_typeDTO getVehicle_typeDTOByID( long ID){
		return mapOfVehicle_typeDTOToiD.get(ID);
	}
	
	
	public List<Vehicle_typeDTO> getVehicle_typeDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfVehicle_typeDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Vehicle_typeDTO> getVehicle_typeDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfVehicle_typeDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vehicle_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


