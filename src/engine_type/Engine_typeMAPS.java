package engine_type;
import java.util.*; 


public class Engine_typeMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static Engine_typeMAPS self = null;
	
	private Engine_typeMAPS()
	{
		
		java_type_map.put("name_bn".toLowerCase(), "String");
		java_type_map.put("name_en".toLowerCase(), "String");

		java_custom_search_map.put("name_bn".toLowerCase(), "String");
		java_custom_search_map.put("name_en".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Engine_typeMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Engine_typeMAPS ();
		}
		return self;
	}
	

}