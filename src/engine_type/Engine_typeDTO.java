package engine_type;
import java.util.*; 


public class Engine_typeDTO {

	public long iD = 0;
    public String nameBn = "";
    public String nameEn = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$Engine_typeDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " isDeleted = " + isDeleted +
            "]";
    }

}