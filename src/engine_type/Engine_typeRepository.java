package engine_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Engine_typeRepository implements Repository {
	Engine_typeDAO engine_typeDAO = new Engine_typeDAO();
	
	
	static Logger logger = Logger.getLogger(Engine_typeRepository.class);
	Map<Long, Engine_typeDTO>mapOfEngine_typeDTOToiD;
	Map<String, Set<Engine_typeDTO> >mapOfEngine_typeDTOTonameBn;
	Map<String, Set<Engine_typeDTO> >mapOfEngine_typeDTOTonameEn;


	static Engine_typeRepository instance = null;  
	private Engine_typeRepository(){
		mapOfEngine_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfEngine_typeDTOTonameBn = new ConcurrentHashMap<>();
		mapOfEngine_typeDTOTonameEn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Engine_typeRepository getInstance(){
		if (instance == null){
			instance = new Engine_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Engine_typeDTO> engine_typeDTOs = engine_typeDAO.getAllEngine_type(reloadAll);
			for(Engine_typeDTO engine_typeDTO : engine_typeDTOs) {
				Engine_typeDTO oldEngine_typeDTO = getEngine_typeDTOByID(engine_typeDTO.iD);
				if( oldEngine_typeDTO != null ) {
					mapOfEngine_typeDTOToiD.remove(oldEngine_typeDTO.iD);
				
					if(mapOfEngine_typeDTOTonameBn.containsKey(oldEngine_typeDTO.nameBn)) {
						mapOfEngine_typeDTOTonameBn.get(oldEngine_typeDTO.nameBn).remove(oldEngine_typeDTO);
					}
					if(mapOfEngine_typeDTOTonameBn.get(oldEngine_typeDTO.nameBn).isEmpty()) {
						mapOfEngine_typeDTOTonameBn.remove(oldEngine_typeDTO.nameBn);
					}
					
					if(mapOfEngine_typeDTOTonameEn.containsKey(oldEngine_typeDTO.nameEn)) {
						mapOfEngine_typeDTOTonameEn.get(oldEngine_typeDTO.nameEn).remove(oldEngine_typeDTO);
					}
					if(mapOfEngine_typeDTOTonameEn.get(oldEngine_typeDTO.nameEn).isEmpty()) {
						mapOfEngine_typeDTOTonameEn.remove(oldEngine_typeDTO.nameEn);
					}
					
					
				}
				if(engine_typeDTO.isDeleted == false) 
				{
					
					mapOfEngine_typeDTOToiD.put(engine_typeDTO.iD, engine_typeDTO);
				
					if( ! mapOfEngine_typeDTOTonameBn.containsKey(engine_typeDTO.nameBn)) {
						mapOfEngine_typeDTOTonameBn.put(engine_typeDTO.nameBn, new HashSet<>());
					}
					mapOfEngine_typeDTOTonameBn.get(engine_typeDTO.nameBn).add(engine_typeDTO);
					
					if( ! mapOfEngine_typeDTOTonameEn.containsKey(engine_typeDTO.nameEn)) {
						mapOfEngine_typeDTOTonameEn.put(engine_typeDTO.nameEn, new HashSet<>());
					}
					mapOfEngine_typeDTOTonameEn.get(engine_typeDTO.nameEn).add(engine_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Engine_typeDTO> getEngine_typeList() {
		List <Engine_typeDTO> engine_types = new ArrayList<Engine_typeDTO>(this.mapOfEngine_typeDTOToiD.values());
		return engine_types;
	}
	
	
	public Engine_typeDTO getEngine_typeDTOByID( long ID){
		return mapOfEngine_typeDTOToiD.get(ID);
	}
	
	
	public List<Engine_typeDTO> getEngine_typeDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfEngine_typeDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Engine_typeDTO> getEngine_typeDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfEngine_typeDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "engine_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


