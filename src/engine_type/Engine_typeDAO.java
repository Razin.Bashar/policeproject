package engine_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class Engine_typeDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"engine_type");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"engine_type");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(Engine_typeDTO engine_typeDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = engine_typeDTO.iD;
		// userDTO.userName = engine_typeDTO.email;
		// userDTO.fullName = engine_typeDTO.name;
		// userDTO.password = engine_typeDTO.password;
		// userDTO.phoneNo = engine_typeDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = engine_typeDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(Engine_typeDTO engine_typeDTO, UserDTO userDTO)
	{
		// userDTO.ID = engine_typeDTO.iD;
		// userDTO.fullName = engine_typeDTO.name;
		// userDTO.phoneNo = engine_typeDTO.phone;
		// userDTO.mailAddress = engine_typeDTO.email;

		return userDTO;
	}
		
		
	
	public void addEngine_type(Engine_typeDTO engine_typeDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			engine_typeDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Engine_type");

			String sql = "INSERT INTO engine_type";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(engine_typeDTO));
			// engine_typeDTO.iD = userDAO.getUserDTOByUsername(engine_typeDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + engine_typeDTO.iD + " in index " + index);
			ps.setObject(index++,engine_typeDTO.iD);
			//System.out.println("Setting object" + engine_typeDTO.nameBn + " in index " + index);
			ps.setObject(index++,engine_typeDTO.nameBn);
			//System.out.println("Setting object" + engine_typeDTO.nameEn + " in index " + index);
			ps.setObject(index++,engine_typeDTO.nameEn);
			//System.out.println("Setting object" + engine_typeDTO.isDeleted + " in index " + index);
			ps.setObject(index++,engine_typeDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Engine_typeRepository.getInstance().reload(false);		
	}
	
	//need another getter for repository
	public Engine_typeDTO getEngine_typeDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Engine_typeDTO engine_typeDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM engine_type";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				engine_typeDTO = new Engine_typeDTO();

				engine_typeDTO.iD = rs.getLong("ID");
				engine_typeDTO.nameBn = rs.getString("name_bn");
				engine_typeDTO.nameEn = rs.getString("name_en");
				engine_typeDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return engine_typeDTO;
	}
	
	public void updateEngine_type(Engine_typeDTO engine_typeDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE engine_type";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "name_bn=?";
			sql += ", ";
			sql += "name_en=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + engine_typeDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + engine_typeDTO.iD + " in index " + index);
			ps.setObject(index++,engine_typeDTO.iD);
			//System.out.println("Setting object" + engine_typeDTO.nameBn + " in index " + index);
			ps.setObject(index++,engine_typeDTO.nameBn);
			//System.out.println("Setting object" + engine_typeDTO.nameEn + " in index " + index);
			ps.setObject(index++,engine_typeDTO.nameEn);
			//System.out.println("Setting object" + engine_typeDTO.isDeleted + " in index " + index);
			ps.setObject(index++,engine_typeDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(engine_typeDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(engine_typeDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Engine_typeRepository.getInstance().reload(false);
	}
	
	public void deleteEngine_typeByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE engine_type";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<Engine_typeDTO> getEngine_typeDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Engine_typeDTO engine_typeDTO = null;
		List<Engine_typeDTO> engine_typeDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM engine_type";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			if(rs.next()){
				engine_typeDTO = new Engine_typeDTO();
				engine_typeDTO.iD = rs.getLong("ID");
				engine_typeDTO.nameBn = rs.getString("name_bn");
				engine_typeDTO.nameEn = rs.getString("name_en");
				engine_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = engine_typeDTO.iD;
				while(i < engine_typeDTOList.size() && engine_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				engine_typeDTOList.add(i,  engine_typeDTO);
				//engine_typeDTOList.add(engine_typeDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return engine_typeDTOList;
	}
	
	public List<Engine_typeDTO> getEngine_typeDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getEngine_typeDTOByColumn(filter);
	}
	
	public List<Engine_typeDTO> getEngine_typeDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getEngine_typeDTOByColumn(filter);
	}
	
	public List<Engine_typeDTO> getEngine_typeDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getEngine_typeDTOByColumn(filter);
	}
		
	
	
	public List<Engine_typeDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Engine_typeDTO engine_typeDTO = null;
		List<Engine_typeDTO> engine_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return engine_typeDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM engine_type";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				engine_typeDTO = new Engine_typeDTO();
				engine_typeDTO.iD = rs.getLong("ID");
				engine_typeDTO.nameBn = rs.getString("name_bn");
				engine_typeDTO.nameEn = rs.getString("name_en");
				engine_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + engine_typeDTO);
				//engine_typeDTOList.add(engine_typeDTO);
				int i = 0;
				long primaryKey = engine_typeDTO.iD;
				while(i < engine_typeDTOList.size() && engine_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				engine_typeDTOList.add(i,  engine_typeDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return engine_typeDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM engine_type";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<Engine_typeDTO> getAllEngine_type (boolean isFirstReload)
    {
		List<Engine_typeDTO> engine_typeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM engine_type";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Engine_typeDTO engine_typeDTO = new Engine_typeDTO();
				engine_typeDTO.iD = rs.getLong("ID");
				engine_typeDTO.nameBn = rs.getString("name_bn");
				engine_typeDTO.nameEn = rs.getString("name_en");
				engine_typeDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = engine_typeDTO.iD;
				while(i < engine_typeDTOList.size() && engine_typeDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				engine_typeDTOList.add(i,  engine_typeDTO);
				//engine_typeDTOList.add(engine_typeDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return engine_typeDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM engine_type";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		Engine_typeDTO engine_typeDTO = new Engine_typeDTO();
		try{

			String sql = "SELECT ID FROM engine_type";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Engine_typeMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Engine_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Engine_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Engine_typeMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	