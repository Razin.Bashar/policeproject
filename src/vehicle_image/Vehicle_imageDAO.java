package vehicle_image;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class Vehicle_imageDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"vehicle_image");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"vehicle_image");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(Vehicle_imageDTO vehicle_imageDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = vehicle_imageDTO.iD;
		// userDTO.userName = vehicle_imageDTO.email;
		// userDTO.fullName = vehicle_imageDTO.name;
		// userDTO.password = vehicle_imageDTO.password;
		// userDTO.phoneNo = vehicle_imageDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = vehicle_imageDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(Vehicle_imageDTO vehicle_imageDTO, UserDTO userDTO)
	{
		// userDTO.ID = vehicle_imageDTO.iD;
		// userDTO.fullName = vehicle_imageDTO.name;
		// userDTO.phoneNo = vehicle_imageDTO.phone;
		// userDTO.mailAddress = vehicle_imageDTO.email;

		return userDTO;
	}
		
		
	
	public void addVehicle_image(Vehicle_imageDTO vehicle_imageDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			vehicle_imageDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Vehicle_image");

			String sql = "INSERT INTO vehicle_image";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(vehicle_imageDTO));
			// vehicle_imageDTO.iD = userDAO.getUserDTOByUsername(vehicle_imageDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + vehicle_imageDTO.iD + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.iD);
			//System.out.println("Setting object" + vehicle_imageDTO.reportId + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.reportId);
			//System.out.println("Setting object" + vehicle_imageDTO.name + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.name);
			//System.out.println("Setting object" + vehicle_imageDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Vehicle_imageRepository.getInstance().reload(false);		
	}
	
	//need another getter for repository
	public Vehicle_imageDTO getVehicle_imageDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_imageDTO vehicle_imageDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_image";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				vehicle_imageDTO = new Vehicle_imageDTO();

				vehicle_imageDTO.iD = rs.getLong("ID");
				vehicle_imageDTO.reportId = rs.getLong("report_id");
				vehicle_imageDTO.name = rs.getString("name");
				vehicle_imageDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_imageDTO;
	}
	
	public void updateVehicle_image(Vehicle_imageDTO vehicle_imageDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE vehicle_image";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "report_id=?";
			sql += ", ";
			sql += "name=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + vehicle_imageDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + vehicle_imageDTO.iD + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.iD);
			//System.out.println("Setting object" + vehicle_imageDTO.reportId + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.reportId);
			//System.out.println("Setting object" + vehicle_imageDTO.name + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.name);
			//System.out.println("Setting object" + vehicle_imageDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicle_imageDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(vehicle_imageDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(vehicle_imageDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//Vehicle_imageRepository.getInstance().reload(false);
	}
	
	public void deleteVehicle_imageByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE vehicle_image";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<Vehicle_imageDTO> getVehicle_imageDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_imageDTO vehicle_imageDTO = null;
		List<Vehicle_imageDTO> vehicle_imageDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_image";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			while(rs.next()){
				vehicle_imageDTO = new Vehicle_imageDTO();
				vehicle_imageDTO.iD = rs.getLong("ID");
				vehicle_imageDTO.reportId = rs.getLong("report_id");
				vehicle_imageDTO.name = rs.getString("name");
				vehicle_imageDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicle_imageDTO.iD;
				while(i < vehicle_imageDTOList.size() && vehicle_imageDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_imageDTOList.add(i,  vehicle_imageDTO);
				//vehicle_imageDTOList.add(vehicle_imageDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_imageDTOList;
	}
	
	public List<Vehicle_imageDTO> getVehicle_imageDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getVehicle_imageDTOByColumn(filter);
	}
	
	public List<Vehicle_imageDTO> getVehicle_imageDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getVehicle_imageDTOByColumn(filter);
	}
	
	public List<Vehicle_imageDTO> getVehicle_imageDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getVehicle_imageDTOByColumn(filter);
	}
		
	
	
	public List<Vehicle_imageDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Vehicle_imageDTO vehicle_imageDTO = null;
		List<Vehicle_imageDTO> vehicle_imageDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return vehicle_imageDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle_image";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				vehicle_imageDTO = new Vehicle_imageDTO();
				vehicle_imageDTO.iD = rs.getLong("ID");
				vehicle_imageDTO.reportId = rs.getLong("report_id");
				vehicle_imageDTO.name = rs.getString("name");
				vehicle_imageDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + vehicle_imageDTO);
				//vehicle_imageDTOList.add(vehicle_imageDTO);
				int i = 0;
				long primaryKey = vehicle_imageDTO.iD;
				while(i < vehicle_imageDTOList.size() && vehicle_imageDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_imageDTOList.add(i,  vehicle_imageDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicle_imageDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM vehicle_image";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<Vehicle_imageDTO> getAllVehicle_image (boolean isFirstReload)
    {
		List<Vehicle_imageDTO> vehicle_imageDTOList = new ArrayList<>();

		String sql = "SELECT * FROM vehicle_image";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Vehicle_imageDTO vehicle_imageDTO = new Vehicle_imageDTO();
				vehicle_imageDTO.iD = rs.getLong("ID");
				vehicle_imageDTO.reportId = rs.getLong("report_id");
				vehicle_imageDTO.name = rs.getString("name");
				vehicle_imageDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicle_imageDTO.iD;
				while(i < vehicle_imageDTOList.size() && vehicle_imageDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicle_imageDTOList.add(i,  vehicle_imageDTO);
				//vehicle_imageDTOList.add(vehicle_imageDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return vehicle_imageDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM vehicle_image";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		Vehicle_imageDTO vehicle_imageDTO = new Vehicle_imageDTO();
		try{

			String sql = "SELECT ID FROM vehicle_image";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Vehicle_imageMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Vehicle_imageMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !Vehicle_imageMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Vehicle_imageMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	