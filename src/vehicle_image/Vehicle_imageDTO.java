package vehicle_image;
import java.util.*; 


public class Vehicle_imageDTO {

	public long iD = 0;
	public long reportId = 0;
    public String name = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$Vehicle_imageDTO[" +
            " iD = " + iD +
            " reportId = " + reportId +
            " name = " + name +
            " isDeleted = " + isDeleted +
            "]";
    }

}