package vehicle_image;
import java.util.*; 


public class Vehicle_imageMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static Vehicle_imageMAPS self = null;
	
	private Vehicle_imageMAPS()
	{
		
		java_type_map.put("report_id".toLowerCase(), "Long");
		java_type_map.put("name".toLowerCase(), "String");

		java_custom_search_map.put("report_id".toLowerCase(), "Long");
		java_custom_search_map.put("name".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("reportId".toLowerCase(), "reportId".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Vehicle_imageMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Vehicle_imageMAPS ();
		}
		return self;
	}
	

}