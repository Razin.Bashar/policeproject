package vehicle_image;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vehicle_imageRepository implements Repository {
	Vehicle_imageDAO vehicle_imageDAO = new Vehicle_imageDAO();
	
	
	static Logger logger = Logger.getLogger(Vehicle_imageRepository.class);
	Map<Long, Vehicle_imageDTO>mapOfVehicle_imageDTOToiD;
	Map<Long, Set<Vehicle_imageDTO> >mapOfVehicle_imageDTOToreportId;
	Map<String, Set<Vehicle_imageDTO> >mapOfVehicle_imageDTOToname;


	static Vehicle_imageRepository instance = null;  
	private Vehicle_imageRepository(){
		mapOfVehicle_imageDTOToiD = new ConcurrentHashMap<>();
		mapOfVehicle_imageDTOToreportId = new ConcurrentHashMap<>();
		mapOfVehicle_imageDTOToname = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vehicle_imageRepository getInstance(){
		if (instance == null){
			instance = new Vehicle_imageRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Vehicle_imageDTO> vehicle_imageDTOs = vehicle_imageDAO.getAllVehicle_image(reloadAll);
			for(Vehicle_imageDTO vehicle_imageDTO : vehicle_imageDTOs) {
				Vehicle_imageDTO oldVehicle_imageDTO = getVehicle_imageDTOByID(vehicle_imageDTO.iD);
				if( oldVehicle_imageDTO != null ) {
					mapOfVehicle_imageDTOToiD.remove(oldVehicle_imageDTO.iD);
				
					if(mapOfVehicle_imageDTOToreportId.containsKey(oldVehicle_imageDTO.reportId)) {
						mapOfVehicle_imageDTOToreportId.get(oldVehicle_imageDTO.reportId).remove(oldVehicle_imageDTO);
					}
					if(mapOfVehicle_imageDTOToreportId.get(oldVehicle_imageDTO.reportId).isEmpty()) {
						mapOfVehicle_imageDTOToreportId.remove(oldVehicle_imageDTO.reportId);
					}
					
					if(mapOfVehicle_imageDTOToname.containsKey(oldVehicle_imageDTO.name)) {
						mapOfVehicle_imageDTOToname.get(oldVehicle_imageDTO.name).remove(oldVehicle_imageDTO);
					}
					if(mapOfVehicle_imageDTOToname.get(oldVehicle_imageDTO.name).isEmpty()) {
						mapOfVehicle_imageDTOToname.remove(oldVehicle_imageDTO.name);
					}
					
					
				}
				if(vehicle_imageDTO.isDeleted == false) 
				{
					
					mapOfVehicle_imageDTOToiD.put(vehicle_imageDTO.iD, vehicle_imageDTO);
				
					if( ! mapOfVehicle_imageDTOToreportId.containsKey(vehicle_imageDTO.reportId)) {
						mapOfVehicle_imageDTOToreportId.put(vehicle_imageDTO.reportId, new HashSet<>());
					}
					mapOfVehicle_imageDTOToreportId.get(vehicle_imageDTO.reportId).add(vehicle_imageDTO);
					
					if( ! mapOfVehicle_imageDTOToname.containsKey(vehicle_imageDTO.name)) {
						mapOfVehicle_imageDTOToname.put(vehicle_imageDTO.name, new HashSet<>());
					}
					mapOfVehicle_imageDTOToname.get(vehicle_imageDTO.name).add(vehicle_imageDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vehicle_imageDTO> getVehicle_imageList() {
		List <Vehicle_imageDTO> vehicle_images = new ArrayList<Vehicle_imageDTO>(this.mapOfVehicle_imageDTOToiD.values());
		return vehicle_images;
	}
	
	
	public Vehicle_imageDTO getVehicle_imageDTOByID( long ID){
		return mapOfVehicle_imageDTOToiD.get(ID);
	}
	
	
	public List<Vehicle_imageDTO> getVehicle_imageDTOByreport_id(long report_id) {
		return new ArrayList<>( mapOfVehicle_imageDTOToreportId.getOrDefault(report_id,new HashSet<>()));
	}
	
	
	public List<Vehicle_imageDTO> getVehicle_imageDTOByname(String name) {
		return new ArrayList<>( mapOfVehicle_imageDTOToname.getOrDefault(name,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vehicle_image";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


