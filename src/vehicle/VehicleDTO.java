package vehicle;
import java.util.*; 


public class VehicleDTO {

	public long iD = 0;
	public int typeId = 0;
    public String modelName = "";
    public String color = "";
    public String engineNumber = "";
	public long engineType = 0;
    public String chassisNumber = "";
	public int engineCc = 0;
    public String registrationNumber = "";
    public String manufacturer = "";
	public int manufacturingYear = 0;
    public String moreDetails = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$VehicleDTO[" +
            " iD = " + iD +
            " typeId = " + typeId +
            " modelName = " + modelName +
            " color = " + color +
            " engineNumber = " + engineNumber +
            " engineType = " + engineType +
            " chassisNumber = " + chassisNumber +
            " engineCc = " + engineCc +
            " registrationNumber = " + registrationNumber +
            " manufacturer = " + manufacturer +
            " manufacturingYear = " + manufacturingYear +
            " moreDetails = " + moreDetails +
            " isDeleted = " + isDeleted +
            "]";
    }

}