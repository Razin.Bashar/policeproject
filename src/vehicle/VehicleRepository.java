package vehicle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class VehicleRepository implements Repository {
	VehicleDAO vehicleDAO = new VehicleDAO();
	
	
	static Logger logger = Logger.getLogger(VehicleRepository.class);
	Map<Long, VehicleDTO>mapOfVehicleDTOToiD;
	Map<Integer, Set<VehicleDTO> >mapOfVehicleDTOTotypeId;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOTomodelName;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOTocolor;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOToengineNumber;
	Map<Long, Set<VehicleDTO> >mapOfVehicleDTOToengineType;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOTochassisNumber;
	Map<Integer, Set<VehicleDTO> >mapOfVehicleDTOToengineCc;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOToregistrationNumber;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOTomanufacturer;
	Map<Integer, Set<VehicleDTO> >mapOfVehicleDTOTomanufacturingYear;
	Map<String, Set<VehicleDTO> >mapOfVehicleDTOTomoreDetails;


	static VehicleRepository instance = null;  
	private VehicleRepository(){
		mapOfVehicleDTOToiD = new ConcurrentHashMap<>();
		mapOfVehicleDTOTotypeId = new ConcurrentHashMap<>();
		mapOfVehicleDTOTomodelName = new ConcurrentHashMap<>();
		mapOfVehicleDTOTocolor = new ConcurrentHashMap<>();
		mapOfVehicleDTOToengineNumber = new ConcurrentHashMap<>();
		mapOfVehicleDTOToengineType = new ConcurrentHashMap<>();
		mapOfVehicleDTOTochassisNumber = new ConcurrentHashMap<>();
		mapOfVehicleDTOToengineCc = new ConcurrentHashMap<>();
		mapOfVehicleDTOToregistrationNumber = new ConcurrentHashMap<>();
		mapOfVehicleDTOTomanufacturer = new ConcurrentHashMap<>();
		mapOfVehicleDTOTomanufacturingYear = new ConcurrentHashMap<>();
		mapOfVehicleDTOTomoreDetails = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static VehicleRepository getInstance(){
		if (instance == null){
			instance = new VehicleRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<VehicleDTO> vehicleDTOs = vehicleDAO.getAllVehicle(reloadAll);
			for(VehicleDTO vehicleDTO : vehicleDTOs) {
				VehicleDTO oldVehicleDTO = getVehicleDTOByID(vehicleDTO.iD);
				if( oldVehicleDTO != null ) {
					mapOfVehicleDTOToiD.remove(oldVehicleDTO.iD);
				
					if(mapOfVehicleDTOTotypeId.containsKey(oldVehicleDTO.typeId)) {
						mapOfVehicleDTOTotypeId.get(oldVehicleDTO.typeId).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTotypeId.get(oldVehicleDTO.typeId).isEmpty()) {
						mapOfVehicleDTOTotypeId.remove(oldVehicleDTO.typeId);
					}
					
					if(mapOfVehicleDTOTomodelName.containsKey(oldVehicleDTO.modelName)) {
						mapOfVehicleDTOTomodelName.get(oldVehicleDTO.modelName).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTomodelName.get(oldVehicleDTO.modelName).isEmpty()) {
						mapOfVehicleDTOTomodelName.remove(oldVehicleDTO.modelName);
					}
					
					if(mapOfVehicleDTOTocolor.containsKey(oldVehicleDTO.color)) {
						mapOfVehicleDTOTocolor.get(oldVehicleDTO.color).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTocolor.get(oldVehicleDTO.color).isEmpty()) {
						mapOfVehicleDTOTocolor.remove(oldVehicleDTO.color);
					}
					
					if(mapOfVehicleDTOToengineNumber.containsKey(oldVehicleDTO.engineNumber)) {
						mapOfVehicleDTOToengineNumber.get(oldVehicleDTO.engineNumber).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOToengineNumber.get(oldVehicleDTO.engineNumber).isEmpty()) {
						mapOfVehicleDTOToengineNumber.remove(oldVehicleDTO.engineNumber);
					}
					
					if(mapOfVehicleDTOToengineType.containsKey(oldVehicleDTO.engineType)) {
						mapOfVehicleDTOToengineType.get(oldVehicleDTO.engineType).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOToengineType.get(oldVehicleDTO.engineType).isEmpty()) {
						mapOfVehicleDTOToengineType.remove(oldVehicleDTO.engineType);
					}
					
					if(mapOfVehicleDTOTochassisNumber.containsKey(oldVehicleDTO.chassisNumber)) {
						mapOfVehicleDTOTochassisNumber.get(oldVehicleDTO.chassisNumber).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTochassisNumber.get(oldVehicleDTO.chassisNumber).isEmpty()) {
						mapOfVehicleDTOTochassisNumber.remove(oldVehicleDTO.chassisNumber);
					}
					
					if(mapOfVehicleDTOToengineCc.containsKey(oldVehicleDTO.engineCc)) {
						mapOfVehicleDTOToengineCc.get(oldVehicleDTO.engineCc).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOToengineCc.get(oldVehicleDTO.engineCc).isEmpty()) {
						mapOfVehicleDTOToengineCc.remove(oldVehicleDTO.engineCc);
					}
					
					if(mapOfVehicleDTOToregistrationNumber.containsKey(oldVehicleDTO.registrationNumber)) {
						mapOfVehicleDTOToregistrationNumber.get(oldVehicleDTO.registrationNumber).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOToregistrationNumber.get(oldVehicleDTO.registrationNumber).isEmpty()) {
						mapOfVehicleDTOToregistrationNumber.remove(oldVehicleDTO.registrationNumber);
					}
					
					if(mapOfVehicleDTOTomanufacturer.containsKey(oldVehicleDTO.manufacturer)) {
						mapOfVehicleDTOTomanufacturer.get(oldVehicleDTO.manufacturer).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTomanufacturer.get(oldVehicleDTO.manufacturer).isEmpty()) {
						mapOfVehicleDTOTomanufacturer.remove(oldVehicleDTO.manufacturer);
					}
					
					if(mapOfVehicleDTOTomanufacturingYear.containsKey(oldVehicleDTO.manufacturingYear)) {
						mapOfVehicleDTOTomanufacturingYear.get(oldVehicleDTO.manufacturingYear).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTomanufacturingYear.get(oldVehicleDTO.manufacturingYear).isEmpty()) {
						mapOfVehicleDTOTomanufacturingYear.remove(oldVehicleDTO.manufacturingYear);
					}
					
					if(mapOfVehicleDTOTomoreDetails.containsKey(oldVehicleDTO.moreDetails)) {
						mapOfVehicleDTOTomoreDetails.get(oldVehicleDTO.moreDetails).remove(oldVehicleDTO);
					}
					if(mapOfVehicleDTOTomoreDetails.get(oldVehicleDTO.moreDetails).isEmpty()) {
						mapOfVehicleDTOTomoreDetails.remove(oldVehicleDTO.moreDetails);
					}
					
					
				}
				if(vehicleDTO.isDeleted == false) 
				{
					
					mapOfVehicleDTOToiD.put(vehicleDTO.iD, vehicleDTO);
				
					if( ! mapOfVehicleDTOTotypeId.containsKey(vehicleDTO.typeId)) {
						mapOfVehicleDTOTotypeId.put(vehicleDTO.typeId, new HashSet<>());
					}
					mapOfVehicleDTOTotypeId.get(vehicleDTO.typeId).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTomodelName.containsKey(vehicleDTO.modelName)) {
						mapOfVehicleDTOTomodelName.put(vehicleDTO.modelName, new HashSet<>());
					}
					mapOfVehicleDTOTomodelName.get(vehicleDTO.modelName).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTocolor.containsKey(vehicleDTO.color)) {
						mapOfVehicleDTOTocolor.put(vehicleDTO.color, new HashSet<>());
					}
					mapOfVehicleDTOTocolor.get(vehicleDTO.color).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOToengineNumber.containsKey(vehicleDTO.engineNumber)) {
						mapOfVehicleDTOToengineNumber.put(vehicleDTO.engineNumber, new HashSet<>());
					}
					mapOfVehicleDTOToengineNumber.get(vehicleDTO.engineNumber).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOToengineType.containsKey(vehicleDTO.engineType)) {
						mapOfVehicleDTOToengineType.put(vehicleDTO.engineType, new HashSet<>());
					}
					mapOfVehicleDTOToengineType.get(vehicleDTO.engineType).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTochassisNumber.containsKey(vehicleDTO.chassisNumber)) {
						mapOfVehicleDTOTochassisNumber.put(vehicleDTO.chassisNumber, new HashSet<>());
					}
					mapOfVehicleDTOTochassisNumber.get(vehicleDTO.chassisNumber).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOToengineCc.containsKey(vehicleDTO.engineCc)) {
						mapOfVehicleDTOToengineCc.put(vehicleDTO.engineCc, new HashSet<>());
					}
					mapOfVehicleDTOToengineCc.get(vehicleDTO.engineCc).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOToregistrationNumber.containsKey(vehicleDTO.registrationNumber)) {
						mapOfVehicleDTOToregistrationNumber.put(vehicleDTO.registrationNumber, new HashSet<>());
					}
					mapOfVehicleDTOToregistrationNumber.get(vehicleDTO.registrationNumber).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTomanufacturer.containsKey(vehicleDTO.manufacturer)) {
						mapOfVehicleDTOTomanufacturer.put(vehicleDTO.manufacturer, new HashSet<>());
					}
					mapOfVehicleDTOTomanufacturer.get(vehicleDTO.manufacturer).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTomanufacturingYear.containsKey(vehicleDTO.manufacturingYear)) {
						mapOfVehicleDTOTomanufacturingYear.put(vehicleDTO.manufacturingYear, new HashSet<>());
					}
					mapOfVehicleDTOTomanufacturingYear.get(vehicleDTO.manufacturingYear).add(vehicleDTO);
					
					if( ! mapOfVehicleDTOTomoreDetails.containsKey(vehicleDTO.moreDetails)) {
						mapOfVehicleDTOTomoreDetails.put(vehicleDTO.moreDetails, new HashSet<>());
					}
					mapOfVehicleDTOTomoreDetails.get(vehicleDTO.moreDetails).add(vehicleDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<VehicleDTO> getVehicleList() {
		List <VehicleDTO> vehicles = new ArrayList<VehicleDTO>(this.mapOfVehicleDTOToiD.values());
		return vehicles;
	}
	
	
	public VehicleDTO getVehicleDTOByID( long ID){
		return mapOfVehicleDTOToiD.get(ID);
	}
	
	
	public List<VehicleDTO> getVehicleDTOBytype_id(int type_id) {
		return new ArrayList<>( mapOfVehicleDTOTotypeId.getOrDefault(type_id,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBymodel_name(String model_name) {
		return new ArrayList<>( mapOfVehicleDTOTomodelName.getOrDefault(model_name,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBycolor(String color) {
		return new ArrayList<>( mapOfVehicleDTOTocolor.getOrDefault(color,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOByengine_number(String engine_number) {
		return new ArrayList<>( mapOfVehicleDTOToengineNumber.getOrDefault(engine_number,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOByengine_type(long engine_type) {
		return new ArrayList<>( mapOfVehicleDTOToengineType.getOrDefault(engine_type,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBychassis_number(String chassis_number) {
		return new ArrayList<>( mapOfVehicleDTOTochassisNumber.getOrDefault(chassis_number,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOByengine_cc(int engine_cc) {
		return new ArrayList<>( mapOfVehicleDTOToengineCc.getOrDefault(engine_cc,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOByregistration_number(String registration_number) {
		return new ArrayList<>( mapOfVehicleDTOToregistrationNumber.getOrDefault(registration_number,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBymanufacturer(String manufacturer) {
		return new ArrayList<>( mapOfVehicleDTOTomanufacturer.getOrDefault(manufacturer,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBymanufacturing_year(int manufacturing_year) {
		return new ArrayList<>( mapOfVehicleDTOTomanufacturingYear.getOrDefault(manufacturing_year,new HashSet<>()));
	}
	
	
	public List<VehicleDTO> getVehicleDTOBymore_details(String more_details) {
		return new ArrayList<>( mapOfVehicleDTOTomoreDetails.getOrDefault(more_details,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vehicle";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


