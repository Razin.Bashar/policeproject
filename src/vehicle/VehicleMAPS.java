package vehicle;
import java.util.*; 


public class VehicleMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static VehicleMAPS self = null;
	
	private VehicleMAPS()
	{
		
		java_type_map.put("type_id".toLowerCase(), "Integer");
		java_type_map.put("model_name".toLowerCase(), "String");
		java_type_map.put("color".toLowerCase(), "String");
		java_type_map.put("engine_number".toLowerCase(), "String");
		java_type_map.put("engine_type".toLowerCase(), "Long");
		java_type_map.put("chassis_number".toLowerCase(), "String");
		java_type_map.put("engine_cc".toLowerCase(), "Integer");
		java_type_map.put("registration_number".toLowerCase(), "String");
		java_type_map.put("manufacturer".toLowerCase(), "String");
		java_type_map.put("manufacturing_year".toLowerCase(), "Integer");
		java_type_map.put("more_details".toLowerCase(), "String");

		java_custom_search_map.put("type_id".toLowerCase(), "Integer");
		java_custom_search_map.put("model_name".toLowerCase(), "String");
		java_custom_search_map.put("color".toLowerCase(), "String");
		java_custom_search_map.put("engine_number".toLowerCase(), "String");
		java_custom_search_map.put("engine_type".toLowerCase(), "Long");
		java_custom_search_map.put("chassis_number".toLowerCase(), "String");
		java_custom_search_map.put("engine_cc".toLowerCase(), "Integer");
		java_custom_search_map.put("registration_number".toLowerCase(), "String");
		java_custom_search_map.put("manufacturer".toLowerCase(), "String");
		java_custom_search_map.put("manufacturing_year".toLowerCase(), "Integer");
		java_custom_search_map.put("more_details".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("typeId".toLowerCase(), "typeId".toLowerCase());
		java_DTO_map.put("modelName".toLowerCase(), "modelName".toLowerCase());
		java_DTO_map.put("color".toLowerCase(), "color".toLowerCase());
		java_DTO_map.put("engineNumber".toLowerCase(), "engineNumber".toLowerCase());
		java_DTO_map.put("engineType".toLowerCase(), "engineType".toLowerCase());
		java_DTO_map.put("chassisNumber".toLowerCase(), "chassisNumber".toLowerCase());
		java_DTO_map.put("engineCc".toLowerCase(), "engineCc".toLowerCase());
		java_DTO_map.put("registrationNumber".toLowerCase(), "registrationNumber".toLowerCase());
		java_DTO_map.put("manufacturer".toLowerCase(), "manufacturer".toLowerCase());
		java_DTO_map.put("manufacturingYear".toLowerCase(), "manufacturingYear".toLowerCase());
		java_DTO_map.put("moreDetails".toLowerCase(), "moreDetails".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static VehicleMAPS GetInstance()
	{
		if(self == null)
		{
			self = new VehicleMAPS ();
		}
		return self;
	}
	

}