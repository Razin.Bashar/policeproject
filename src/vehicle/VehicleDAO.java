package vehicle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class VehicleDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"vehicle");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"vehicle");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(VehicleDTO vehicleDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = vehicleDTO.iD;
		// userDTO.userName = vehicleDTO.email;
		// userDTO.fullName = vehicleDTO.name;
		// userDTO.password = vehicleDTO.password;
		// userDTO.phoneNo = vehicleDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = vehicleDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(VehicleDTO vehicleDTO, UserDTO userDTO)
	{
		// userDTO.ID = vehicleDTO.iD;
		// userDTO.fullName = vehicleDTO.name;
		// userDTO.phoneNo = vehicleDTO.phone;
		// userDTO.mailAddress = vehicleDTO.email;

		return userDTO;
	}
		
		
	
	public long addVehicle(VehicleDTO vehicleDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			vehicleDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Vehicle");

			String sql = "INSERT INTO vehicle";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "type_id";
			sql += ", ";
			sql += "model_name";
			sql += ", ";
			sql += "color";
			sql += ", ";
			sql += "engine_number";
			sql += ", ";
			sql += "engine_type";
			sql += ", ";
			sql += "chassis_number";
			sql += ", ";
			sql += "engine_cc";
			sql += ", ";
			sql += "registration_number";
			sql += ", ";
			sql += "manufacturer";
			sql += ", ";
			sql += "manufacturing_year";
			sql += ", ";
			sql += "more_details";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(vehicleDTO));
			// vehicleDTO.iD = userDAO.getUserDTOByUsername(vehicleDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + vehicleDTO.iD + " in index " + index);
			ps.setObject(index++,vehicleDTO.iD);
			//System.out.println("Setting object" + vehicleDTO.typeId + " in index " + index);
			ps.setObject(index++,vehicleDTO.typeId);
			//System.out.println("Setting object" + vehicleDTO.modelName + " in index " + index);
			ps.setObject(index++,vehicleDTO.modelName);
			//System.out.println("Setting object" + vehicleDTO.color + " in index " + index);
			ps.setObject(index++,vehicleDTO.color);
			//System.out.println("Setting object" + vehicleDTO.engineNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineNumber);
			//System.out.println("Setting object" + vehicleDTO.engineType + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineType);
			//System.out.println("Setting object" + vehicleDTO.chassisNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.chassisNumber);
			//System.out.println("Setting object" + vehicleDTO.engineCc + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineCc);
			//System.out.println("Setting object" + vehicleDTO.registrationNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.registrationNumber);
			//System.out.println("Setting object" + vehicleDTO.manufacturer + " in index " + index);
			ps.setObject(index++,vehicleDTO.manufacturer);
			//System.out.println("Setting object" + vehicleDTO.manufacturingYear + " in index " + index);
			ps.setObject(index++,vehicleDTO.manufacturingYear);
			//System.out.println("Setting object" + vehicleDTO.moreDetails + " in index " + index);
			ps.setObject(index++,vehicleDTO.moreDetails);
			//System.out.println("Setting object" + vehicleDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicleDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//VehicleRepository.getInstance().reload(false);	
		return vehicleDTO.iD;
	}
	
	//need another getter for repository
	public VehicleDTO getVehicleDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VehicleDTO vehicleDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "type_id";
			sql += ", ";
			sql += "model_name";
			sql += ", ";
			sql += "color";
			sql += ", ";
			sql += "engine_number";
			sql += ", ";
			sql += "engine_type";
			sql += ", ";
			sql += "chassis_number";
			sql += ", ";
			sql += "engine_cc";
			sql += ", ";
			sql += "registration_number";
			sql += ", ";
			sql += "manufacturer";
			sql += ", ";
			sql += "manufacturing_year";
			sql += ", ";
			sql += "more_details";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				vehicleDTO = new VehicleDTO();

				vehicleDTO.iD = rs.getLong("ID");
				vehicleDTO.typeId = rs.getInt("type_id");
				vehicleDTO.modelName = rs.getString("model_name");
				vehicleDTO.color = rs.getString("color");
				vehicleDTO.engineNumber = rs.getString("engine_number");
				vehicleDTO.engineType = rs.getLong("engine_type");
				vehicleDTO.chassisNumber = rs.getString("chassis_number");
				vehicleDTO.engineCc = rs.getInt("engine_cc");
				vehicleDTO.registrationNumber = rs.getString("registration_number");
				vehicleDTO.manufacturer = rs.getString("manufacturer");
				vehicleDTO.manufacturingYear = rs.getInt("manufacturing_year");
				vehicleDTO.moreDetails = rs.getString("more_details");
				vehicleDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicleDTO;
	}
	
	public void updateVehicle(VehicleDTO vehicleDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE vehicle";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "type_id=?";
			sql += ", ";
			sql += "model_name=?";
			sql += ", ";
			sql += "color=?";
			sql += ", ";
			sql += "engine_number=?";
			sql += ", ";
			sql += "engine_type=?";
			sql += ", ";
			sql += "chassis_number=?";
			sql += ", ";
			sql += "engine_cc=?";
			sql += ", ";
			sql += "registration_number=?";
			sql += ", ";
			sql += "manufacturer=?";
			sql += ", ";
			sql += "manufacturing_year=?";
			sql += ", ";
			sql += "more_details=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + vehicleDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + vehicleDTO.iD + " in index " + index);
			ps.setObject(index++,vehicleDTO.iD);
			//System.out.println("Setting object" + vehicleDTO.typeId + " in index " + index);
			ps.setObject(index++,vehicleDTO.typeId);
			//System.out.println("Setting object" + vehicleDTO.modelName + " in index " + index);
			ps.setObject(index++,vehicleDTO.modelName);
			//System.out.println("Setting object" + vehicleDTO.color + " in index " + index);
			ps.setObject(index++,vehicleDTO.color);
			//System.out.println("Setting object" + vehicleDTO.engineNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineNumber);
			//System.out.println("Setting object" + vehicleDTO.engineType + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineType);
			//System.out.println("Setting object" + vehicleDTO.chassisNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.chassisNumber);
			//System.out.println("Setting object" + vehicleDTO.engineCc + " in index " + index);
			ps.setObject(index++,vehicleDTO.engineCc);
			//System.out.println("Setting object" + vehicleDTO.registrationNumber + " in index " + index);
			ps.setObject(index++,vehicleDTO.registrationNumber);
			//System.out.println("Setting object" + vehicleDTO.manufacturer + " in index " + index);
			ps.setObject(index++,vehicleDTO.manufacturer);
			//System.out.println("Setting object" + vehicleDTO.manufacturingYear + " in index " + index);
			ps.setObject(index++,vehicleDTO.manufacturingYear);
			//System.out.println("Setting object" + vehicleDTO.moreDetails + " in index " + index);
			ps.setObject(index++,vehicleDTO.moreDetails);
			//System.out.println("Setting object" + vehicleDTO.isDeleted + " in index " + index);
			ps.setObject(index++,vehicleDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(vehicleDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(vehicleDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//VehicleRepository.getInstance().reload(false);
	}
	
	public void deleteVehicleByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE vehicle";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<VehicleDTO> getVehicleDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VehicleDTO vehicleDTO = null;
		List<VehicleDTO> vehicleDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "type_id";
			sql += ", ";
			sql += "model_name";
			sql += ", ";
			sql += "color";
			sql += ", ";
			sql += "engine_number";
			sql += ", ";
			sql += "engine_type";
			sql += ", ";
			sql += "chassis_number";
			sql += ", ";
			sql += "engine_cc";
			sql += ", ";
			sql += "registration_number";
			sql += ", ";
			sql += "manufacturer";
			sql += ", ";
			sql += "manufacturing_year";
			sql += ", ";
			sql += "more_details";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			if(rs.next()){
				vehicleDTO = new VehicleDTO();
				vehicleDTO.iD = rs.getLong("ID");
				vehicleDTO.typeId = rs.getInt("type_id");
				vehicleDTO.modelName = rs.getString("model_name");
				vehicleDTO.color = rs.getString("color");
				vehicleDTO.engineNumber = rs.getString("engine_number");
				vehicleDTO.engineType = rs.getLong("engine_type");
				vehicleDTO.chassisNumber = rs.getString("chassis_number");
				vehicleDTO.engineCc = rs.getInt("engine_cc");
				vehicleDTO.registrationNumber = rs.getString("registration_number");
				vehicleDTO.manufacturer = rs.getString("manufacturer");
				vehicleDTO.manufacturingYear = rs.getInt("manufacturing_year");
				vehicleDTO.moreDetails = rs.getString("more_details");
				vehicleDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicleDTO.iD;
				while(i < vehicleDTOList.size() && vehicleDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicleDTOList.add(i,  vehicleDTO);
				//vehicleDTOList.add(vehicleDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicleDTOList;
	}
	
	public List<VehicleDTO> getVehicleDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getVehicleDTOByColumn(filter);
	}
	
	public List<VehicleDTO> getVehicleDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getVehicleDTOByColumn(filter);
	}
	
	public List<VehicleDTO> getVehicleDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getVehicleDTOByColumn(filter);
	}
		
	
	
	public List<VehicleDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VehicleDTO vehicleDTO = null;
		List<VehicleDTO> vehicleDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return vehicleDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "type_id";
			sql += ", ";
			sql += "model_name";
			sql += ", ";
			sql += "color";
			sql += ", ";
			sql += "engine_number";
			sql += ", ";
			sql += "engine_type";
			sql += ", ";
			sql += "chassis_number";
			sql += ", ";
			sql += "engine_cc";
			sql += ", ";
			sql += "registration_number";
			sql += ", ";
			sql += "manufacturer";
			sql += ", ";
			sql += "manufacturing_year";
			sql += ", ";
			sql += "more_details";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM vehicle";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				vehicleDTO = new VehicleDTO();
				vehicleDTO.iD = rs.getLong("ID");
				vehicleDTO.typeId = rs.getInt("type_id");
				vehicleDTO.modelName = rs.getString("model_name");
				vehicleDTO.color = rs.getString("color");
				vehicleDTO.engineNumber = rs.getString("engine_number");
				vehicleDTO.engineType = rs.getLong("engine_type");
				vehicleDTO.chassisNumber = rs.getString("chassis_number");
				vehicleDTO.engineCc = rs.getInt("engine_cc");
				vehicleDTO.registrationNumber = rs.getString("registration_number");
				vehicleDTO.manufacturer = rs.getString("manufacturer");
				vehicleDTO.manufacturingYear = rs.getInt("manufacturing_year");
				vehicleDTO.moreDetails = rs.getString("more_details");
				vehicleDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + vehicleDTO);
				//vehicleDTOList.add(vehicleDTO);
				int i = 0;
				long primaryKey = vehicleDTO.iD;
				while(i < vehicleDTOList.size() && vehicleDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicleDTOList.add(i,  vehicleDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return vehicleDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM vehicle";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<VehicleDTO> getAllVehicle (boolean isFirstReload)
    {
		List<VehicleDTO> vehicleDTOList = new ArrayList<>();

		String sql = "SELECT * FROM vehicle";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				VehicleDTO vehicleDTO = new VehicleDTO();
				vehicleDTO.iD = rs.getLong("ID");
				vehicleDTO.typeId = rs.getInt("type_id");
				vehicleDTO.modelName = rs.getString("model_name");
				vehicleDTO.color = rs.getString("color");
				vehicleDTO.engineNumber = rs.getString("engine_number");
				vehicleDTO.engineType = rs.getLong("engine_type");
				vehicleDTO.chassisNumber = rs.getString("chassis_number");
				vehicleDTO.engineCc = rs.getInt("engine_cc");
				vehicleDTO.registrationNumber = rs.getString("registration_number");
				vehicleDTO.manufacturer = rs.getString("manufacturer");
				vehicleDTO.manufacturingYear = rs.getInt("manufacturing_year");
				vehicleDTO.moreDetails = rs.getString("more_details");
				vehicleDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = vehicleDTO.iD;
				while(i < vehicleDTOList.size() && vehicleDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				vehicleDTOList.add(i,  vehicleDTO);
				//vehicleDTOList.add(vehicleDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return vehicleDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM vehicle";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		VehicleDTO vehicleDTO = new VehicleDTO();
		try{

			String sql = "SELECT ID FROM vehicle";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = VehicleMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(VehicleMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !VehicleMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(VehicleMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	