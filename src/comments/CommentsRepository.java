package comments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class CommentsRepository implements Repository {
	CommentsDAO commentsDAO = new CommentsDAO();
	
	
	static Logger logger = Logger.getLogger(CommentsRepository.class);
	Map<Long, CommentsDTO>mapOfCommentsDTOToiD;
	Map<Long, Set<CommentsDTO> >mapOfCommentsDTOToreportId;
	Map<String, Set<CommentsDTO> >mapOfCommentsDTOTocomment;
	Map<Long, Set<CommentsDTO> >mapOfCommentsDTOTocommentDate;


	static CommentsRepository instance = null;  
	private CommentsRepository(){
		mapOfCommentsDTOToiD = new ConcurrentHashMap<>();
		mapOfCommentsDTOToreportId = new ConcurrentHashMap<>();
		mapOfCommentsDTOTocomment = new ConcurrentHashMap<>();
		mapOfCommentsDTOTocommentDate = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static CommentsRepository getInstance(){
		if (instance == null){
			instance = new CommentsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<CommentsDTO> commentsDTOs = commentsDAO.getAllComments(reloadAll);
			for(CommentsDTO commentsDTO : commentsDTOs) {
				CommentsDTO oldCommentsDTO = getCommentsDTOByID(commentsDTO.iD);
				if( oldCommentsDTO != null ) {
					mapOfCommentsDTOToiD.remove(oldCommentsDTO.iD);
				
					if(mapOfCommentsDTOToreportId.containsKey(oldCommentsDTO.reportId)) {
						mapOfCommentsDTOToreportId.get(oldCommentsDTO.reportId).remove(oldCommentsDTO);
					}
					if(mapOfCommentsDTOToreportId.get(oldCommentsDTO.reportId).isEmpty()) {
						mapOfCommentsDTOToreportId.remove(oldCommentsDTO.reportId);
					}
					
					if(mapOfCommentsDTOTocomment.containsKey(oldCommentsDTO.comment)) {
						mapOfCommentsDTOTocomment.get(oldCommentsDTO.comment).remove(oldCommentsDTO);
					}
					if(mapOfCommentsDTOTocomment.get(oldCommentsDTO.comment).isEmpty()) {
						mapOfCommentsDTOTocomment.remove(oldCommentsDTO.comment);
					}
					
					if(mapOfCommentsDTOTocommentDate.containsKey(oldCommentsDTO.commentDate)) {
						mapOfCommentsDTOTocommentDate.get(oldCommentsDTO.commentDate).remove(oldCommentsDTO);
					}
					if(mapOfCommentsDTOTocommentDate.get(oldCommentsDTO.commentDate).isEmpty()) {
						mapOfCommentsDTOTocommentDate.remove(oldCommentsDTO.commentDate);
					}
					
					
				}
				if(commentsDTO.isDeleted == false) 
				{
					
					mapOfCommentsDTOToiD.put(commentsDTO.iD, commentsDTO);
				
					if( ! mapOfCommentsDTOToreportId.containsKey(commentsDTO.reportId)) {
						mapOfCommentsDTOToreportId.put(commentsDTO.reportId, new HashSet<>());
					}
					mapOfCommentsDTOToreportId.get(commentsDTO.reportId).add(commentsDTO);
					
					if( ! mapOfCommentsDTOTocomment.containsKey(commentsDTO.comment)) {
						mapOfCommentsDTOTocomment.put(commentsDTO.comment, new HashSet<>());
					}
					mapOfCommentsDTOTocomment.get(commentsDTO.comment).add(commentsDTO);
					
					if( ! mapOfCommentsDTOTocommentDate.containsKey(commentsDTO.commentDate)) {
						mapOfCommentsDTOTocommentDate.put(commentsDTO.commentDate, new HashSet<>());
					}
					mapOfCommentsDTOTocommentDate.get(commentsDTO.commentDate).add(commentsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<CommentsDTO> getCommentsList() {
		List <CommentsDTO> commentss = new ArrayList<CommentsDTO>(this.mapOfCommentsDTOToiD.values());
		return commentss;
	}
	
	
	public CommentsDTO getCommentsDTOByID( long ID){
		return mapOfCommentsDTOToiD.get(ID);
	}
	
	
	public List<CommentsDTO> getCommentsDTOByreport_id(long report_id) {
		return new ArrayList<>( mapOfCommentsDTOToreportId.getOrDefault(report_id,new HashSet<>()));
	}
	
	
	public List<CommentsDTO> getCommentsDTOBycomment(String comment) {
		return new ArrayList<>( mapOfCommentsDTOTocomment.getOrDefault(comment,new HashSet<>()));
	}
	
	
	public List<CommentsDTO> getCommentsDTOBycomment_date(long comment_date) {
		return new ArrayList<>( mapOfCommentsDTOTocommentDate.getOrDefault(comment_date,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "comments";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


