package comments;
import java.util.*; 


public class CommentsDTO {

	public long iD = 0;
	public long reportId = 0;
    public String comment = "";
	public long commentDate = 0;
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$CommentsDTO[" +
            " iD = " + iD +
            " reportId = " + reportId +
            " comment = " + comment +
            " commentDate = " + commentDate +
            " isDeleted = " + isDeleted +
            "]";
    }

}