package comments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class CommentsDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"comments");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"comments");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(CommentsDTO commentsDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = commentsDTO.iD;
		// userDTO.userName = commentsDTO.email;
		// userDTO.fullName = commentsDTO.name;
		// userDTO.password = commentsDTO.password;
		// userDTO.phoneNo = commentsDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = commentsDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(CommentsDTO commentsDTO, UserDTO userDTO)
	{
		// userDTO.ID = commentsDTO.iD;
		// userDTO.fullName = commentsDTO.name;
		// userDTO.phoneNo = commentsDTO.phone;
		// userDTO.mailAddress = commentsDTO.email;

		return userDTO;
	}
		
		
	
	public long addComments(CommentsDTO commentsDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			commentsDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Comments");

			String sql = "INSERT INTO comments";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "comment";
			sql += ", ";
			sql += "comment_date";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(commentsDTO));
			// commentsDTO.iD = userDAO.getUserDTOByUsername(commentsDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + commentsDTO.iD + " in index " + index);
			ps.setObject(index++,commentsDTO.iD);
			//System.out.println("Setting object" + commentsDTO.reportId + " in index " + index);
			ps.setObject(index++,commentsDTO.reportId);
			//System.out.println("Setting object" + commentsDTO.comment + " in index " + index);
			ps.setObject(index++,commentsDTO.comment);
			//System.out.println("Setting object" + commentsDTO.commentDate + " in index " + index);
			ps.setObject(index++,commentsDTO.commentDate);
			//System.out.println("Setting object" + commentsDTO.isDeleted + " in index " + index);
			ps.setObject(index++,commentsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//CommentsRepository.getInstance().reload(false);	
		return commentsDTO.iD;
	}
	
	//need another getter for repository
	public CommentsDTO getCommentsDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CommentsDTO commentsDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "comment";
			sql += ", ";
			sql += "comment_date";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM comments";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				commentsDTO = new CommentsDTO();

				commentsDTO.iD = rs.getLong("ID");
				commentsDTO.reportId = rs.getLong("report_id");
				commentsDTO.comment = rs.getString("comment");
				commentsDTO.commentDate = rs.getLong("comment_date");
				commentsDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return commentsDTO;
	}
	
	public void updateComments(CommentsDTO commentsDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE comments";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "report_id=?";
			sql += ", ";
			sql += "comment=?";
			sql += ", ";
			sql += "comment_date=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + commentsDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + commentsDTO.iD + " in index " + index);
			ps.setObject(index++,commentsDTO.iD);
			//System.out.println("Setting object" + commentsDTO.reportId + " in index " + index);
			ps.setObject(index++,commentsDTO.reportId);
			//System.out.println("Setting object" + commentsDTO.comment + " in index " + index);
			ps.setObject(index++,commentsDTO.comment);
			//System.out.println("Setting object" + commentsDTO.commentDate + " in index " + index);
			ps.setObject(index++,commentsDTO.commentDate);
			//System.out.println("Setting object" + commentsDTO.isDeleted + " in index " + index);
			ps.setObject(index++,commentsDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(commentsDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(commentsDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//CommentsRepository.getInstance().reload(false);
	}
	
	public void deleteCommentsByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE comments";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<CommentsDTO> getCommentsDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CommentsDTO commentsDTO = null;
		List<CommentsDTO> commentsDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "comment";
			sql += ", ";
			sql += "comment_date";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM comments";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			while(rs.next()){
				commentsDTO = new CommentsDTO();
				commentsDTO.iD = rs.getLong("ID");
				commentsDTO.reportId = rs.getLong("report_id");
				commentsDTO.comment = rs.getString("comment");
				commentsDTO.commentDate = rs.getLong("comment_date");
				commentsDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = commentsDTO.iD;
				while(i < commentsDTOList.size() && commentsDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				commentsDTOList.add(i,  commentsDTO);
				//commentsDTOList.add(commentsDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return commentsDTOList;
	}
	
	public List<CommentsDTO> getCommentsDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getCommentsDTOByColumn(filter);
	}
	
	public List<CommentsDTO> getCommentsDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getCommentsDTOByColumn(filter);
	}
	
	public List<CommentsDTO> getCommentsDTOByColumn(String column, long value){
		String filter = column + " = " + value;
		return getCommentsDTOByColumn(filter);
	}
	
	public List<CommentsDTO> getCommentsDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getCommentsDTOByColumn(filter);
	}
		
	
	
	public List<CommentsDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CommentsDTO commentsDTO = null;
		List<CommentsDTO> commentsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return commentsDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "report_id";
			sql += ", ";
			sql += "comment";
			sql += ", ";
			sql += "comment_date";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM comments";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				commentsDTO = new CommentsDTO();
				commentsDTO.iD = rs.getLong("ID");
				commentsDTO.reportId = rs.getLong("report_id");
				commentsDTO.comment = rs.getString("comment");
				commentsDTO.commentDate = rs.getLong("comment_date");
				commentsDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + commentsDTO);
				//commentsDTOList.add(commentsDTO);
				int i = 0;
				long primaryKey = commentsDTO.iD;
				while(i < commentsDTOList.size() && commentsDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				commentsDTOList.add(i,  commentsDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return commentsDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM comments";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<CommentsDTO> getAllComments (boolean isFirstReload)
    {
		List<CommentsDTO> commentsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM comments";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				CommentsDTO commentsDTO = new CommentsDTO();
				commentsDTO.iD = rs.getLong("ID");
				commentsDTO.reportId = rs.getLong("report_id");
				commentsDTO.comment = rs.getString("comment");
				commentsDTO.commentDate = rs.getLong("comment_date");
				commentsDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = commentsDTO.iD;
				while(i < commentsDTOList.size() && commentsDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				commentsDTOList.add(i,  commentsDTO);
				//commentsDTOList.add(commentsDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return commentsDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM comments";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		CommentsDTO commentsDTO = new CommentsDTO();
		try{

			String sql = "SELECT ID FROM comments";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = CommentsMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(CommentsMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !CommentsMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(CommentsMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	