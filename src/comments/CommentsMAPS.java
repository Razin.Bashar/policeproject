package comments;
import java.util.*; 


public class CommentsMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static CommentsMAPS self = null;
	
	private CommentsMAPS()
	{
		
		java_type_map.put("report_id".toLowerCase(), "Long");
		java_type_map.put("comment".toLowerCase(), "String");
		java_type_map.put("comment_date".toLowerCase(), "Long");

		java_custom_search_map.put("report_id".toLowerCase(), "Long");
		java_custom_search_map.put("comment".toLowerCase(), "String");
		java_custom_search_map.put("comment_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("reportId".toLowerCase(), "reportId".toLowerCase());
		java_DTO_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_DTO_map.put("commentDate".toLowerCase(), "commentDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static CommentsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new CommentsMAPS ();
		}
		return self;
	}
	

}