package report;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import citizen.CitizenDAO;
import citizen.CitizenDTO;
import comment_image.Comment_imageDAO;
import comment_image.Comment_imageDTO;
import comments.CommentsDAO;
import comments.CommentsDTO;
import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;
import vehicle.VehicleDAO;
import vehicle.VehicleDTO;
import vehicle_image.Vehicle_imageDAO;
import vehicle_image.Vehicle_imageDTO;

import java.io.*;
import javax.servlet.http.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import report.Constants;

/**
 * Servlet implementation class ReportServlet
 */
@WebServlet("/ReportServlet")
@MultipartConfig
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Logger logger = Logger.getLogger(ReportServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		if (loginDTO == null) {
////			loginDTO = new LoginDTO();
////			loginDTO.userID = 1;
////			loginDTO.loginSourceIP = "127.0.0.1";
////			request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
//		}
		loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO.userID);
		try {
			String actionType = request.getParameter("actionType");
			if (actionType.equals("getAddPage")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REPORT_ADD)) {
					getAddPage(request, response);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			} else if (actionType.equals("getLandingPage")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LANDING_PAGE)) {
					getLandingPage(request, response);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			} else if (actionType.equals("getEditPage")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
						MenuConstants.REPORT_UPDATE)) {
					getReport(request, response);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}

			} else if (actionType.equals("search")) {
				System.out.println("search requested");
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
						MenuConstants.REPORT_SEARCH)) {
					searchReport(request, response);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("report/reportEdit.jsp");
		requestDispatcher.forward(request, response);
	}

	private void getLandingPage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher;
		String choice = request.getParameter("flstatus");
		if (choice != null) {
			requestDispatcher = request.getRequestDispatcher("landing_page_new/landingPageNewSecond.jsp");
		} else {
			requestDispatcher = request.getRequestDispatcher("landing_page_new/landingPageNewFirst.jsp");
		}

		requestDispatcher.forward(request, response);
	}

	private String getFileName(final Part part) {
		if (part == null)
			return null;
		final String partHeader = part.getHeader("content-disposition");
		System.out.println("Part Header = {0}" + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}

	private void UploadFile(HttpServletRequest request, HttpServletResponse response, Part filePart, String fileName,String filePath) {

		OutputStream out = null;
		InputStream filecontent = null;

		//String path = getServletContext().getRealPath("/img2/");
		String path = getServletContext().getRealPath(filePath);

		File dir = new File(path);

		if (!dir.exists()) {
			dir.mkdir();
			System.out.println("created directory " + path);
		}

		try {
			out = new FileOutputStream(new File(path + File.separator + fileName));
			filecontent = filePart.getInputStream();

			int read = 0;
			final byte[] bytes = new byte[1024];

			while ((read = filecontent.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			System.out.println("New file " + fileName + " created at " + path);

		} catch (IOException fne) {
			System.out.println("You either did not specify a file to upload or are "
					+ "trying to upload a file to a protected or nonexistent " + "location.");
			System.out.println("ERROR: " + fne.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (filecontent != null) {
				try {
					filecontent.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO.userID);
		System.out.println("doPost");

		try {
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if (actionType.equals("add")) {

				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REPORT_ADD)) {
					System.out.println("going to  addReport ");
					addReport(request, response, true);
				} else {
					System.out.println("Not going to  addReport ");
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}

			}
			if (actionType.equals("cmtadd")) {

				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REPORT_ADD)) {
					// System.out.println("going to addReport ");
					// addReport(request, response, true);
					CommentsDTO commentsDTO = new CommentsDTO();
					commentsDTO.reportId = Long.parseLong(request.getParameter("reportId"));
					commentsDTO.commentDate = Long.parseLong(request.getParameter("commentDate"));
					commentsDTO.comment = request.getParameter("comment");
					CommentsDAO commentsDAO = new CommentsDAO();
					long commentId = commentsDAO.addComments(commentsDTO);
					
					int imgCount = Integer.parseInt(request.getParameter("cmtimgCount"));
					Comment_imageDAO comment_imageDAO = new Comment_imageDAO();
					for (int i = 0; i < imgCount; i++) {
						Comment_imageDTO comment_imageDTO = new Comment_imageDTO();
						comment_imageDTO.commentId = commentId;
						Part filePart_image1 = request.getPart("cmtimage"+i);
						String Value = getFileName(filePart_image1);
						System.out.println("image"+i+" = " + Value);
						if ( (Value != null && !Value.equalsIgnoreCase(""))) {
							if (Value.toLowerCase().endsWith(".jpg")||Value.toLowerCase().endsWith(".jpeg") || Value.toLowerCase().endsWith(".png")
									|| Value.toLowerCase().endsWith(".gif") || Value.toLowerCase().endsWith(".bmp")
									|| Value.toLowerCase().endsWith(".ico")) {
								String FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
								String FileName = commentsDTO.reportId+"_postId_"+commentId+"_commentimage_"+FileNamePrefix + "_" + "image1" + "_" + Value;
								comment_imageDTO.name = (FileName);
								UploadFile(request, response, filePart_image1, FileName,"/police_citizen_resource/vehicle_comment_image/");
								comment_imageDAO.addComment_image(comment_imageDTO);
							}
						} else {
							System.out.println("FieldName has a null value, not updating" + " = " + Value);
						}
					}
					return;

				} else {
					System.out.println("Not going to  addReport ");
					// request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request,
					// response);
				}

			} else if (actionType.equals("edit")) {

				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
						MenuConstants.REPORT_UPDATE)) {
					addReport(request, response, false);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			} else if (actionType.equals("delete")) {
				deleteReport(request, response);
			} else if (actionType.equals("search")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
						MenuConstants.REPORT_SEARCH)) {
					searchReport(request, response);
				} else {
					request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void addReport(HttpServletRequest request, HttpServletResponse response, Boolean addFlag)
			throws IOException {
		// TODO Auto-generated method stub
		try {
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addReport");
			long vehicle_Id = 0, citizen_Id = 0, report_Id = 0;

			CitizenDAO citizenDAO = new CitizenDAO();
			CitizenDTO citizenDTO;
			if (addFlag == true) {
				citizenDTO = new CitizenDTO();
			} else {
				citizenDTO = citizenDAO.getCitizenDTOByID(Long.parseLong(request.getParameter("reporterId")));
			}

			String Value = "";

			Value = request.getParameter("nid");
			System.out.println("nid = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.nid = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.nid = (Value);
			}
			Value = request.getParameter("name");
			System.out.println("name = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.name = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.name = (Value);
			}
			Value = request.getParameter("phoneNumber");
			System.out.println("phoneNumber = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.phoneNumber = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.phoneNumber = (Value);
			}
			Value = request.getParameter("address");
			System.out.println("address = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.address = Integer.parseInt(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.address = Integer.parseInt(Value);
			}
			Value = request.getParameter("addressDetails");
			System.out.println("addressDetails = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.addressDetails = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.addressDetails = (Value);
			}
			Value = request.getParameter("mailId");
			System.out.println("mailId = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				citizenDTO.mailId = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					citizenDTO.mailId = (Value);
			}

			System.out.println("Done adding  addCitizen dto = " + citizenDTO);

			if (addFlag == true) {
				citizen_Id = (long) citizenDAO.addCitizen(citizenDTO);
			} else {
				citizenDAO.updateCitizen(citizenDTO);
			}

			VehicleDAO vehicleDAO = new VehicleDAO();
			VehicleDTO vehicleDTO;
			if (addFlag == true) {
				vehicleDTO = new VehicleDTO();
			} else {
				vehicleDTO = vehicleDAO.getVehicleDTOByID(Long.parseLong(request.getParameter("vehicleId")));
			}

			Value = request.getParameter("typeId");
			System.out.println("typeId = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.typeId = Integer.parseInt(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.typeId = Integer.parseInt(Value);
			}
			Value = request.getParameter("modelName");
			System.out.println("modelName = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.modelName = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.modelName = (Value);
			}
			Value = request.getParameter("color");
			System.out.println("color = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.color = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.color = (Value);
			}
			Value = request.getParameter("engineNumber");
			System.out.println("engineNumber = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.engineNumber = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.engineNumber = (Value);
			}
			Value = request.getParameter("engineType");
			System.out.println("engineType = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.engineType = Long.parseLong(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.engineType = Long.parseLong(Value);
			}
			Value = request.getParameter("chassisNumber");
			System.out.println("chassisNumber = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.chassisNumber = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.chassisNumber = (Value);
			}
			Value = request.getParameter("engineCc");
			System.out.println("engineCc = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.engineCc = Integer.parseInt(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.engineCc = Integer.parseInt(Value);
			}
			Value = request.getParameter("registrationNumber");
			System.out.println("registrationNumber = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.registrationNumber = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.registrationNumber = (Value);
			}
			Value = request.getParameter("manufacturer");
			System.out.println("manufacturer = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.manufacturer = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.manufacturer = (Value);
			}
			Value = request.getParameter("manufacturingYear");
			System.out.println("manufacturingYear = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				vehicleDTO.manufacturingYear = Integer.parseInt(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					vehicleDTO.manufacturingYear = Integer.parseInt(Value);
			}
			

			System.out.println("Done adding  addVehicle dto = " + vehicleDTO);

			if (addFlag == true) {
				vehicle_Id = vehicleDAO.addVehicle(vehicleDTO);
			} else {
				vehicleDAO.updateVehicle(vehicleDTO);
			}

			ReportDAO reportDAO = new ReportDAO();
			ReportDTO reportDTO;
			String FileNamePrefix;
			if (addFlag == true) {
				reportDTO = new ReportDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			} else {
				reportDTO = reportDAO.getReportDTOByID(Long.parseLong(request.getParameter("iD")));
				FileNamePrefix = request.getParameter("iD");
			}

			Value = "";
			Value = request.getParameter("reportingDate");
			System.out.println("reportingDate = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.reportingDate = Long.parseLong(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.reportingDate = Long.parseLong(Value);
			}

			if (addFlag == true) {
				reportDTO.reporterId = citizen_Id;
			}

			if (addFlag == true) {
				reportDTO.vehicleId = vehicle_Id;
			}

			Value = request.getParameter("lostDate");
			System.out.println("lostDate = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.lostDate = Long.parseLong(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.lostDate = Long.parseLong(Value);
			}
			Value = request.getParameter("foundDate");
			System.out.println("foundDate = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.foundDate = Long.parseLong(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.foundDate = Long.parseLong(Value);
			}
			Value = request.getParameter("statusId");
			System.out.println("statusId = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.statusId = Long.parseLong(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.statusId = Long.parseLong(Value);
			}
			Value = request.getParameter("thanaAddress");
			System.out.println("thanaAddress = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.thanaAddress = Integer.parseInt(Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.thanaAddress = Integer.parseInt(Value);
			}
			Value = request.getParameter("moreDetails");
			System.out.println("moreDetails = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.moreDetail = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.moreDetail = (Value);
			}
			Value = request.getParameter("blog");
			System.out.println("blog = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.blog = (Value);
			} else {
				if ((Value != null && !Value.equalsIgnoreCase("")))
					reportDTO.blog = (Value);
			}

			
			Value = request.getParameter("isDeleted");
			System.out.println("isDeleted = " + Value);
			if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
				reportDTO.isDeleted = Boolean.parseBoolean(Value);
			} else {
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addReport dto = " + reportDTO);

			if (addFlag == true) {
				report_Id=reportDAO.addReport(reportDTO);
			} else {
				reportDAO.updateReport(reportDTO);
			}
			
			String imc = request.getParameter("imgCount");
			if(imc!=null&&(!imc.trim().equals(""))){
				int imgCount = Integer.parseInt(request.getParameter("imgCount"));
				Vehicle_imageDAO vehicle_imageDAO = new Vehicle_imageDAO();
				for (int i = 0; i < imgCount; i++) {
					Vehicle_imageDTO vehicle_imageDTO = new Vehicle_imageDTO();
					vehicle_imageDTO.reportId = report_Id;
					Part filePart_image1 = request.getPart("image"+i);
					Value = getFileName(filePart_image1);
					System.out.println("image"+i+" = " + Value);
					if (addFlag == true && (Value != null && !Value.equalsIgnoreCase(""))) {
						if (Value.toLowerCase().endsWith(".jpg")||Value.toLowerCase().endsWith(".jpeg") || Value.toLowerCase().endsWith(".png")
								|| Value.toLowerCase().endsWith(".gif") || Value.toLowerCase().endsWith(".bmp")
								|| Value.toLowerCase().endsWith(".ico")) {
							String FileName = report_Id+"_post_"+FileNamePrefix + "_" + "image1" + "_" + Value;
							vehicle_imageDTO.name = (FileName);
							UploadFile(request, response, filePart_image1, FileName,"/police_citizen_resource/vehicle_post_image/");
							vehicle_imageDAO.addVehicle_image(vehicle_imageDTO);
						}
					} else {
						System.out.println("FieldName has a null value, not updating" + " = " + Value);
					}
				}
			}

			String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
			if (request.getParameter("flstatus") != null) {
				String url = "ReportServlet?actionType=search&&land=1&&flstatus=" + request.getParameter("flstatus")
						+ "&&type=" + request.getParameter("type") + "";
				System.out.println(url);
				response.sendRedirect(url);
			} else {
				if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
					response.sendRedirect("ReportServlet?actionType=search&&land=1");
				} else {
					response.sendRedirect("ReportServlet?actionType=search");
				}
			}

		} catch (Exception e) {
			logger.debug(e);
		}
	}

	private void deleteReport(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String[] IDsToDelete = request.getParameterValues("ID");
			for (int i = 0; i < IDsToDelete.length; i++) {
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("###DELETING " + IDsToDelete[i]);
				new ReportDAO().deleteReportByID(id);
			}
		} catch (Exception ex) {
			logger.debug(ex);
		}
		response.sendRedirect("ReportServlet?actionType=search");
	}

	private void getReport(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("in getReport");
		ReportDTO reportDTO = null;
		try {
			reportDTO = new ReportDAO().getReportDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", reportDTO.iD);
			request.setAttribute("reportDTO", reportDTO);

			String URL = "";

			String inPlaceEdit = (String) request.getParameter("inplaceedit");
			String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

			if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
				URL = "report/reportInPlaceEdit.jsp";
				request.setAttribute("inplaceedit", "");
			} else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
				URL = "report/reportSearchRow.jsp";
				request.setAttribute("inplacesubmit", "");
			} else {
				URL = "report/reportEdit.jsp?actionType=edit";
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void searchReport(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("in  searchReport 1");
		ReportDAO reportDAO = new ReportDAO();
		LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
		String ajax = (String) request.getParameter("ajax");
		boolean hasAjax = false;
		ArrayList<String> tableList = new ArrayList<>();
		tableList.add("report");
		tableList.add("vehicle");
		tableList.add("citizen");

		if (ajax != null && !ajax.equalsIgnoreCase("")) {
			hasAjax = true;
		}
		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_REPORT, request, reportDAO,
				SessionConstants.VIEW_REPORT, SessionConstants.SEARCH_REPORT);
		try {
			System.out.println("trying to dojob");
			if (request.getParameter("flstatus") != null) {
				rnManager.doJob(loginDTO, tableList);

			} else if (request.getParameter("multidao") != null) {
				rnManager.doJob(loginDTO, tableList);
			} else
				rnManager.doJob(loginDTO);
		} catch (Exception e) {
			System.out.println("failed to dojob" + e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("landing_page/landingpage.jsp");

		if (hasAjax == false) {
			if (request.getParameter("flstatus") != null) {
				rd = request.getRequestDispatcher("landing_page_new/landingPageNew.jsp");
			} else {
				if (request.getParameter("multidao") != null) {
					rd = request.getRequestDispatcher("landing_page/landingpage.jsp");
				} else {

					if (request.getParameter("land") != null)
						rd = request.getRequestDispatcher("landing_page/landingpage.jsp");
					else
						rd = request.getRequestDispatcher("report/reportSearch.jsp");
				}
			}
		} else {
			if (request.getParameter("flstatus") != null) {
				rd = request.getRequestDispatcher("landing_page_new/landingPageNewForm.jsp");
			} else {
				if (request.getParameter("multidao") != null) {
					rd = request.getRequestDispatcher("landing_page/landingpageForm.jsp");
				} else {
					System.out.println("Going to report/reportSearchForm.jsp");
					if (request.getParameter("land") != null)
						rd = request.getRequestDispatcher("landing_page/landingpageForm.jsp");
					else
						rd = request.getRequestDispatcher("report/reportSearchForm.jsp");
				}
			}
		}
		rd.forward(request, response);
	}

}
