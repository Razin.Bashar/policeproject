package util;

public enum TransactionType{
	PART_OF_PREVIOUS_TRANSACTION,
	INDIVIDUAL_TRANSACTION,
	READONLY
}
