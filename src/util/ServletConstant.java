package util;

import user.UserRepository;

/**
 * @author Kayesh Parvez
 *
 */
public class ServletConstant {
	
	public static final String DEFAULT_PASSWORD = "�����";
	

	
	public static final String USERNAME = "userName";
	public static final String PASSWORD = "password";
	public static final String USER_TYPE = "userType";
	public static final String MAIL_ADDRESS = "mailAddress";
	public static final String FULL_NAME = "fullName";
	public static final String PHONE_NO = "phoneNo";
	public static final String ID = "ID";
	public static final String ROLE_NAME = "roleName";
	public static final String LANGUAGE_ID = "languageID";
	public static final String USER_DTO = "userDTO";
	public static final String ROLE_LIST = "roleList";
	public static final String DESCRIPTION = "description";
	public static final String ROLE_DTO = "roleDTO";
	public static final String CHECK_USER_AVAILABLE = "checkUserAvailable";

	public static final String SUCCESSFUL_MSG = "successMsg";
	public static final String FAILURE_MSG = "failureMsg";
	
	public static final String MENU_ID = "menuID";
	public static final String COLUMN_ID = "columnID";
	public static final String DELETE_ID = "deleteID";
	public static final String MENU_CONSTANT_NAME = "menuConstantName";
	
	public static final String LANGUAGE_TEXT_ENGLISH = "languageTextEnglish";
	public static final String LANGUAGE_TEXT_BANGLA = "languageTextBangla";
	public static final String LANGUAGE_CONSTANT = "languageConstant";
	public static final String LANGUAGE_CONSTANT_PREFIX = "languageConstantPrefix";
	public static final String LANGUAGE_TEXT_MENU_SELECTED = "languageTextMenuSelected";
	public static final String LANGUAGE_CONSTANT_PREFIX_SELECTED = "languageConstantPrefixSelected";
	public static final String REMEMBER_ME_COOKIE_NAME = "rememberMe";
}
