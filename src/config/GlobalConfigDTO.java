package config;


public class GlobalConfigDTO
{
	public int ID = -1;
	public String name = "";
	public int value = 0;
	public String comments = "";
	public int groupID = -1;
}
