package language;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import common.RequestFailureException;
import login.LoginDTO;
import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;

@WebServlet("/languageChangeServlet")
public class LanguageChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(LanguageChangeServlet.class);
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		if(loginDTO!=null){
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO.userID);
			userDTO.languageID = (userDTO.languageID == 1?2:1);
			try{
				new UserDAO().updateUser(userDTO);
			}catch(Exception ex){
				if(ex instanceof RequestFailureException){
					throw (RequestFailureException)ex;
				}
			}
		}
		
		String reffer = request.getHeader("referer");
		if(reffer!=null){
			response.sendRedirect(reffer);
		}else{
			response.sendRedirect("");
		}
		
		
	}


}
