/*
 * Created on Oct 27, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package sessionmanager;

import language.LC;
import language.LM;

public class SessionConstants {   
    //navigation bar related
    public final static String NAVIGATION_BAR_FIRST = "first";
    public final static String NAVIGATION_BAR_NEXT = "next";
    public final static String NAVIGATION_BAR_PREVIOUS = "previous";
    public final static String NAVIGATION_BAR_LAST = "last";
    public final static String NAVIGATION_BAR_CURRENT = "current";
    //form check related
    public final static String NAVIGATION_LINK = "id";
    public final static String GO_CHECK_FIELD = "go";
    public final static String SEARCH_CHECK_FIELD = "search";
    public final static String HTML_SEARCH_CHECK_FIELD = "htmlsearch";
    //search quiry related
    public final static String ADVANCED_SEARCH = "AdvancedSearch";
    //record navigation initial parameter
    public final static int CRRENT_PAGE_NO = 1;
    public final static int TOTAL_PAGES = 10;
    public final static int PAGE_SIZE = 50000;
    public final static int NUMBER_OF_RECORDS = 100;
    public final static int FIRST_PAGE = 1;
    public static final String RECORDS_PER_PAGE = "RECORDS_PER_PAGE";
    /*
     * Failure Message
     */
    //Error Message
    public final static String FAILURE_MESSAGE = "failuremessage";
	/*
	 * User Login
	 */
    public final static String USER_LOGIN = "user_login";
    /*
     * Role
     */
    public final static String NAV_ROLE = "navrole";
    public final static String VIEW_ROLE = "viewrole";
    public final static String[][] SEARCH_ROLE = {
            {
                ""+LC.ROLE_SEARCH_ROLE_NAME, "roleName"},
            {
                ""+LC.ROLE_SEARCH_DESCRIPTION, "description"}
        };



    public final static String NAV_USER = "navuser";
    public final static String VIEW_USER = "viewuser";
	public final static String SEARCH_USER[][] = {
			{""+LC.USER_SEARCH_USER_ID,"userName"},
			{".//user//userType.jsp","userType"},
			{""+LC.USER_SEARCH_MAIL_ADDRESS,"mailAddress"},
			{""+LC.USER_SEARCH_FULL_NAME,"fullName"},
			{""+LC.USER_SEARCH_PHONE_NO,"phoneNo"}
		};

	public final static String VIEW_LANGUAGE = "viewlanguage";
	public final static String NAV_LANGUAGE = "navlanguage";
	public final static String SEARCH_LANGUAGE[][] = {
			{".//language//menu.jsp", "menuID"},
			{"English Text","languageTextEnglish"},
			{"Bangla Text","languageTextBangla"},
			{"Constant Prefix","languageConstantPrefix"},
			{"Constant","languageConstant"}
		};
	
	public final static String VIEW_LANGUAGE_GROUP = "viewlanguageGroup";
	public final static String NAV_LANGUAGE_GROUP = "navlanguageGroup";
	public final static String SEARCH_LANGUAGE_GROUP[][] = {
			{"Name", "name"},
			{"Url","url"}			
		};




	public final static String NAV_PUPPIES = "navPUPPIES";
	public final static String VIEW_PUPPIES = "viewPUPPIES";
	public static final String[][] SEARCH_PUPPIES = {
		{ "Name" , "Name" },
		{ "Age" , "Age" },
		{ "Address" , "Address" },
		{ "Gender" , "Gender" },
		{ "Color" , "Color" },
		{ "IsWellBehaved" , "IsWellBehaved" },
		{ "IsDeleted" , "isDeleted" }
	};












	public final static String NAV_THEME = "navTHEME";
	public final static String VIEW_THEME = "viewTHEME";
	public static final String[][] SEARCH_THEME = {
		{ ""+LC.THEME_SEARCH_THEMENAME, "theme_name" },
		{ ""+LC.THEME_SEARCH_DIRECTORY, "directory" },
		{ ""+LC.THEME_SEARCH_ISAPPLIED, "isApplied" },
		{ ""+LC.THEME_SEARCH_ANYFIELD , "AnyField" }
	};












	public final static String NAV_SUPER_HERO_TABLE = "navSUPER_HERO_TABLE";
	public final static String VIEW_SUPER_HERO_TABLE = "viewSUPER_HERO_TABLE";
	public static final String[][] SEARCH_SUPER_HERO_TABLE = {
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_FIRSTNAME, "first_name" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_LASTNAME, "last_name" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_GENDER, "gender" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_RELIGION, "religion" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_DATEOFBIRTH, "date_of_birth" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_OTHERDATABASE, "other_database" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_EDUCATION, "education" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_PASSINGYEAR, "passing_year" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_ISSTRONG, "is_strong" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_EYECOLOR, "eye_color" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_HASARADIO, "has_a_radio" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_PRECISEWEIGHT, "precise_weight" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_MAILRICHTEXT, "mail_richtext" },
		{ ""+LC.SUPER_HERO_TABLE_SEARCH_ANYFIELD , "AnyField" }
	};






	public final static String NAV_TESTRAZIN = "navTESTRAZIN";
	public final static String VIEW_TESTRAZIN = "viewTESTRAZIN";
	public static final String[][] SEARCH_TESTRAZIN = {
		{ ""+LC.TESTRAZIN_SEARCH_FIRSTNAME, "first_name" },
		{ ""+LC.TESTRAZIN_SEARCH_LASTNAME, "last_name" },
		{ ""+LC.TESTRAZIN_SEARCH_GENDER, "gender" },
		{ ""+LC.TESTRAZIN_SEARCH_RELIGION, "religion" },
		{ ""+LC.TESTRAZIN_SEARCH_DATEOFBIRTH, "date_of_birth" },
		{ ""+LC.TESTRAZIN_SEARCH_ANYFIELD , "AnyField" }
	};






	public final static String NAV_CITIZEN = "navCITIZEN";
	public final static String VIEW_CITIZEN = "viewCITIZEN";
	public static final String[][] SEARCH_CITIZEN = {
		{ ""+LC.CITIZEN_SEARCH_NID, "nid" },
		{ ""+LC.CITIZEN_SEARCH_NAME, "name" },
		{ ""+LC.CITIZEN_SEARCH_PHONENUMBER, "phone_number" },
		{ ""+LC.CITIZEN_SEARCH_MAILID, "mail_id" },
		{ ""+LC.CITIZEN_SEARCH_ANYFIELD , "AnyField" }
	};




	public final static String NAV_VEHICLE_TYPE = "navVEHICLE_TYPE";
	public final static String VIEW_VEHICLE_TYPE = "viewVEHICLE_TYPE";
	public static final String[][] SEARCH_VEHICLE_TYPE = {
		{ ""+LC.VEHICLE_TYPE_SEARCH_NAMEBN, "name_bn" },
		{ ""+LC.VEHICLE_TYPE_SEARCH_NAMEEN, "name_en" },
		{ ""+LC.VEHICLE_TYPE_SEARCH_ANYFIELD , "AnyField" }
	};




	public final static String NAV_ENGINE_TYPE = "navENGINE_TYPE";
	public final static String VIEW_ENGINE_TYPE = "viewENGINE_TYPE";
	public static final String[][] SEARCH_ENGINE_TYPE = {
		{ ""+LC.ENGINE_TYPE_SEARCH_NAMEBN, "name_bn" },
		{ ""+LC.ENGINE_TYPE_SEARCH_NAMEEN, "name_en" },
		{ ""+LC.ENGINE_TYPE_SEARCH_ANYFIELD , "AnyField" }
	};




	public final static String NAV_REPORT = "navREPORT";
	public final static String VIEW_REPORT = "viewREPORT";
	public static final String[][] SEARCH_REPORT = {
		{ ""+LC.REPORT_SEARCH_NAME, "name" },
		{ ""+LC.REPORT_SEARCH_NID, "nid" },
		{ ""+LC.REPORT_SEARCH_PHONENUMBER, "phone_number" },
		{ ""+LC.REPORT_SEARCH_MAILID, "mail_id" },
		{ ""+LC.REPORT_SEARCH_TYPEID, "type_id" },
		{ ""+LC.REPORT_SEARCH_MODELNAME, "model_name" },
		{ ""+LC.REPORT_SEARCH_COLOR, "color" },
		{ ""+LC.REPORT_SEARCH_ENGINENUMBER, "engine_number" },
		{ ""+LC.REPORT_SEARCH_ENGINETYPE, "engine_type" },
		{ ""+LC.REPORT_SEARCH_CHASSISNUMBER, "chassis_number" },
		{ ""+LC.REPORT_SEARCH_ENGINECC, "engine_cc" },
		{ ""+LC.REPORT_SEARCH_REGISTRATIONNUMBER, "registration_number" },
		{ ""+LC.REPORT_SEARCH_MANUFACTURER, "manufacturer" },
		{ ""+LC.REPORT_SEARCH_MANUFACTURINGYEAR, "manufacturing_year" },
		{ ""+LC.REPORT_SEARCH_MOREDETAILS, "more_details" },
		{ ""+LC.REPORT_SEARCH_REPORTINGDATE, "reporting_date" },
		{ ""+LC.REPORT_SEARCH_REPORTERID, "reporter_id" },
		{ ""+LC.REPORT_SEARCH_VEHICLEID, "vehicle_id" },
		{ ""+LC.REPORT_SEARCH_LOSTDATE, "lost_date" },
		{ ""+LC.REPORT_SEARCH_FOUNDDATE, "found_date" },
		{ ""+LC.REPORT_SEARCH_BLOG, "blog" },
		{ ""+LC.REPORT_SEARCH_ANYFIELD , "AnyField" }
	};




	public final static String NAV_VEHICLE = "navVEHICLE";
	public final static String VIEW_VEHICLE = "viewVEHICLE";
	public static final String[][] SEARCH_VEHICLE = {
		{ ""+LC.VEHICLE_SEARCH_TYPEID, "type_id" },
		{ ""+LC.VEHICLE_SEARCH_MODELNAME, "model_name" },
		{ ""+LC.VEHICLE_SEARCH_COLOR, "color" },
		{ ""+LC.VEHICLE_SEARCH_ENGINENUMBER, "engine_number" },
		{ ""+LC.VEHICLE_SEARCH_ENGINETYPE, "engine_type" },
		{ ""+LC.VEHICLE_SEARCH_CHASSISNUMBER, "chassis_number" },
		{ ""+LC.VEHICLE_SEARCH_ENGINECC, "engine_cc" },
		{ ""+LC.VEHICLE_SEARCH_REGISTRATIONNUMBER, "registration_number" },
		{ ""+LC.VEHICLE_SEARCH_MANUFACTURER, "manufacturer" },
		{ ""+LC.VEHICLE_SEARCH_MANUFACTURINGYEAR, "manufacturing_year" },
		{ ""+LC.VEHICLE_SEARCH_MOREDETAILS, "more_details" },
		{ ""+LC.VEHICLE_SEARCH_ANYFIELD , "AnyField" }
	};






	public final static String NAV_VEHICLE_IMAGE = "navVEHICLE_IMAGE";
	public final static String VIEW_VEHICLE_IMAGE = "viewVEHICLE_IMAGE";
	public static final String[][] SEARCH_VEHICLE_IMAGE = {
		{ ""+LC.VEHICLE_IMAGE_SEARCH_REPORTID, "report_id" },
		{ ""+LC.VEHICLE_IMAGE_SEARCH_NAME, "name" },
		{ ""+LC.VEHICLE_IMAGE_SEARCH_ANYFIELD , "AnyField" }
	};




	public final static String NAV_COMMENT_IMAGE = "navCOMMENT_IMAGE";
	public final static String VIEW_COMMENT_IMAGE = "viewCOMMENT_IMAGE";
	public static final String[][] SEARCH_COMMENT_IMAGE = {
		{ ""+LC.COMMENT_IMAGE_SEARCH_COMMENTID, "comment_id" },
		{ ""+LC.COMMENT_IMAGE_SEARCH_NAME, "name" },
		{ ""+LC.COMMENT_IMAGE_SEARCH_ANYFIELD , "AnyField" }
	};

}
