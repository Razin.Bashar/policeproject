package citizen;
import java.util.*; 


public class CitizenDTO {

	public long iD = 0;
    public String nid = "";
    public String name = "";
    public String phoneNumber = "";
	public int address = 0;
    public String addressDetails = "";
    public String mailId = "";
	public boolean isDeleted = false;
	
    @Override
	public String toString() {
            return "$CitizenDTO[" +
            " iD = " + iD +
            " nid = " + nid +
            " name = " + name +
            " phoneNumber = " + phoneNumber +
            " address = " + address +
            " addressDetails = " + addressDetails +
            " mailId = " + mailId +
            " isDeleted = " + isDeleted +
            "]";
    }

}