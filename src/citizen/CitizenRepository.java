package citizen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class CitizenRepository implements Repository {
	CitizenDAO citizenDAO = new CitizenDAO();
	
	
	static Logger logger = Logger.getLogger(CitizenRepository.class);
	Map<Long, CitizenDTO>mapOfCitizenDTOToiD;
	Map<String, Set<CitizenDTO> >mapOfCitizenDTOTonid;
	Map<String, Set<CitizenDTO> >mapOfCitizenDTOToname;
	Map<String, Set<CitizenDTO> >mapOfCitizenDTOTophoneNumber;
	Map<Integer, Set<CitizenDTO> >mapOfCitizenDTOToaddress;
	Map<String, Set<CitizenDTO> >mapOfCitizenDTOToaddressDetails;
	Map<String, Set<CitizenDTO> >mapOfCitizenDTOTomailId;


	static CitizenRepository instance = null;  
	private CitizenRepository(){
		mapOfCitizenDTOToiD = new ConcurrentHashMap<>();
		mapOfCitizenDTOTonid = new ConcurrentHashMap<>();
		mapOfCitizenDTOToname = new ConcurrentHashMap<>();
		mapOfCitizenDTOTophoneNumber = new ConcurrentHashMap<>();
		mapOfCitizenDTOToaddress = new ConcurrentHashMap<>();
		mapOfCitizenDTOToaddressDetails = new ConcurrentHashMap<>();
		mapOfCitizenDTOTomailId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static CitizenRepository getInstance(){
		if (instance == null){
			instance = new CitizenRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<CitizenDTO> citizenDTOs = citizenDAO.getAllCitizen(reloadAll);
			for(CitizenDTO citizenDTO : citizenDTOs) {
				CitizenDTO oldCitizenDTO = getCitizenDTOByID(citizenDTO.iD);
				if( oldCitizenDTO != null ) {
					mapOfCitizenDTOToiD.remove(oldCitizenDTO.iD);
				
					if(mapOfCitizenDTOTonid.containsKey(oldCitizenDTO.nid)) {
						mapOfCitizenDTOTonid.get(oldCitizenDTO.nid).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOTonid.get(oldCitizenDTO.nid).isEmpty()) {
						mapOfCitizenDTOTonid.remove(oldCitizenDTO.nid);
					}
					
					if(mapOfCitizenDTOToname.containsKey(oldCitizenDTO.name)) {
						mapOfCitizenDTOToname.get(oldCitizenDTO.name).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOToname.get(oldCitizenDTO.name).isEmpty()) {
						mapOfCitizenDTOToname.remove(oldCitizenDTO.name);
					}
					
					if(mapOfCitizenDTOTophoneNumber.containsKey(oldCitizenDTO.phoneNumber)) {
						mapOfCitizenDTOTophoneNumber.get(oldCitizenDTO.phoneNumber).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOTophoneNumber.get(oldCitizenDTO.phoneNumber).isEmpty()) {
						mapOfCitizenDTOTophoneNumber.remove(oldCitizenDTO.phoneNumber);
					}
					
					if(mapOfCitizenDTOToaddress.containsKey(oldCitizenDTO.address)) {
						mapOfCitizenDTOToaddress.get(oldCitizenDTO.address).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOToaddress.get(oldCitizenDTO.address).isEmpty()) {
						mapOfCitizenDTOToaddress.remove(oldCitizenDTO.address);
					}
					
					if(mapOfCitizenDTOToaddressDetails.containsKey(oldCitizenDTO.addressDetails)) {
						mapOfCitizenDTOToaddressDetails.get(oldCitizenDTO.addressDetails).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOToaddressDetails.get(oldCitizenDTO.addressDetails).isEmpty()) {
						mapOfCitizenDTOToaddressDetails.remove(oldCitizenDTO.addressDetails);
					}
					
					if(mapOfCitizenDTOTomailId.containsKey(oldCitizenDTO.mailId)) {
						mapOfCitizenDTOTomailId.get(oldCitizenDTO.mailId).remove(oldCitizenDTO);
					}
					if(mapOfCitizenDTOTomailId.get(oldCitizenDTO.mailId).isEmpty()) {
						mapOfCitizenDTOTomailId.remove(oldCitizenDTO.mailId);
					}
					
					
				}
				if(citizenDTO.isDeleted == false) 
				{
					
					mapOfCitizenDTOToiD.put(citizenDTO.iD, citizenDTO);
				
					if( ! mapOfCitizenDTOTonid.containsKey(citizenDTO.nid)) {
						mapOfCitizenDTOTonid.put(citizenDTO.nid, new HashSet<>());
					}
					mapOfCitizenDTOTonid.get(citizenDTO.nid).add(citizenDTO);
					
					if( ! mapOfCitizenDTOToname.containsKey(citizenDTO.name)) {
						mapOfCitizenDTOToname.put(citizenDTO.name, new HashSet<>());
					}
					mapOfCitizenDTOToname.get(citizenDTO.name).add(citizenDTO);
					
					if( ! mapOfCitizenDTOTophoneNumber.containsKey(citizenDTO.phoneNumber)) {
						mapOfCitizenDTOTophoneNumber.put(citizenDTO.phoneNumber, new HashSet<>());
					}
					mapOfCitizenDTOTophoneNumber.get(citizenDTO.phoneNumber).add(citizenDTO);
					
					if( ! mapOfCitizenDTOToaddress.containsKey(citizenDTO.address)) {
						mapOfCitizenDTOToaddress.put(citizenDTO.address, new HashSet<>());
					}
					mapOfCitizenDTOToaddress.get(citizenDTO.address).add(citizenDTO);
					
					if( ! mapOfCitizenDTOToaddressDetails.containsKey(citizenDTO.addressDetails)) {
						mapOfCitizenDTOToaddressDetails.put(citizenDTO.addressDetails, new HashSet<>());
					}
					mapOfCitizenDTOToaddressDetails.get(citizenDTO.addressDetails).add(citizenDTO);
					
					if( ! mapOfCitizenDTOTomailId.containsKey(citizenDTO.mailId)) {
						mapOfCitizenDTOTomailId.put(citizenDTO.mailId, new HashSet<>());
					}
					mapOfCitizenDTOTomailId.get(citizenDTO.mailId).add(citizenDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<CitizenDTO> getCitizenList() {
		List <CitizenDTO> citizens = new ArrayList<CitizenDTO>(this.mapOfCitizenDTOToiD.values());
		return citizens;
	}
	
	
	public CitizenDTO getCitizenDTOByID( long ID){
		return mapOfCitizenDTOToiD.get(ID);
	}
	
	
	public List<CitizenDTO> getCitizenDTOBynid(String nid) {
		return new ArrayList<>( mapOfCitizenDTOTonid.getOrDefault(nid,new HashSet<>()));
	}
	
	
	public List<CitizenDTO> getCitizenDTOByname(String name) {
		return new ArrayList<>( mapOfCitizenDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<CitizenDTO> getCitizenDTOByphone_number(String phone_number) {
		return new ArrayList<>( mapOfCitizenDTOTophoneNumber.getOrDefault(phone_number,new HashSet<>()));
	}
	
	
	public List<CitizenDTO> getCitizenDTOByaddress(int address) {
		return new ArrayList<>( mapOfCitizenDTOToaddress.getOrDefault(address,new HashSet<>()));
	}
	
	
	public List<CitizenDTO> getCitizenDTOByaddress_details(String address_details) {
		return new ArrayList<>( mapOfCitizenDTOToaddressDetails.getOrDefault(address_details,new HashSet<>()));
	}
	
	
	public List<CitizenDTO> getCitizenDTOBymail_id(String mail_id) {
		return new ArrayList<>( mapOfCitizenDTOTomailId.getOrDefault(mail_id,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "citizen";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


