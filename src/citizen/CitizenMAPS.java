package citizen;
import java.util.*; 


public class CitizenMAPS 
{

	public HashMap<String, String> java_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_custom_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	
	private static CitizenMAPS self = null;
	
	private CitizenMAPS()
	{
		
		java_type_map.put("nid".toLowerCase(), "String");
		java_type_map.put("name".toLowerCase(), "String");
		java_type_map.put("phone_number".toLowerCase(), "String");
		java_type_map.put("mail_id".toLowerCase(), "String");

		java_custom_search_map.put("nid".toLowerCase(), "String");
		java_custom_search_map.put("name".toLowerCase(), "String");
		java_custom_search_map.put("phone_number".toLowerCase(), "String");
		java_custom_search_map.put("mail_id".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nid".toLowerCase(), "nid".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("phoneNumber".toLowerCase(), "phoneNumber".toLowerCase());
		java_DTO_map.put("address".toLowerCase(), "address".toLowerCase());
		java_DTO_map.put("addressDetails".toLowerCase(), "addressDetails".toLowerCase());
		java_DTO_map.put("mailId".toLowerCase(), "mailId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static CitizenMAPS GetInstance()
	{
		if(self == null)
		{
			self = new CitizenMAPS ();
		}
		return self;
	}
	

}