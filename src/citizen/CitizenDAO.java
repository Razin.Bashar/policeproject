package citizen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import login.LoginDTO;
import repository.RepositoryManager;
import util.NavigationService;

import user.UserDTO;
import user.UserDAO;
import user.UserRepository;


public class CitizenDAO  implements NavigationService{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	private void printSql(String sql)
	{
		 System.out.println("sql: " + sql);
	}
	
	private void printSqlUpdated(String sql)
	{
		 System.out.println("Updated sql: " + sql);
	}

	private void recordUpdateTime(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"citizen");
		ps.execute();
		ps.close();
	}
	
	private void recordUpdateTimeInUserTable(Connection connection, PreparedStatement ps, long lastModificationTime) throws SQLException
	{
		String query = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,lastModificationTime);
		ps.setString(2,"user");
		ps.execute();
		ps.close();
	}
	
	
	private void addLastIDTovbSequencer(Connection connection, PreparedStatement ps, long id) throws SQLException
	{
		String query = "UPDATE vbSequencer SET next_id=? WHERE table_name=?";
		ps = connection.prepareStatement(query);
		ps.setLong(1,id);
		ps.setString(2,"citizen");
		ps.execute();
		ps.close();
	}
	
	public UserDTO getUserDTO(CitizenDTO citizenDTO)
	{
		UserDTO userDTO = new UserDTO();
		// userDTO.ID = citizenDTO.iD;
		// userDTO.userName = citizenDTO.email;
		// userDTO.fullName = citizenDTO.name;
		// userDTO.password = citizenDTO.password;
		// userDTO.phoneNo = citizenDTO.phone;
		// userDTO.roleID = 6003;
		// userDTO.mailAddress = citizenDTO.email;
		// userDTO.userType = 4;

		return userDTO;
	}
	
	public UserDTO fillUserDTO(CitizenDTO citizenDTO, UserDTO userDTO)
	{
		// userDTO.ID = citizenDTO.iD;
		// userDTO.fullName = citizenDTO.name;
		// userDTO.phoneNo = citizenDTO.phone;
		// userDTO.mailAddress = citizenDTO.email;

		return userDTO;
	}
		
		
	
	public long addCitizen(CitizenDTO citizenDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DatabaseManager.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			citizenDTO.iD = DatabaseManager.getInstance().getNextSequenceId("Citizen");

			String sql = "INSERT INTO citizen";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "nid";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "phone_number";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_details";
			sql += ", ";
			sql += "mail_id";
			sql += ", ";
			sql += "isDeleted";
			sql += ", lastModificationTime)";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ?)";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			// UserDAO userDAO = new UserDAO();
			// userDAO.addUser(getUserDTO(citizenDTO));
			// citizenDTO.iD = userDAO.getUserDTOByUsername(citizenDTO.email).ID;
			

			int index = 1;

			//System.out.println("Setting object" + citizenDTO.iD + " in index " + index);
			ps.setObject(index++,citizenDTO.iD);
			//System.out.println("Setting object" + citizenDTO.nid + " in index " + index);
			ps.setObject(index++,citizenDTO.nid);
			//System.out.println("Setting object" + citizenDTO.name + " in index " + index);
			ps.setObject(index++,citizenDTO.name);
			//System.out.println("Setting object" + citizenDTO.phoneNumber + " in index " + index);
			ps.setObject(index++,citizenDTO.phoneNumber);
			//System.out.println("Setting object" + citizenDTO.address + " in index " + index);
			ps.setObject(index++,citizenDTO.address);
			//System.out.println("Setting object" + citizenDTO.addressDetails + " in index " + index);
			ps.setObject(index++,citizenDTO.addressDetails);
			//System.out.println("Setting object" + citizenDTO.mailId + " in index " + index);
			ps.setObject(index++,citizenDTO.mailId);
			//System.out.println("Setting object" + citizenDTO.isDeleted + " in index " + index);
			ps.setObject(index++,citizenDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			//recordUpdateTimeInUserTable(connection, ps, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("ex = " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){e.printStackTrace();}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){ex2.printStackTrace();}
		}
		//CitizenRepository.getInstance().reload(false);	
		return citizenDTO.iD;
	}
	
	//need another getter for repository
	public CitizenDTO getCitizenDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CitizenDTO citizenDTO = null;
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "nid";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "phone_number";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_details";
			sql += ", ";
			sql += "mail_id";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM citizen";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				citizenDTO = new CitizenDTO();

				citizenDTO.iD = rs.getLong("ID");
				citizenDTO.nid = rs.getString("nid");
				citizenDTO.name = rs.getString("name");
				citizenDTO.phoneNumber = rs.getString("phone_number");
				citizenDTO.address = rs.getInt("address");
				citizenDTO.addressDetails = rs.getString("address_details");
				citizenDTO.mailId = rs.getString("mail_id");
				citizenDTO.isDeleted = rs.getBoolean("isDeleted");

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return citizenDTO;
	}
	
	public void updateCitizen(CitizenDTO citizenDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DatabaseManager.getInstance().getConnection();

			String sql = "UPDATE citizen";
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "nid=?";
			sql += ", ";
			sql += "name=?";
			sql += ", ";
			sql += "phone_number=?";
			sql += ", ";
			sql += "address=?";
			sql += ", ";
			sql += "address_details=?";
			sql += ", ";
			sql += "mail_id=?";
			sql += ", ";
			sql += "isDeleted=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + citizenDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			//System.out.println("Setting object" + citizenDTO.iD + " in index " + index);
			ps.setObject(index++,citizenDTO.iD);
			//System.out.println("Setting object" + citizenDTO.nid + " in index " + index);
			ps.setObject(index++,citizenDTO.nid);
			//System.out.println("Setting object" + citizenDTO.name + " in index " + index);
			ps.setObject(index++,citizenDTO.name);
			//System.out.println("Setting object" + citizenDTO.phoneNumber + " in index " + index);
			ps.setObject(index++,citizenDTO.phoneNumber);
			//System.out.println("Setting object" + citizenDTO.address + " in index " + index);
			ps.setObject(index++,citizenDTO.address);
			//System.out.println("Setting object" + citizenDTO.addressDetails + " in index " + index);
			ps.setObject(index++,citizenDTO.addressDetails);
			//System.out.println("Setting object" + citizenDTO.mailId + " in index " + index);
			ps.setObject(index++,citizenDTO.mailId);
			//System.out.println("Setting object" + citizenDTO.isDeleted + " in index " + index);
			ps.setObject(index++,citizenDTO.isDeleted);
			System.out.println(ps);
			ps.executeUpdate();
			

			// UserDAO userDAO = new UserDAO();
			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(citizenDTO.iD);
			// if(userDTO == null)
			// {
				// System.out.println("null userdto");
			// }
			// else
			// {
				// userDTO = fillUserDTO(citizenDTO, userDTO);
				// System.out.println(userDTO);
				// userDAO.updateUser(userDTO);
			// }
			
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
						
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//CitizenRepository.getInstance().reload(false);
	}
	
	public void deleteCitizenByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE citizen";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DatabaseManager.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			
			// UserDAO userDAO = new UserDAO();			
			// UserDTO userDTO = UserRepository.getUserDTOByUserID(ID);
			// userDAO.deleteUserByUserID(ID);
			
			recordUpdateTime(connection, ps, lastModificationTime);
			// recordUpdateTimeInUserTable(connection, ps, lastModificationTime);
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
	//2 versions, big table and small table
	//also a repo version
	//Returns a single DTO
	private List<CitizenDTO> getCitizenDTOByColumn(String filter){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CitizenDTO citizenDTO = null;
		List<CitizenDTO> citizenDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "nid";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "phone_number";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_details";
			sql += ", ";
			sql += "mail_id";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM citizen";
			
			
			sql += " WHERE " +  filter;
			
			printSql(sql);
		
			logger.debug(sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);
			


			if(rs.next()){
				citizenDTO = new CitizenDTO();
				citizenDTO.iD = rs.getLong("ID");
				citizenDTO.nid = rs.getString("nid");
				citizenDTO.name = rs.getString("name");
				citizenDTO.phoneNumber = rs.getString("phone_number");
				citizenDTO.address = rs.getInt("address");
				citizenDTO.addressDetails = rs.getString("address_details");
				citizenDTO.mailId = rs.getString("mail_id");
				citizenDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = citizenDTO.iD;
				while(i < citizenDTOList.size() && citizenDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				citizenDTOList.add(i,  citizenDTO);
				//citizenDTOList.add(citizenDTO);
				// INSERTion sort

			}
						
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return citizenDTOList;
	}
	
	public List<CitizenDTO> getCitizenDTOByColumn(String column, String value){
		String filter = column + " = '" + value + "'";
		return getCitizenDTOByColumn(filter);
	}
	
	public List<CitizenDTO> getCitizenDTOByColumn(String column, int value){
		String filter = column + " = " + value;
		return getCitizenDTOByColumn(filter);
	}
	
	public List<CitizenDTO> getCitizenDTOByColumn(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getCitizenDTOByColumn(filter);
	}
		
	
	
	public List<CitizenDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		CitizenDTO citizenDTO = null;
		List<CitizenDTO> citizenDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return citizenDTOList;
		}
		try{
			
			String sql = "SELECT ";
			sql += "ID";
			sql += ", ";
			sql += "nid";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "phone_number";
			sql += ", ";
			sql += "address";
			sql += ", ";
			sql += "address_details";
			sql += ", ";
			sql += "mail_id";
			sql += ", ";
			sql += "isDeleted";
			sql += " FROM citizen";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				citizenDTO = new CitizenDTO();
				citizenDTO.iD = rs.getLong("ID");
				citizenDTO.nid = rs.getString("nid");
				citizenDTO.name = rs.getString("name");
				citizenDTO.phoneNumber = rs.getString("phone_number");
				citizenDTO.address = rs.getInt("address");
				citizenDTO.addressDetails = rs.getString("address_details");
				citizenDTO.mailId = rs.getString("mail_id");
				citizenDTO.isDeleted = rs.getBoolean("isDeleted");
				System.out.println("got this DTO: " + citizenDTO);
				//citizenDTOList.add(citizenDTO);
				int i = 0;
				long primaryKey = citizenDTO.iD;
				while(i < citizenDTOList.size() && citizenDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				citizenDTOList.add(i,  citizenDTO);

			}			
			
		}catch(Exception ex){
			System.out.println("got this database error: " + ex);
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return citizenDTOList;
	
	}

	
	public Collection getIDs(LoginDTO loginDTO) 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = "SELECT ID FROM citizen";

		sql += " WHERE isDeleted = 0";
		
		printSql(sql);
		
        try
        {
	        connection = DatabaseManager.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e.toString(), e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DatabaseManager.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e.toString());}
        }
        return data;
    }
	
	//add repository
	public List<CitizenDTO> getAllCitizen (boolean isFirstReload)
    {
		List<CitizenDTO> citizenDTOList = new ArrayList<>();

		String sql = "SELECT * FROM citizen";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DatabaseManager.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				CitizenDTO citizenDTO = new CitizenDTO();
				citizenDTO.iD = rs.getLong("ID");
				citizenDTO.nid = rs.getString("nid");
				citizenDTO.name = rs.getString("name");
				citizenDTO.phoneNumber = rs.getString("phone_number");
				citizenDTO.address = rs.getInt("address");
				citizenDTO.addressDetails = rs.getString("address_details");
				citizenDTO.mailId = rs.getString("mail_id");
				citizenDTO.isDeleted = rs.getBoolean("isDeleted");
				int i = 0;
				long primaryKey = citizenDTO.iD;
				while(i < citizenDTOList.size() && citizenDTOList.get(i).iD < primaryKey)
				{
					i ++;
				}
				citizenDTOList.add(i,  citizenDTO);
				//citizenDTOList.add(citizenDTO);
			}			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DatabaseManager.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return citizenDTOList;
    }
	
	//normal table, transaction table, int, float
	private List<Long> getIDsWithSearchCriteria(String filter){
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try{

			String sql = "SELECT ID FROM citizen";
			
			sql += " WHERE isDeleted = 0";

				
			sql+= " AND  ";  
			sql+= filter;

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, String value){
		String filter = column + " LIKE '" + value + "'";
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, int value){
		String filter = column + " = " + value;
		return getIDsWithSearchCriteria(filter);
	}
	
	public List<Long> getIDsWithSearchCriteria(String column, double valueBase, double valueDelta){
		String filter = column + " BETWEEN " + (valueBase - valueDelta) + " AND " + (valueBase + valueDelta);
		return getIDsWithSearchCriteria(filter);
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception
    {
		System.out.println("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		CitizenDTO citizenDTO = new CitizenDTO();
		try{

			String sql = "SELECT ID FROM citizen";
			
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			String AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = CitizenMAPS.GetInstance().java_custom_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			String AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(CitizenMAPS.GetInstance().java_type_map.get(str.toLowerCase()) != null &&  !CitizenMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(CitizenMAPS.GetInstance().java_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
			sql += " WHERE ";
			sql += " isDeleted = false";				
			
			
			if(!AnyfieldSql.equals("()"))
			{
				sql += " AND " + AnyfieldSql;
				
			}
			if(!AllFieldSql.equals("()"))
			{			
				sql += " AND " + AllFieldSql;
			}
			
			

			printSql(sql);
		
			connection = DatabaseManager.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			System.out.println("Sql error: " + ex);
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){
					DatabaseManager.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO, ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO,
			ArrayList<String> table_names) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

		
}
	