
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="report.ReportAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle.VehicleAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
	String url = "ReportServlet?actionType=search";
	String navigator = SessionConstants.NAV_REPORT;
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String actionName = "add";
	String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
	  int i = 0;
%>

<%
	String context = "../../.." + request.getContextPath() + "/";
%>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<style>
	.page-content-wrapper{
		max-width:1250px!important;
	}
</style>
<div class="portlet box">
	<div class="portlet-body" style="">
	
	<div class="workspace"
   style="display: flex; flex-direction: column; align-items: center;">

		<style>
			.workspace {margin: 0; padding: 0;}

				
				@media(min-width: 600px){
					.workspace ul{
						columns: 2;
						-webkit-columns: 2;
						-moz-columns: 2;
					}
				}
				
				@media(max-width: 600px){
					.workspace ul{
						columns: 1;
						-webkit-columns: 1;
						-moz-columns: 1;
					}
				}
				.workspace li {
				 padding: 5px 0px;
				}
				
				.workspace p,h3 {
				  text-align: left;
				  margin-left:15px;
				  display:inline-block;
				}
				
				td > * {
				    vertical-align : middle;
				}
		</style>
		<div style="zoom:140%;width:100%;">
		<%
			out.print("<ul style=\" width: 100%;\">");
		      ArrayList<Vehicle_typeDTO> typeDTOs = (ArrayList<Vehicle_typeDTO>) new Vehicle_typeDAO()
		      		.getAllVehicle_type(true);
		      
		      for (int iii = 0; iii < typeDTOs.size(); iii++) {
		      
		      //	out.print("<a id=\"type_"+(int) typeDTOs.get(iii).iD+"\" class=\"filter vehicleType\"  onclick=\"find(" + (int) typeDTOs.get(iii).iD + ")\" >"
		      	//		+ VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(iii).iD, "vehicle_type") + "</a>");
		      
		      			if(iii<4)out.print("<li style=\"\">");
		      			else out.print("<li>");
		      				out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(iii).iD+"' style=\"color:#666;display:flex;align-items:center;\">");
		      				out.print("<img src=\""+request.getContextPath()+typeDTOs.get(iii).logo+"\" style=\"max-width: 15%;\">");
		      				out.print("<h3 style=\"color: purple;\">"+VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(iii).iD, "vehicle_type")+"</h3>");
	      					
		      			out.print("</a>");
		      	out.print("</li>");
		      }
		  	out.print("</ul>");
		      
		  
/*
		      typeDTOs = (ArrayList<Vehicle_typeDTO>) new Vehicle_typeDAO()
		      		.getAllVehicle_type(true);
			out.print("<table style=\"width:100%;\">");
		      int c1=0,c2=4,t=4;
		      for (int iii = 0; iii < typeDTOs.size(); iii++) {
		      
		      	//out.print("<a id=\"type_"+(int) typeDTOs.get(iii).iD+"\" class=\"filter vehicleType\"  onclick=\"find(" + (int) typeDTOs.get(iii).iD + ")\" >"
		      	//		+ VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(iii).iD, "vehicle_type") + "</a>");
		      
		      			//if(iii<4&&iii==-1)out.print("<li style=\"padding-bottom: 20px;\">");
		      			//else ;
		      			//	out.print("<li>");
		      			//	out.print("<a href='ReportServlet?actionType=search&land=1&flstatus="+request.getParameter("flstatus")+"&type="+(int) typeDTOs.get(iii).iD+"' style=\"color:#666;\">");
		      			//	out.print("<img src=\""+request.getContextPath()+typeDTOs.get(iii).logo+"\" style=\"max-width: 15%;\">");
		      			//	out.print("<h3 style=\"color: purple;\">"+VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(iii).iD, "vehicle_type")+"</h3>");
	      					
		      			//out.print("</a>");
		      	//out.print("</li>");
		      	c1 = iii;
		      	c2=iii+4;
		      	if(c2>=typeDTOs.size())break;
		      	if(c1<t){
		      		%>
		      			<tr class="spacer">
		      				<td>
		      					
		      					<%		
		      							out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(c1).iD+"' style=\"color:#666;\">");
		      							out.print("<img src=\""+request.getContextPath()+typeDTOs.get(c1).logo+"\" style=\"width: 60px;float:right;padding-right:10px;\">");
		    		      				
		    		      				out.print("</a>"); 
		    		      		%>
		      					
		      				</td>
		      				<td style="display: flex;
									    justify-content: space-between;
									    align-items: center;">
		      					<%
		      					out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(c1).iD+"' style=\"color:#666;\">");
		      							out.print("<h3 style=\"color: purple;display: inline-block;float:left;margin:0px!important;padding:10px 0px;\">"+VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(c1).iD, "vehicle_type")+"</h3>");
		      							out.print("</a>"); 
		      							out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(c2).iD+"' style=\"color:#666;\">");		
		      					out.print("<img  src=\""+request.getContextPath()+typeDTOs.get(c2).logo+"\" style=\"width: 60px;float:right;padding-right:10px;\">");
		      					out.print("</a>"); 		
		      					
		      					%>
		      				</td>
		      				<td>
		      					<%		
		      							out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(c2).iD+"' style=\"color:#666;\">");
		      							out.print("<h3 style=\"color: purple;display: inline-block;float:left;margin:0px!important;\">"+VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(c2).iD, "vehicle_type")+"</h3>");
		    		      				out.print("</a>"); 
		    		      		%>
		      				</td>
		      			</tr>
		      		<%
		      	}else{
		      		%>
	      			<tr class="spacer" >
	      				<td >
		      					
		      				</td>
		      				<td style="display: flex; justify-content: flex-end; align-items: center;">
		      					<%
		      							
		      							out.print("<img  src=\""+request.getContextPath()+typeDTOs.get(c2).logo+"\" style=\"width: 60px;float:right;padding:10px 0px;padding-right:10px;\">");
		      							
		      					
		      					%>
		      				</td>
		      				<td>
		      					<%		
		      							out.print("<a href='ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+(int) typeDTOs.get(c2).iD+"' style=\"color:#666;\">");
		      							out.print("<h3 style=\"color: purple;display: inline-block;float:left;margin:0px!important;\">"+VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(c2).iD, "vehicle_type")+"</h3>");
		    		      				out.print("</a>"); 
		    		      		%>
		      				</td>
	      			</tr>
	      		<%
		      	}
		      }
		  	out.print("</table>");
		  	
		  	*/
	      %>
		  
		</div>


	</div>
</div>
</div>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js"
	type="text/javascript"></script>



