
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="report.ReportAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle.VehicleAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
	String url = "ReportServlet?actionType=search&land=1";
	String navigator = SessionConstants.NAV_REPORT;
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String actionName = "add";
	String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
	  int i = 0;
%>

<%
	String context = "../../.." + request.getContextPath() + "/";
%>
<style>
	.page-content-wrapper{
		max-width:750px!important;
	}
</style>
<div class="portlet box" style="margin:0px;">
	<div class="portlet-body" style="padding: 26px 0px;">
	
	<div class="workspace"
   style="display: flex; flex-direction: column; align-items: center;">

		<style>
			.workspace {margin: 0; padding: 0;}

				.workspace div {
				  margin: 20px;
				}
				
				.workspace ul {
				  list-style-type: none;
			
				}
				
				.workspace h3 {
				  font: bold 20px/1.5 Helvetica, Verdana, sans-serif;
				}
				
				.workspace li img {
				  float: left;
				  margin: 0 15px 0 0;
				}
				
				.workspace li p {
				  font: 200 12px/1.5 Georgia, Times New Roman, serif;
				}
				
				.workspace li {
				  padding: 10px;
				  overflow: auto;
				}
				
				.workspace li:hover {
				  background: #eee;
				  cursor: pointer;
				}
				
				.workspace p,h3 {
				  text-align: left;
				}
		</style>
		<div style="zoom:110%;">
		  <ul style="margin:0px;">
		    <li style="margin-bottom: 30px;">
		    <a href='ReportServlet?actionType=getLandingPage&&flstatus=1' style="color:#666;">
		      <img src="https://media.socastsrm.com/wordpress/wp-content/blogs.dir/1166/files/2017/09/Lost-7.png" style="max-width: 30%;" >
		      <h3 style="color: red;"><%=LM.getText(LC.REPORT_LANDING_LOST, loginDTO)%></h3>
		      <p><%=LM.getText(LC.LIST_DES_LOST, loginDTO)%></p>
		    </a>
		    </li>
		      
		    <li>
		    <a href='ReportServlet?actionType=getLandingPage&&flstatus=2' style="color:#666;">
		      <img src="http://allthingsd.com/files/2013/01/53912_FoundLogo-HorizontalJPEG-577x200-feature.jpeg" style="max-width: 30%;">
		      <h3><%=LM.getText(LC.REPORT_LANDING_FOUND, loginDTO)%></h3>
		      <p><%=LM.getText(LC.LIST_DES_FOUND, loginDTO)%></p>
		    </a>
		    </li>
		  </ul>
		</div>


	</div>
</div>
</div>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js"
	type="text/javascript"></script>



