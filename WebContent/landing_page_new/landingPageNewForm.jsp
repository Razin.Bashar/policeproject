<%@page import="comment_image.Comment_imageDTO"%>
<%@page import="comment_image.Comment_imageDAO"%>
<%@page import="vehicle_image.Vehicle_imageDTO"%>
<%@page import="java.util.List"%>
<%@page import="vehicle_image.Vehicle_imageDAO"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="comments.CommentsDTO"%>
<%@page import="comments.CommentsDAO"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="image.ImageServlet"%>
<%@page import="report.ReportDAO"%>
<%@page import="org.json.JSONObject"%>
<%@page import="citizen.CitizenDAO"%>
<%@page import="vehicle.VehicleDAO"%>
<%@page import="citizen.CitizenRepository"%>
<%@page import="citizen.CitizenDTO"%>
<%@page import="vehicle.VehicleDTO"%>
<%@page import="vehicle.VehicleRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="report.ReportDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="vehicle.VehicleAnotherDBDAO"%>
<%@ page import="report.ReportAnotherDBDAO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%
   LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
   GeoLocationDAO2 geoLocationDAO = new GeoLocationDAO2();
   String actionName = "add";
   String failureMessage = (String) request.getAttribute("failureMessage");
   if (failureMessage == null || failureMessage.isEmpty()) {
   	failureMessage = "";
   }
   out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
   String value = "";
   String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
   VehicleDAO vehicleDAO = new VehicleDAO();
   CitizenDAO citizenDAO = new CitizenDAO();
   ReportDAO reportDAO = new ReportDAO();
   CommentsDAO commentsDAO = new CommentsDAO();
   %>
<%
   ReportDTO reportDTO;
   VehicleDTO vehicleDTO = null;
   CitizenDTO citizenDTO = null;
   reportDTO = (ReportDTO) request.getAttribute("reportDTO");
   //LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
   if (reportDTO == null) {
   	reportDTO = new ReportDTO();
   
   } else {
   	vehicleDTO = new VehicleDAO().getVehicleDTOByID(reportDTO.vehicleId);
   	citizenDTO = new CitizenDAO().getCitizenDTOByID(reportDTO.reporterId);
   
   	if (vehicleDTO == null)
   		vehicleDTO = new VehicleDTO();
   	if (citizenDTO == null)
   		citizenDTO = new CitizenDTO();
   }
   System.out.println("reportDTO = " + reportDTO);
   
   String ID = request.getParameter("ID");
   if (ID == null || ID.isEmpty()) {
   	ID = "0";
   }
   System.out.println("ID = " + ID);
   int i = 0;
   %>

   <%
      ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_REPORT);
  
      try {
      
      	if (data != null) {
      		 Collections.sort(data, new Comparator<ReportDTO>() {
      		    public int compare(ReportDTO s1, ReportDTO s2) {
      		        return Long.compare(s2.iD, s1.iD);
      		    }
      		});
      		int size = data.size();
      		System.out.println("data not null and size = " + size + " data = " + data);
      		String linkLogo = "";
      		Vehicle_imageDAO vehicle_imageDAO = new Vehicle_imageDAO();
      		for (int ii = 0; ii < size; ii++) {
      			ReportDTO row = (ReportDTO) data.get(ii);
      
      			vehicleDTO = vehicleDAO.getVehicleDTOByID(row.vehicleId);
      			citizenDTO = citizenDAO.getCitizenDTOByID(row.reporterId);
      			ArrayList<JSONObject> imageList = (ArrayList<JSONObject>) reportDAO
      					.getAllImageByReportId(row.iD);
      
      			System.out.println(row.vehicleId + " -- " + row.reporterId);
      			if (vehicleDTO != null && citizenDTO != null) {
      				String deletedStyle = "color:red";
      				if (!row.isDeleted)
      					deletedStyle = "";
      %>
   <div class="postbox"
      style="display: flex; flex-direction: column; width: 100%;">
      <% 
		if(linkLogo.equals(""))linkLogo = VehicleAnotherDBDAO.getLogo(Language, vehicleDTO.typeId, "vehicle_type");
      
    	out.println("<div style=\"display:flex;align-items: center;\"><img src=\""+request.getContextPath()+linkLogo+"\" style=\"  width: 50px; height: 50px;border-radius: 1000px!important;\">");
    	%>
      <label style="font-size: 26px;margin:10px;"> <%
    		  			
         				value = row.statusId + "";
         
         				value = ReportAnotherDBDAO.getName(Language, Integer.parseInt(value), "status");
         
         				out.println(value);
         
         				
         				out.println("<td id = '" + i + "_typeId'>");
         				out.println(VehicleAnotherDBDAO.getName(Language, vehicleDTO.typeId, "vehicle_type"));
         
         				//out.println(row.iD);
         %>
      </label></div>
      <%
      int st = Integer.parseInt(request.getParameter("flstatus"));
      System.out.println(st);
      if(st==2){ 
      	out.print("<p style=\"text-align:left;padding-left:30px;padding-bottom:15px;\">");
     	 
		value = row.thanaAddress + "";
		out.println(geoLocationDAO.getLocationText(Integer.parseInt(value)));
		out.println("Thana");
		out.println(". Contact person SI Mohit ph 01814xxxx");

      	out.print("</p>");
      }
     	%>
      <div class="imagesection"
         style="max-width:70%; margin: auto; nbox-shadow: 0px 0px 50px lightblue;">
         <%
         	List<Vehicle_imageDTO> vehicle_imageDTOs = vehicle_imageDAO.getVehicle_imageDTOByColumn("report_id", ""+row.iD);
            value = row.image1 + "";
            				//out.println("<img src='img2/" + value +"' style=\" max-width:400px;\" >");
            				switch (vehicle_imageDTOs.size()) {
            					case 0 :
            						//out.println("<div style=\"display:grid;grid-template-columns:auto;\">");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						//out.println("</div>");
            
            						break;
            					case 1 :
            						out.println("<div style=\"display:grid;grid-template-columns:100%;\">");
            						for(int ij=0;ij<vehicle_imageDTOs.size();ij++){
            							out.println("<div style=\" width:100%;overflow:hidden;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\"width:100%;min-height:100%; \"  ></div>");
            						}
            						out.println("</div>");
            						break;
            					case 2 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:50% 50%;\">");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						for(int ij=0;ij<vehicle_imageDTOs.size();ij++){
            							out.println("<div style=\" width:100%;overflow:hidden;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\"width:100%;min-height:100%; \"></div>");
            						}
            						out.println("</div>");
            						break;
            					case 3 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:50% 50%;\">");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;grid-column:1/3\" >");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						//out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						for(int ij=0;ij<vehicle_imageDTOs.size();ij++){
            							if(ij==0)out.println("<div style=\" width:100%;overflow:hidden;grid-column:1/ 3;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
            							else out.println("<div style=\" width:100%;overflow:hidden;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
            						}
            						out.println("</div>");
            						break;
            					case 4 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:50% 50%;\">");
            						/*out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");*/
            						for(int ij=0;ij<vehicle_imageDTOs.size();ij++){
            							out.println("<div style=\" width:100%;overflow:hidden;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
            						}
            						out.println("</div>");
            						break;
            					default :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:16.666% 16.666% 16.666% 16.666% 16.666% 16.666%;ngrid-template-rows: 250px 250px;\">");
            						/*out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:1/ 4;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:4/ 7;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:1/ 3;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:3/ 5;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:5/ 7;\" >");*/
            						
            						for(int ij=0;ij<5;ij++){
            							switch(ij){
            							    case 0:out.println("<div  style=\" width:100%;overflow:hidden;grid-column:1/ 4;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
            								       break;
            							    case 1:out.println("<div  style=\" width:100%;overflow:hidden;grid-column:4/ 7;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
     								       			break;
            							    case 2:out.println("<div style=\" width:100%;overflow:hidden;grid-column:1/ 3;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
     								       			break;
            							    case 3:out.println("<div style=\" width:100%;overflow:hidden;grid-column:3/ 5;\"><img  data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
     								       			break;
            							    case 4:out.println("<div style=\" width:100%;overflow:hidden;grid-column:5/ 7;\"><img data-id =\""+ij+"\" src='police_citizen_resource/vehicle_post_image/" + vehicle_imageDTOs.get(ij).name + "' style=\" width:100%;min-height:100%;\" ></div>");
     								       			break;
            							
            							}
            						}
            						out.println("</div>");
            						break;
            				}
            %>
      </div>
      <style type="text/css">
      
		.detailBox {
		    width:320px;
		    border:1px solid #bbb;
		    margin:50px;
		}
		.titleBox {
		    background-color:#fdfdfd;
		    padding:10px;
		}
		.titleBox label{
		  color:#444;
		  margin:0;
		  display:inline-block;
		}
		
		.commentBox {
		    padding:10px;
		    border-top:1px dotted #bbb;
		}
		.commentBox .form-group:first-child, .actionBox .form-group:first-child {
		    width:80%;
		}
		.commentBox .form-group:nth-child(2), .actionBox .form-group:nth-child(2) {
		    width:18%;
		}
		.actionBox .form-group * {
		    width:100%;
		}
		.taskDescription {
		    margin-top:10px 0;
		}
		.commentList {
		    padding:0;
		    list-style:none;
		    overflow:auto;
		}
		.commentList li {
		    margin:0;
		    margin-top:10px;
		}
		.commentList li > div {
		    display:table-cell;
		   
		}
		.commenterImage {
		    width:30px;
		    margin-right:5px;
		    height:100%;
		    float:left;
		}
		.commenterImage img {
		    width:100%;
		    border-radius:50%;
		}
		.commentText p {
		    margin:0;
		}
		.sub-text {
		    color:#aaa;
		    font-family:verdana;
		    font-size:11px;
		}
		.actionBox {
		    border-top:1px dotted #bbb;
		    padding:10px;
		}
		.commentList p{
			font-size: 17px;
		}
      </style>
      <div class="commentsection" style="padding-left: 20px;
    padding-bottom: 100px;">
         <a href="#" style="font-size: 20px;" ><%=LM.getText(LC.REPORT_LANDING_COMMENT, loginDTO)%></a> 
         
          <div class="commentBox">
        
         <p class="taskDescription" style="text-align:left;"><% out.print((row.moreDetail!=null&&!row.moreDetail.trim().equals(""))?row.moreDetail:"I have lost my vehicle.please find it"); %> </p> 
    </div>
   
    <div class="actionBox" style="zoom: 97%;
    padding-left: 20px;">
    <% 
       out.print("<ul id=\"commentList_"+row.iD+"\" class=\"commentList\">");
    SimpleDateFormat format_lostDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String[] monthNames = {"January", "February", "March", "April", "May", "June",
                      "July", "August", "September", "October", "November", "December"};
    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat outputFormat = new SimpleDateFormat("' 'KK:mm a");
    	ArrayList<CommentsDTO> commentDTOs = (ArrayList<CommentsDTO>)commentsDAO.getCommentsDTOByColumn("report_id", row.iD);
    	Comment_imageDAO comment_imageDAO = new Comment_imageDAO();
    	for(int j=0;j<commentDTOs.size();j++){
    		List<Comment_imageDTO> comment_imageDTOs = comment_imageDAO.getComment_imageDTOByColumn("comment_id", commentDTOs.get(j).iD+"");
    		out.print("<li>");
    			out.print("<div class=\"commenterImage\">");
    				out.print("<img src=\""+request.getContextPath()+"/assets/images/defaultuser.png\" />");
    			out.print("</div>");
    			out.print("<div class=\"commentText imagesection\">");
    				Date date = new Date(commentDTOs.get(j).commentDate);
    				String formatted_lostDate = format_lostDate.format(commentDTOs.get(j).commentDate).toString();
    				for(int ij=0;ij<comment_imageDTOs.size();ij++){
    					out.print("<img data-id =\""+ij+"\" style=\"max-width:75px;max-height:75px;\" src='police_citizen_resource/vehicle_comment_image/" + comment_imageDTOs.get(ij).name + "'/>");
    				}
    				out.print(" <p style=\"text-align:left;\">"+commentDTOs.get(j).comment+"</p> <span class=\"date sub-text\">on "+date.getDate()+" "+monthNames[date.getMonth()]+" "+(2000+date.getYear())+", "+new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)+outputFormat.format(inputFormat.parse(formatted_lostDate))+"</span>");
				out.print("</div>");
    		out.print("</li>");
    	}
    	out.print("</ul>");
    
    %>
     <!--        <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/50/50" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
            <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/45/45" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
            <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/40/40" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
        </ul>-->
        <div class="form-inline" role="form" style="border: 1px solid lightgrey;
    border-radius: 50px!important;
    height: 37px;display:flex;width: 93%;align-items:center;">
            <div class="form-group" style="margin: 0px!important;flex-grow:1;">
            <%
                out.print("<input style=\"border: none; padding: 0px 15px!important;margin-left: 10px;\" id=\"cmt_"+row.iD+"\" data-cid=\""+row.iD+"\" class=\"form-control cid\" type=\"text\" placeholder=\""+LM.getText(LC.REPORT_LANDING_COMMENT_PLACEHOLDER, loginDTO)+"\" />");
            %>
            </div>
            
            <% 
            	//out.print("<button type=\"button\" style=\"width: 30px;height: 30px;background: transparent;border: none;color: navy;border: 1px solid lightgray;border-radius: 100px!important;padding: 0px;margin-right:5px;\" onclick=\"docomment("+row.iD+")\"><i style=\"font-size: 18px; margin-top: 5px;\" class=\"fa fa-image\" aria-hidden=\"true\"></i></button>");
            	out.print("<button type=\"button\" style=\"width: 30px;height: 30px;background: transparent;border: none;color: navy;border: 1px solid lightgray;border-radius: 100px!important;padding: 0px;margin-right:5px;\" onclick=\"IUpload("+row.iD+")\"><i style=\"font-size: 18px; margin-top: 5px;\" class=\"fal fa-camera\" aria-hidden=\"true\"></i></button>");
                //out.print("<button type=\"button\" style=\"width: 30px;height: 30px;background: transparent;border: none;color: green;border: 1px solid lightgray;border-radius: 100px!important;padding: 0px;margin-right:5px;\" onclick=\"docomment("+row.iD+")\"><i style=\"font-size: 18px; margin-top: 5px;\" class=\"fa fa-comment\" aria-hidden=\"true\"></i></button>");
            	out.print("<input type='file' style=\"display: none;\" name='ImUpload"+row.iD+"'/>");
                %>
         
         		
        </div>
        </div>
    </div>
         
         
      
   </div>
   <%
      }
      		}
      
      		System.out.println("printing done");
      	} else {
      		System.out.println("data  null");
      	}
      } catch (Exception e) {
      	System.out.println("JSP exception " + e);
      }
      %>
</div>
<%
   String navigator2 = SessionConstants.NAV_REPORT;
   System.out.println("navigator2 = " + navigator2);
   RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
   System.out.println("rn2 = " + rn2);
   String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
   String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
   %>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

$('.cid').keypress(function(e) {
    if(e.which == 13) {
        var cid = $(this).attr("data-cid");
        console.log(cid);
        docomment(cid);
    }
});
function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
function convert(date) {
    if(date==null||date=='null'||date==undefined||date=='undefined'||date=='')return '';
var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];
var d = new Date(date);
var dayName = days[d.getDay()];
var Year = d.getFullYear();
var monthName = monthNames[d.getMonth()];
return d.getDate()+' '+monthName+' '+Year+', '+dayName+' '+formatAMPM(date);
}

var commentImg=[];
function IUpload(id){
	console.log("yes");
	$("input[name='ImUpload"+id+"']").click();
	$("input[name='ImUpload"+id+"']").on('change',function(e){
	    
		   // var file;
		   // var length= e.target.files.length;
		   // for(var i = 0;i<length;i++){
		   // 	file  = e.target.files[i];
		   // 	var img = $('<img style="max-width:75px;max-height:75px;" src="'+ URL.createObjectURL(file)+'"/>');
		   // 	$('#imagePreviewField').append(img);
		   // }
		    commentImg = e.target.files;
		    //submitAjax(0,e.target.files);
		});
}



function getImage(){
		 var file;
		 var img='';
	   var length= commentImg.length;
	    for(var i = 0;i<length;i++){
	    	file  = commentImg[i];
	   	    img += '<img style="max-width:75px;max-height:75px;" src="'+ URL.createObjectURL(file)+'"/>';
	    }
	    return img;
}
function docomment(id){
	if($('#cmt_'+id).val()==''&&commentImg.length==0){
		alert('Write your comment or Upload image');
		return;
	}
	var data = new FormData();
	data.append('reportId',id);
	data.append('comment', $('#cmt_'+id).val());
	var d = new Date().getTime();
	data.append('commentDate', d); 
	
	data.append("cmtimgCount",commentImg.length);
 	 $.each(commentImg, function(k, value)
            {
 		data.append('cmtimage'+k, value);
            });

 	
	
 	 var c =data.get('comment');

	$.ajax({
        type:"POST",
        url :"ReportServlet?actionType=cmtadd",
        data : data,
        processData: false,
        contentType: false,
        async: true,
        success : function(response) {
          // alert('success');
           
          var p = '<p class="" style="text-align:left;">'+c+'</p> <span class="date sub-text">on '+convert(new Date(d))+'</span> ';
          var im = (commentImg.length>0)?getImage():''; 
          var temp = $('<li>  ' +    
           '  <div class="commenterImage "> '+
           '          <img src=\"<%=request.getContextPath()%>/assets/images/defaultuser.png\" />   '+
           '  </div> '+
           '  <div class="commentText imagesection">  '+im +p+
           '  </div>'+
           '</li>');
           
           $('#commentList_'+id).append(temp);
           commentImg=[];
       	 $('#cmt_'+id).val("");
        },
        error: function(response) {
            alert('error');
        }
    });
}


   function PreprocessBeforeSubmiting(row, validate) {
   	console
   			.log("found date = "
   					+ document.getElementById('reportingDate_date_Date_'
   							+ row).value);
   	document.getElementById('reportingDate_date_' + row).value = new Date(
   			document.getElementById('reportingDate_date_Date_' + row).value)
   			.getTime();
   	console.log("found date = "
   			+ document.getElementById('lostDate_date_Date_' + row).value);
   	document.getElementById('lostDate_date_' + row).value = new Date(
   			document.getElementById('lostDate_date_Date_' + row).value)
   			.getTime();
   	console.log("found date = "
   			+ document.getElementById('foundDate_date_Date_' + row).value);
   	document.getElementById('foundDate_date_' + row).value = new Date(
   			document.getElementById('foundDate_date_Date_' + row).value)
   			.getTime();
   	if (validate) {
   		var empty_fields = "";
   		var i = 0;
   		if (document.getElementById('name_text_' + row).value == "") {
   			empty_fields += "'name'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('phoneNumber_text_' + row).value == "") {
   			empty_fields += "'phoneNumber'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('address_geolocation_' + row).value == "") {
   			empty_fields += "'address'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('addressDetails_textarea_' + row).value == "") {
   			empty_fields += "'addressDetails'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('mailId_text_' + row).value == "") {
   			empty_fields += "'mailId'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('modelName_text_' + row).value == "") {
   			empty_fields += "'modelName'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('thanaAddress_geolocation_' + row).value == "") {
   			empty_fields += "'thanaAddress'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (empty_fields != "") {
   			alert(empty_fields + " cannot be empty.");
   			return false;
   		}
   	}
   	return true;
   }
   function addrselected(value, htmlID, selectedIndex, tagname, geodiv,
   		hiddenfield) {
   	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
   	try {
   		document.getElementById(hiddenfield).value = value;
   		var elements = document.getElementById(geodiv).children;
   		var ids = '';
   		for (var i = elements.length - 1; i >= 0; i--) {
   			var elemID = elements[i].id;
   			if (elemID.includes(htmlID) && elemID > htmlID) {
   				ids += elements[i].id + ' ';
   
   				for (var j = elements[i].options.length - 1; j >= 0; j--) {
   
   					elements[i].options[j].remove();
   				}
   				elements[i].remove();
   
   			}
   		}
   
   		var newid = htmlID + '_1';
   
   		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='"
   				+ tagname
   				+ "' id = '"
   				+ newid
   				+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '"
   				+ geodiv + "', '" + hiddenfield + "')\"></select>";
   		console.log('innerHTML= '
   				+ document.getElementById(geodiv).innerHTML);
   		document.getElementById(htmlID).options[0].innerHTML = document
   				.getElementById(htmlID).options[selectedIndex].innerHTML;
   		document.getElementById(htmlID).options[0].value = document
   				.getElementById(htmlID).options[selectedIndex].value;
   		console.log('innerHTML again = '
   				+ document.getElementById(geodiv).innerHTML);
   
   		var xhttp = new XMLHttpRequest();
   		xhttp.onreadystatechange = function() {
   			if (this.readyState == 4 && this.status == 200) {
   				if (!this.responseText.includes('option')) {
   					document.getElementById(newid).remove();
   				} else {
   					document.getElementById(newid).innerHTML = this.responseText;
   				}
   
   			} else if (this.readyState == 4 && this.status != 200) {
   				alert('failed ' + this.status);
   			}
   		};
   
   		var redirect = "geolocation/geoloc.jsp?myID=" + value;
   
   		xhttp.open("GET", redirect, true);
   		xhttp.send();
   	} catch (err) {
   		alert("got error: " + err);
   	}
   	return;
   }
   
   function addHTML(id, HTML) {
   	document.getElementById(id).innerHTML += HTML;
   }
   
   function getRequests() {
   	var s1 = location.search.substring(1, location.search.length)
   			.split('&'), r = {}, s2, i;
   	for (i = 0; i < s1.length; i += 1) {
   		s2 = s1[i].split('=');
   		r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
   	}
   	return r;
   }
   
   function Request(name) {
   	return getRequests()[name.toLowerCase()];
   }
   
   function ShowExcelParsingResult(suffix) {
   	var failureMessage = document
   			.getElementById("failureMessage_" + suffix);
   	if (failureMessage == null) {
   		console.log("failureMessage_" + suffix + " not found");
   	}
   	console.log("value = " + failureMessage.value);
   	if (failureMessage != null && failureMessage.value != "") {
   		alert("Excel uploading result:" + failureMessage.value);
   	}
   }
   
   function init(row) {
   
   	var xhttp = new XMLHttpRequest();
   	xhttp.onreadystatechange = function() {
   		if (this.readyState == 4 && this.status == 200) {
   			document.getElementById('address_geoSelectField_' + row).innerHTML = this.responseText;
   			document.getElementById('thanaAddress_geoSelectField_' + row).innerHTML = this.responseText;
   		} else if (this.readyState == 4 && this.status != 200) {
   			alert('failed ' + this.status);
   		}
   	};
   	xhttp.open("GET", "geolocation/geoloc.jsp?myID=1", true);
   	xhttp.send();
   }
   function doEdit(params, i, id, deletedStyle) {
   	var xhttp = new XMLHttpRequest();
   	xhttp.onreadystatechange = function() {
   		if (this.readyState == 4 && this.status == 200) {
   			if (this.responseText != '') {
   				var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle
   						+ "')";
   				document.getElementById('tr_' + i).innerHTML = this.responseText;
   				document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
   				document.getElementById(i + '_Submit').innerHTML += "<a onclick=\""+ onclickFunc +"\">Submit</a>";
   				document.getElementById('tr_' + i).innerHTML += "<td>"
   						+ "<input type='checkbox' name='ID' value='" + id + "'/>"
   						+ "</td>";
   				init(i);
   			} else {
   				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
   			}
   		} else if (this.readyState == 4 && this.status != 200) {
   			alert('failed ' + this.status);
   		}
   	};
   
   	xhttp
   			.open("Get", "ReportServlet?actionType=getEditPage" + params,
   					true);
   	xhttp.send();
   }
   
   function fixedToEditable(i, deletedStyle, id) {
   	console.log('fixedToEditable called');
   	var params = '&identity=' + id + '&inplaceedit=true' + '&deletedStyle='
   			+ deletedStyle + '&ID=' + id + '&rownum=' + i + '&dummy=dummy';
   	console.log('fixedToEditable i = ' + i + ' id = ' + id);
   	doEdit(params, i, id, deletedStyle);
   
   }
   window.onload = function() {
   	ShowExcelParsingResult('general');
   }
</script>

<script type="text/javascript">
	var previous =1;
	var current = 0;
	var timagecount =1;
	var lock =0;
  $(document).on('click','.imagesection',function(e){
	  console.log(e.target);
	  var img = $('img',this);
	  //console.log(img);
	  $('.modal-body').empty();
	  timagecount = img.length;
	  
	  var startpoint = $(e.target).attr('data-id');
	  if(startpoint!=undefined || startpoint!=null){
		  next =(parseInt(startpoint)+1)%timagecount;
		  current = parseInt(startpoint);
	  }else{
	  next =1;
	  current = 0;
	  }
	 
	  for(var i=0;i<img.length;i++){
		  var image = new Image();
		  image.src = $(img[i]).attr('src');
		 // alert('width: ' + image.naturalWidth + ' and height: ' + image.naturalHeight);
		  var ratio = image.naturalWidth/image.naturalHeight;
		  var style='';
		  if(ratio<1){
			  var h =0;
			  if(image.naturalHeight<$(window).height())h=image.naturalHeight;
			  else h=$(window).height();
			  style = 'height:'+h+'px';
		  }else {
			  var w =0;
			  if(image.naturalWidth<$(window).width())w=image.naturalWidth;
			  else w=$(window).width()/2;
			  style = 'width:'+w+'px';
		  }
		  
	  		$('.modal-body').append('<img id="modalimagepreview'+i+'" style="'+style+';display:none;" src="'+image.src+'">');
	  }
	  $('.modal-body #modalimagepreview'+(current)).css('display','block');
	 
	  if(timagecount)$('#exampleModalCenter').modal('show');
  });
  
  $(document).on('click','.modal-body',function(){
	  if(lock==1)return;
	  lock=1;
	  previous = current;
	  current = (current +1)%timagecount;
	  var psel = '.modal-body #modalimagepreview'+previous;
	  var nsel = '.modal-body #modalimagepreview'+current;
	  console.log('previous->'+psel);
	  console.log('current->'+nsel);
	  $(psel).fadeOut('slow',function(){
		  $(nsel).fadeIn('slow',function(){
			  lock =0;
		  });
	  });
	  
	  
  });
</script>
<img id='iamgeTest' src='' style='display:none;'>
<div class="modal fade" id="exampleModalCenter"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    align-items: center;
        margin: 0px;
    justify-content: center;">
    <div class="modal-content">
      <a href='#' data-dismiss="modal" aria-label="Close" style="    margin-right: 5px;
    z-index: 100000000000;
    position: absolute;
    color: white;
    background: black;
    right: 0px;
    padding: 0px;
    margin: 0px;
    display: flex;">
          <i style="margin: 0px;
    padding: 0px;" class="fas fa-times"></i>
        </a>
      <div class="modal-body" style="padding:0px;">
        
      </div>
    </div>
  </div>
</div>