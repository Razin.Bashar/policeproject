
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="report.ReportAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle.VehicleAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String url = "ReportServlet?actionType=search&&flstatus="+request.getParameter("flstatus")+"&&type="+request.getParameter("type")+"";
	String navigator = SessionConstants.NAV_REPORT;
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String actionName = "add";
	String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
	  int i = 0;
%>

<%
	String context = "../../.." + request.getContextPath() + "/";
%>

<div class="portlet box">
	<div class="portlet-body">

		<div class="ctw"
			style="display: flex; flex-direction: column; align-items: center;  margin: auto">


			<div
				style="font-size: 18px; padding-bottom: 20px; align-self: flex-end;">

				<!--  <a class="filter d" href="#"
         style="color: navy;font-weight: bold;    background: goldenrod;
    border-radius: 36px!important;"
         onclick="findlost(4)"
         data-toggle="collapse" data-target="#reportform"><%=LM.getText(LC.REPORT_LANDING_REPORT, loginDTO)%></a>-->
			</div>
			<style type="text/css">
				#reportform * {
					font-size: 14px;
				}
				
				@media ( min-width : 1400px) {
					.ctw {
						max-width:1080px;
					}
					.colm{
						min-width:9%;
					}
					.coln{
						min-width:91%;
					}
				}
				
				@media ( max-width : 1400px) {
					.ctw {
						max-width:870px;
					}
					.colm{
						min-width:11%;
					}
					.coln{
						min-width:82%;
					}
				}
				
				.mark {
					color: red;
					font-weight: bold;
				}
			</style>

			<div id="reportform"
				style="width: 100%; margin: 20px; padding: 20px;">
				<div class="container" style="width: 100%;">
					<div class="row">
						<form role="form"
							style="border: 1px solid #eaeaea; padding: 15px; padding-bottom: 0px;">
							<div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
								<label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_NAME, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_NAME, loginDTO))%>
								</label> <input type='text' class='form-control' name='name'
									id='name_text_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
							</div>
							<div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
								<label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_NID, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_NID, loginDTO))%>
								</label> <input type='text' class='form-control' name='nid'
									id='nid_text_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
							</div>
							<div class="clearfix"></div>
							<div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
								<label for="typeId_select_0"><%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_TYPEID, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_TYPEID, loginDTO))%></label> <select
									class='form-control' name='typeId' id='typeId_select_0'>
									<%
                     Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
                     String Options = ReportAnotherDBDAO.getOptions(Language, "select", "vehicle_type", "typeId_select_" + 0,
                     		"form-control", "typeId",request.getParameter("type"));
                     out.print(Options);
                     %>
								</select>
							</div>
							<div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
								<label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_REPORTINGDATE, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_REPORTINGDATE, loginDTO))%>
								</label> <input type='date' class='form-control'
									name='reportingDate_Date_<%=i%>'
									id='reportingDate_date_Date_<%=i%>'
									value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "'");
                        }%>>
								<input type='hidden' class='form-control' name='reportingDate'
									id='reportingDate_date_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
							</div>
							<div class="clearfix"></div>
							<div class="form-group col-xs-10 col-sm-10 col-md-6 col-lg-6">
								<label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_REGISTRATIONNUMBER, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_REGISTRATIONNUMBER, loginDTO))%>
								</label> <input type='text' class='form-control'
									name='registrationNumber' id='registrationNumber_text_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
							</div>
							<div class="col-xs-10 col-sm-10 col-md-6 col-lg-6">
								<label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_PHONENUMBER, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_PHONENUMBER, loginDTO))%>

								</label> <input type='text' class='form-control' name='phoneNumber'
									id='phoneNumber_text_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
							</div>
							<div class="clearfix"></div>

							<!--<div class="colm col-xs-1 col-sm-1 col-md-1 col-lg-1"
								style="text-align: center;">
								  <img alt=""
									style="border-radius: 1000px !important; height: 75px;"
									src="<%=request.getContextPath()%>/assets/images/moon.jpeg" />
							</div>-->
							<div class="coln col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<textarea class='form-control' name='moreDetails' id = 'moreDetails_textarea_<%=i%>'
									style="resize: vertical; min-height: 94px;border: 1px solid lightgrey;"
									placeholder="বিস্তারিত লিখুন (Write in detail) ..."></textarea>
							</div>

							<div class="clearfix"></div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
								id="imagePreviewField"></div>

							<div class="clearfix"></div>
							<div id='lostDate_div_<%=i%>' style="display: none;">
								<input type='date' class='form-control'
									name='lostDate_Date_<%=i%>' id='lostDate_date_Date_<%=i%>'
									value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "'");
                        }%>>
								<input type='hidden' class='form-control' name='lostDate'
									id='lostDate_date_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
							</div>
							<div id='foundDate_div_<%=i%>' style="display: none;">
								<input type='date' class='form-control'
									name='foundDate_Date_<%=i%>' id='foundDate_date_Date_<%=i%>'
									value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" +new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "'");
                        }%>>
								<input type='hidden' class='form-control' name='foundDate'
									id='foundDate_date_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
							</div>
							<div id='statusId_div_<%=i%>' style="display: none;">
								<select class='form-control' name='statusId'
									id='statusId_select_<%=i%>'>
									<%
                     Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
                     Options = ReportAnotherDBDAO.getOptions(Language, "select", "status", "statusId_select_" + i,
                     		"form-control", "statusId");
                     out.print(Options);
                     %>
								</select>
							</div>
							<!--   <label class="col-sm-3 control-label"> <%=(actionName.equals("edit"))
                  ? (LM.getText(LC.REPORT_EDIT_IMAGE1, loginDTO))
                  : (LM.getText(LC.REPORT_ADD_IMAGE1, loginDTO))%>
               </label>
               <div class="form-group ">
                  <div class="col-sm-6 " id='image1_div_<%=i%>'>
                     <input type='file' class='form-control' multiple="multiple" 
                        name='image1' id='image1_image_<%=i%>'
                        value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
                  </div>
               </div>-->

							<div id="buttonContainer"
								style="display: flex; justify-content: flex-start; border-top: 1px solid #eaeaea; padding: 5px 0px; position: relative;">

								<!-- <div style="border-left: 1px dotted #e4e4e4; margin-right: 5px;"></div>-->
								<div
									style="display: flex; margin: 0px; align-items: center; padding: 3px 18px; background: #EBEDF0; border-radius: 50px !important; font-size: 15px; color: #7a7ab3; font-weight: 600; margin-right: 5px;">

									<!-- 	<button type="button" style="width: 30px;height: 30px;background: transparent;border: none;color: skyblue;border: 1px solid lightgray;border-radius: 100px!important;padding: 0px;" onclick="trgimage()"><i style="font-size: 18px; margin-top: 5px;" class="fa fa-image" aria-hidden="true"></i></button> -->
									<button type="button"
										style="width: 30px;height: 30px;background: url(<%request.getContextPath(); %>assets/images/7LrkhdMlJ4d.png) no-repeat;    background-position-x: center;    background-position-y: center;border: none;color: skyblue;border: 1px solid lightgray;border-radius: 100px!important;padding: 0px;"
										onclick="trgimage()"></button>
									<label onclick="trgimage()" style="cursor: pointer;"><%=LM.getText(LC.REPORT_ADD_IMAGE1, loginDTO)%></label>
									<script>
               		function trgimage(){
               			$("input[name='image1']").click();
               		}
               		function trgsub(){
               			$("button[id='addreport']").click();
               		}
               	</script>
									<input type='file' class='form-control' multiple="multiple"
										style="display: none;" name='image1' id='image1_image_<%=i%>'
										value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
								</div>
								<!--   <div style="border-left: 1px dotted #e4e4e4; margin-right: 5px;"></div>-->
								<div id="sendComplainbtn"
									style="display: flex; align-items: center; margin: 0px; padding: 3px 18px; background: #EBEDF0; border-radius: 50px !important; font-size: 15px; color: #7a7ab3; font-weight: 600; margin-right: 5px; cursor: pointer; position: absolute; right: 0px;">
									<div
										style="border-left: 1px dotted #e4e4e4; margin-right: 5px;"></div>
									<button class="addreport" type="button"
										style="width: 30px; height: 30px; background: transparent; border: none; color: skyblue; border: 1px solid lightgray; border-radius: 100px !important; padding: 0px;">
										<i style="font-size: 18px; margin-left: -4px;"
											class="fa fa-paper-plane" aria-hidden="true"></i>
									</button>

									<label class="addreport" style="cursor: pointer;"><%=LM.getText(LC.REPORT_LANDING_REPORT, loginDTO)%></label>
								</div>
								<button id="addreport" style="display: none;" type="button"
									class="btn btn-default"><%=LM.getText(LC.VEHICLE_TYPE_EDIT_VEHICLE_TYPE_SUBMIT_BUTTON, loginDTO)%></button>
							</div>

						</form>
						<div class="clearfix"></div>
						<br /> <br />
					</div>
				</div>
			</div>
			<script type="text/javascript">
   		var files=[] ;
      $(document).on("click", ".addreport", function() {
      	submitAjax(0);
      })
      
      
       $("input[name='image1']").on('change',function(e){
            
            var file;
            var length= e.target.files.length;
            for(var i = 0;i<length;i++){
            	file  = e.target.files[i];
            	var img = $('<img style="max-width:75px;max-height:75px;" src="'+ URL.createObjectURL(file)+'"/>');
            	$('#imagePreviewField').append(img);
            }
            files = e.target.files;
            //submitAjax(0,e.target.files);
        });
      
      function submitAjax(i) {
      	console.log('submitAjax called');
      	//PreprocessBeforeSubmiting(i, false);
      	var formData = new FormData();
      	var value;
      	//value = document.getElementById('iD_text_' + i).value;
      	//console.log('submitAjax i = ' + i + ' id = ' + value);
      	//formData.append('iD', value);
      	//formData.append("identity", value);
      	//formData.append("ID", value);
      	formData.append('flstatus',<%= request.getParameter("flstatus")%>);
      	formData.append('type',<%= request.getParameter("type")%>);
      	formData.append('name',
      			document.getElementById('name_text_' + i).value);
      	formData.append('nid',
      			document.getElementById('nid_text_' + i).value);
      	//formData.append('phoneNumber', document.getElementById('phoneNumber_text_' + i).value);
      	//formData.append('address', document.getElementById('address_geolocation_' + i).value);
      	//formData.append('addressDetails', document.getElementById('addressDetails_textarea_' + i).value);
      	//formData.append('mailId', document.getElementById('mailId_text_' + i).value);
      	formData.append('typeId', document.getElementById('typeId_select_'
      			+ i).value);
      	//formData.append('modelName', document.getElementById('modelName_text_' + i).value);
      	//formData.append('color', document.getElementById('color_color_' + i).value);
      	//formData.append('engineNumber', document.getElementById('engineNumber_text_' + i).value);
      	//formData.append('engineType', document.getElementById('engineType_select_' + i).value);
      	//formData.append('chassisNumber', document.getElementById('chassisNumber_text_' + i).value);
      	//formData.append('engineCc', document.getElementById('engineCc_text_' + i).value);
      	formData.append('registrationNumber', document
      			.getElementById('registrationNumber_text_' + i).value);
      	//formData.append('manufacturer', document.getElementById('manufacturer_text_' + i).value);
      	//formData.append('manufacturingYear', document.getElementById('manufacturingYear_number_' + i).value);
      	formData.append('moreDetails', document.getElementById('moreDetails_textarea_' + i).value);
      	formData.append('reportingDate', document
      			.getElementById('reportingDate_date_' + i).value);
      	//formData.append('reporterId', document.getElementById('reporterId_text_' + i).value);
      	//formData.append('vehicleId', document.getElementById('vehicleId_text_' + i).value);
      	formData.append('lostDate', document
      			.getElementById('lostDate_date_' + i).value);
      	formData.append('foundDate', document
      			.getElementById('foundDate_date_' + i).value);
      	formData.append('statusId', document
      			.getElementById('statusId_select_' + i).value);
      	//formData.append('thanaAddress', document.getElementById('thanaAddress_geolocation_' + i).value);
      	//formData.append('blog', document.getElementById('blog_text_' + i).value);
      	//formData.append('image1', document.getElementById('image1_image_'
      	//		+ i).files);
      	
      	formData.append("imgCount",files.length);
      	 $.each(files, function(k, value)
                 {
      				formData.append('image'+k, value);
                 });
      	//formData.append('isDeleted', document.getElementById('isDeleted_checkbox_' + i).value);
      
      //	var xhttp = new XMLHttpRequest();
      //	xhttp.onreadystatechange = function() {
      //		if (this.readyState == 4 && this.status == 200) {
      //			window.location = 'ReportServlet?actionType=search&&flstatus=<%=request.getParameter("flstatus")%>&&type=<%=request.getParameter("type")%>';
      //		} else if (this.readyState == 4 && this.status != 200) {
      //			alert('failed ' + this.status);
      //		}
      //	};
      //	xhttp.open("POST",
      //			'ReportServlet?actionType=add&inplacesubmit=true&rownum='
      //					+ i, true);
      //	xhttp.send(formData);
      
      	 $.ajax({
             type:"POST",
             url :'ReportServlet?actionType=add&inplacesubmit=true&rownum='+ i,
             data : formData,
             processData: false,
             contentType: false,
             async: true,
             timeout:0,
             success : function(response) {
            	 window.location = 'ReportServlet?actionType=search&&flstatus=<%=request.getParameter("flstatus")%>&&type=<%=request.getParameter("type")%>';
			 },
			 error : function(response) {

			 }
		 });
 }
 </script>
			<form action="ReportServlet?actionType=delete" method="POST"
				id="tableForm" enctype="multipart/form-data"
				style="width: 100%; padding: 20px;">
				<jsp:include page="landingPageNewForm.jsp" flush="true">
					<jsp:param name="pageName"
						value="<%=LM.getText(LC.REPORT_SEARCH_REPORT_SEARCH_FORMNAME, loginDTO)%>" />
				</jsp:include>
			</form>
			<jsp:include page="../includes/landingnav.jsp" flush="true">
				<jsp:param name="url" value="<%=url%>" />
				<jsp:param name="navigator" value="<%=navigator%>" />
				<jsp:param name="pageName"
					value="<%=LM.getText(LC.REPORT_SEARCH_REPORT_SEARCH_FORMNAME, loginDTO)%>" />
			</jsp:include>
		</div>
	</div>
	<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js"
		type="text/javascript"></script>