
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="testrazin.TestrazinDTO"%>
<%@page import="java.util.ArrayList"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
TestrazinDTO testrazinDTO;
testrazinDTO = (TestrazinDTO)request.getAttribute("testrazinDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(testrazinDTO == null)
{
	testrazinDTO = new TestrazinDTO();
	
}
System.out.println("testrazinDTO = " + testrazinDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.TESTRAZIN_EDIT_TESTRAZIN_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.TESTRAZIN_ADD_TESTRAZIN_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;
%>



<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i><%=formTitle%>
		</div>

	</div>
	<div class="portlet-body form">
		<form class="form-horizontal" action="TestrazinServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,<%=actionName.equals("edit")?false:true%>)">
			<div class="form-body">
				
				
				








	























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='password' id = 'password_hiddenpass_<%=i%>' value=<%=actionName.equals("edit")?("'" + testrazinDTO.password + "'"):("'" + UUID.randomUUID().toString() + "'")%>/>
											
												
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.TESTRAZIN_EDIT_FIRSTNAME, loginDTO)):(LM.getText(LC.TESTRAZIN_ADD_FIRSTNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'firstName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='firstName' id = 'firstName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + testrazinDTO.firstName + "'"):("''")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.TESTRAZIN_EDIT_LASTNAME, loginDTO)):(LM.getText(LC.TESTRAZIN_ADD_LASTNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'lastName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='lastName' id = 'lastName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + testrazinDTO.lastName + "'"):("''")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.TESTRAZIN_EDIT_GENDER, loginDTO)):(LM.getText(LC.TESTRAZIN_ADD_GENDER, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'gender_div_<%=i%>'>	
		<select class='form-control'  name='gender' id = 'gender_select_<%=i%>'>
			<option class='form-control'  value='Male' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.gender).equals("Male"))?("selected"):""%>>Male<br>
			<option class='form-control'  value='Female' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.gender).equals("Female"))?("selected"):""%>>Female<br>
			<option class='form-control'  value='Other' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.gender).equals("Other"))?("selected"):""%>>Other<br>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.TESTRAZIN_EDIT_RELIGION, loginDTO)):(LM.getText(LC.TESTRAZIN_ADD_RELIGION, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'religion_div_<%=i%>'>	
		<select class='form-control'  name='religion' id = 'religion_select_<%=i%>'>
			<option class='form-control'  value='Islam' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.religion).equals("Islam"))?("selected"):""%>>Islam<br>
			<option class='form-control'  value='Hinduism' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.religion).equals("Hinduism"))?("selected"):""%>>Hinduism<br>
			<option class='form-control'  value='Buddhism' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.religion).equals("Buddhism"))?("selected"):""%>>Buddhism<br>
			<option class='form-control'  value='Christianity' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.religion).equals("Christianity"))?("selected"):""%>>Christianity<br>
			<option class='form-control'  value='Other' <%=(actionName.equals("edit") && String.valueOf(testrazinDTO.religion).equals("Other"))?("selected"):""%>>Other<br>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.TESTRAZIN_EDIT_DATEOFBIRTH, loginDTO)):(LM.getText(LC.TESTRAZIN_ADD_DATEOFBIRTH, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'dateOfBirth_div_<%=i%>'>	
		<input type='date' class='form-control'  name='dateOfBirth_Date_<%=i%>' id = 'dateOfBirth_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_dateOfBirth = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_dateOfBirth = format_dateOfBirth.format(new Date(testrazinDTO.dateOfBirth)).toString();
	out.println("'" + formatted_dateOfBirth + "'");
}
else
{
	out.println("'" + "1970-01-01" + "'");
}
%>
>
		<input type='hidden' class='form-control'  name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + testrazinDTO.dateOfBirth + "'"):("'" + "0" + "'")%>>
						
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + testrazinDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
					
	
				<div class="form-actions text-center">
					<a class="btn btn-cancel-btcl" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.TESTRAZIN_EDIT_TESTRAZIN_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.TESTRAZIN_ADD_TESTRAZIN_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-submit-btcl" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.TESTRAZIN_EDIT_TESTRAZIN_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.TESTRAZIN_ADD_TESTRAZIN_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


function PreprocessBeforeSubmiting(row, validate)
{
	console.log("found date = " + document.getElementById('dateOfBirth_date_Date_' + row).value);
	document.getElementById('dateOfBirth_date_' + row).value = new Date(document.getElementById('dateOfBirth_date_Date_' + row).value).getTime();
	if(validate)
	{
		var empty_fields = "";
		var i = 0;
		if(document.getElementById('firstName_text_' + row).value == "")
		{
			empty_fields += "'firstName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('lastName_text_' + row).value == "")
		{
			empty_fields += "'lastName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('gender_select_' + row).value == "")
		{
			empty_fields += "'gender'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(empty_fields != "")
		{
			alert(empty_fields + " cannot be empty.");
			return false;
		}
	}
	return true;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

</script>






