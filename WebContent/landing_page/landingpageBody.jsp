
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="report.ReportAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle.VehicleAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
	String url = "ReportServlet?actionType=search&land=1";
	String navigator = SessionConstants.NAV_REPORT;
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String actionName = "add";
	String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
	  int i = 0;
%>

<%
	String context = "../../.." + request.getContextPath() + "/";
%>

<div class="portlet box">
	<div class="portlet-body">
	
	<div class=""
   style="display: flex; flex-direction: column; align-items: center; max-width: 60%; margin: auto">

   <script type="text/javascript">
      function find(id) {
    	$('.vehicleType').removeClass('mark');
    	$('#type_'+id).addClass('mark');
      	allfield_changed(id, foundlost);
      	//$('#vehicletype').hide();
      }
   </script>
   <div style="font-size: 18px;padding-bottom: 20px;">
   		<a class="filter a mark" onclick="findlost(3)" ><%=LM.getText(LC.REPORT_LANDING_ALL, loginDTO)%></a>
      <a class="filter b" onclick="findlost(1)" data-toggle="collapse" data-target="#vehicletype"><%=LM.getText(LC.REPORT_LANDING_LOST, loginDTO)%></a> 
      <a class="filter c" onclick="findlost(2)" data-toggle="collapse" data-target="#vehicletype"><%=LM.getText(LC.REPORT_LANDING_FOUND, loginDTO)%></a> 
      <script type="text/javascript">
         var foundlost = 0;
         function findlost(status) {
         	if (status == 3)
         		window.location = 'ReportServlet?actionType=search&land=1';
         	else if(status ==4){
         		$('.filter').removeClass('mark');
         		$('.d').addClass('mark');
         	}
         	else {
         		$('#vehicletype').hide(function() {
         			
         			$('.filter').removeClass('mark');
             		if(status==1)$('.b').addClass('mark');
             		else if(status==2)$('.c').addClass('mark');
             		
         			foundlost = status;
         			$('#vehicletype').show('slow');
         		});
         	}
         
         }
      </script>
      <a class="filter d" href="#"
         style=""
         onclick="findlost(4)"
         data-toggle="collapse" data-target="#reportform"><%=LM.getText(LC.REPORT_LANDING_REPORT, loginDTO)%></a>
   </div>
   <style type="text/css">
      #reportform * {
      font-size: 14px;
      }
      
      .mark{
      	color:red;font-weight:bold;
      }
   </style>
      <%
      out.print("<div id = \"vehicletype\" class=\"collapse\" style=\"display:none;padding-bottom: 15px;\">");
      ArrayList<Vehicle_typeDTO> typeDTOs = (ArrayList<Vehicle_typeDTO>) new Vehicle_typeDAO()
      		.getAllVehicle_type(true);
      
      for (int iii = 0; iii < typeDTOs.size(); iii++) {
      
      	out.print("<a id=\"type_"+(int) typeDTOs.get(iii).iD+"\" class=\"filter vehicleType\"  onclick=\"find(" + (int) typeDTOs.get(iii).iD + ")\" >"
      			+ VehicleAnotherDBDAO.getName(Language, (int) typeDTOs.get(iii).iD, "vehicle_type") + "</a>");
      
      }
      out.print("</div>");
      %>
   <div id="reportform" class="collapse"
      style="width: 100%; border: 1px solid lightgray; margin: 20px; padding: 20px;">
      <div class="container" style="width: 100%;">
         <div class="row">
            <form role="form">
               <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
                  <label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_NAME, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_NAME, loginDTO))%> <span
                     class="required"> * </span>
                  </label> <input type='text' class='form-control' name='name'
                     id='name_text_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
               </div>
               <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
                  <label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_NID, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_NID, loginDTO))%>
                  </label> <input type='text' class='form-control' name='nid'
                     id='nid_text_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
               </div>
               <div class="clearfix"></div>
               <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
                  <label for="typeId_select_0"><%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_TYPEID, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_TYPEID, loginDTO))%></label> <select
                     class='form-control' name='typeId' id='typeId_select_0'>
                  <%
                     Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
                     String Options = ReportAnotherDBDAO.getOptions(Language, "select", "vehicle_type", "typeId_select_" + 0,
                     		"form-control", "typeId");
                     out.print(Options);
                     %>
                  </select>
               </div>
               <div class="form-group col-xs-10 col-sm-6 col-md-6 col-lg-6">
                  <label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_REPORTINGDATE, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_REPORTINGDATE, loginDTO))%>
                  </label> <input type='date' class='form-control'
                     name='reportingDate_Date_<%=i%>'
                     id='reportingDate_date_Date_<%=i%>'
                     value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" + "1970-01-01" + "'");
                        }%>>
                  <input type='hidden' class='form-control' name='reportingDate'
                     id='reportingDate_date_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
               </div>
               <div class="clearfix"></div>
               <div class="form-group col-xs-10 col-sm-10 col-md-6 col-lg-6">
                  <label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_REGISTRATIONNUMBER, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_REGISTRATIONNUMBER, loginDTO))%>
                  </label> <input type='text' class='form-control' name='registrationNumber'
                     id='registrationNumber_text_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
               </div>
               <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6">
                  <label> <%=(actionName.equals("edit"))
                     ? (LM.getText(LC.REPORT_EDIT_PHONENUMBER, loginDTO))
                     : (LM.getText(LC.REPORT_ADD_PHONENUMBER, loginDTO))%>
                  <span class="required"> * </span>
                  </label> <input type='text' class='form-control' name='phoneNumber'
                     id='phoneNumber_text_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
               </div>
               <div class="clearfix"></div>
               <div id='lostDate_div_<%=i%>' style="display: none;">
                  <input type='date' class='form-control'
                     name='lostDate_Date_<%=i%>' id='lostDate_date_Date_<%=i%>'
                     value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" + "1970-01-01" + "'");
                        }%>>
                  <input type='hidden' class='form-control' name='lostDate'
                     id='lostDate_date_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
               </div>
               <div id='foundDate_div_<%=i%>' style="display: none;">
                  <input type='date' class='form-control'
                     name='foundDate_Date_<%=i%>' id='foundDate_date_Date_<%=i%>'
                     value=<%if (actionName.equals("edit")) {
                       
                        } else {
                        out.println("'" + "1970-01-01" + "'");
                        }%>>
                  <input type='hidden' class='form-control' name='foundDate'
                     id='foundDate_date_<%=i%>'
                     value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + "0" + "'")%>>
               </div>
               <div id='statusId_div_<%=i%>' style="display: none;">
                  <select class='form-control' name='statusId'
                     id='statusId_select_<%=i%>'>
                  <%
                     Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
                     Options = ReportAnotherDBDAO.getOptions(Language, "select", "status", "statusId_select_" + i,
                     		"form-control", "statusId");
                     out.print(Options);
                     %>
                  </select>
               </div>
               <label class="col-sm-3 control-label"> <%=(actionName.equals("edit"))
                  ? (LM.getText(LC.REPORT_EDIT_IMAGE1, loginDTO))
                  : (LM.getText(LC.REPORT_ADD_IMAGE1, loginDTO))%>
               </label>
               <div class="form-group ">
                  <div class="col-sm-6 " id='image1_div_<%=i%>'>
                     <input type='file' class='form-control' multiple="multiple"
                        name='image1' id='image1_image_<%=i%>'
                        value=<%=actionName.equals("edit") ? ("'" + "" + "'") : ("'" + " " + "'")%> />
                  </div>
               </div>
               <div class="col-xs-10 col-sm-4 col-md-6 col-lg-6">
                  <button id="addreport" type="button" class="btn btn-default">Submit</button>
               </div>
            </form>
            <div class="clearfix"></div>
            <br /> <br />
         </div>
      </div>
   </div>
   <script type="text/javascript">
      $(document).on("click", "#addreport", function() {
      	submitAjax(0);
      })
      function submitAjax(i) {
      	console.log('submitAjax called');
      	//PreprocessBeforeSubmiting(i, false);
      	var formData = new FormData();
      	var value;
      	//value = document.getElementById('iD_text_' + i).value;
      	//console.log('submitAjax i = ' + i + ' id = ' + value);
      	//formData.append('iD', value);
      	//formData.append("identity", value);
      	//formData.append("ID", value);
      	formData.append('name',
      			document.getElementById('name_text_' + i).value);
      	formData.append('nid',
      			document.getElementById('nid_text_' + i).value);
      	//formData.append('phoneNumber', document.getElementById('phoneNumber_text_' + i).value);
      	//formData.append('address', document.getElementById('address_geolocation_' + i).value);
      	//formData.append('addressDetails', document.getElementById('addressDetails_textarea_' + i).value);
      	//formData.append('mailId', document.getElementById('mailId_text_' + i).value);
      	formData.append('typeId', document.getElementById('typeId_select_'
      			+ i).value);
      	//formData.append('modelName', document.getElementById('modelName_text_' + i).value);
      	//formData.append('color', document.getElementById('color_color_' + i).value);
      	//formData.append('engineNumber', document.getElementById('engineNumber_text_' + i).value);
      	//formData.append('engineType', document.getElementById('engineType_select_' + i).value);
      	//formData.append('chassisNumber', document.getElementById('chassisNumber_text_' + i).value);
      	//formData.append('engineCc', document.getElementById('engineCc_text_' + i).value);
      	formData.append('registrationNumber', document
      			.getElementById('registrationNumber_text_' + i).value);
      	//formData.append('manufacturer', document.getElementById('manufacturer_text_' + i).value);
      	//formData.append('manufacturingYear', document.getElementById('manufacturingYear_number_' + i).value);
      	//formData.append('moreDetails', document.getElementById('moreDetails_textarea_' + i).value);
      	formData.append('reportingDate', document
      			.getElementById('reportingDate_date_' + i).value);
      	//formData.append('reporterId', document.getElementById('reporterId_text_' + i).value);
      	//formData.append('vehicleId', document.getElementById('vehicleId_text_' + i).value);
      	formData.append('lostDate', document
      			.getElementById('lostDate_date_' + i).value);
      	formData.append('foundDate', document
      			.getElementById('foundDate_date_' + i).value);
      	formData.append('statusId', document
      			.getElementById('statusId_select_' + i).value);
      	//formData.append('thanaAddress', document.getElementById('thanaAddress_geolocation_' + i).value);
      	//formData.append('blog', document.getElementById('blog_text_' + i).value);
      	formData.append('image1', document.getElementById('image1_image_'
      			+ i).files[0]);
      	//formData.append('isDeleted', document.getElementById('isDeleted_checkbox_' + i).value);
      
      	var xhttp = new XMLHttpRequest();
      	xhttp.onreadystatechange = function() {
      		if (this.readyState == 4 && this.status == 200) {
      			window.location = 'ReportServlet?actionType=search&&land=1';
      		} else if (this.readyState == 4 && this.status != 200) {
      			alert('failed ' + this.status);
      		}
      	};
      	xhttp.open("POST",
      			'ReportServlet?actionType=add&inplacesubmit=true&rownum='
      					+ i, true);
      	xhttp.send(formData);
      }
   </script>
		<form action="ReportServlet?actionType=delete" method="POST"
			id="tableForm" enctype="multipart/form-data" style="width: 100%;">
			<jsp:include page="landingpageForm.jsp" flush="true">
				<jsp:param name="pageName"
					value="<%=LM.getText(LC.REPORT_SEARCH_REPORT_SEARCH_FORMNAME, loginDTO)%>" />
			</jsp:include>
		</form>
		<jsp:include page="../includes/landingnav.jsp" flush="true">
			<jsp:param name="url" value="<%=url%>" />
			<jsp:param name="navigator" value="<%=navigator%>" />
			<jsp:param name="pageName"
				value="<%=LM.getText(LC.REPORT_SEARCH_REPORT_SEARCH_FORMNAME, loginDTO)%>" />
		</jsp:include>
	</div>
</div>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js"
	type="text/javascript"></script>



