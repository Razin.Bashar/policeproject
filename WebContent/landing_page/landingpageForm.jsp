<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="comments.CommentsDTO"%>
<%@page import="comments.CommentsDAO"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="image.ImageServlet"%>
<%@page import="report.ReportDAO"%>
<%@page import="org.json.JSONObject"%>
<%@page import="citizen.CitizenDAO"%>
<%@page import="vehicle.VehicleDAO"%>
<%@page import="citizen.CitizenRepository"%>
<%@page import="citizen.CitizenDTO"%>
<%@page import="vehicle.VehicleDTO"%>
<%@page import="vehicle.VehicleRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="report.ReportDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="vehicle.VehicleAnotherDBDAO"%>
<%@ page import="report.ReportAnotherDBDAO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%
   LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
   GeoLocationDAO2 geoLocationDAO = new GeoLocationDAO2();
   String actionName = "add";
   String failureMessage = (String) request.getAttribute("failureMessage");
   if (failureMessage == null || failureMessage.isEmpty()) {
   	failureMessage = "";
   }
   out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
   String value = "";
   String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
   VehicleDAO vehicleDAO = new VehicleDAO();
   CitizenDAO citizenDAO = new CitizenDAO();
   ReportDAO reportDAO = new ReportDAO();
   CommentsDAO commentsDAO = new CommentsDAO();
   %>
<%
   ReportDTO reportDTO;
   VehicleDTO vehicleDTO = null;
   CitizenDTO citizenDTO = null;
   reportDTO = (ReportDTO) request.getAttribute("reportDTO");
   //LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
   if (reportDTO == null) {
   	reportDTO = new ReportDTO();
   
   } else {
   	vehicleDTO = new VehicleDAO().getVehicleDTOByID(reportDTO.vehicleId);
   	citizenDTO = new CitizenDAO().getCitizenDTOByID(reportDTO.reporterId);
   
   	if (vehicleDTO == null)
   		vehicleDTO = new VehicleDTO();
   	if (citizenDTO == null)
   		citizenDTO = new CitizenDTO();
   }
   System.out.println("reportDTO = " + reportDTO);
   
   String ID = request.getParameter("ID");
   if (ID == null || ID.isEmpty()) {
   	ID = "0";
   }
   System.out.println("ID = " + ID);
   int i = 0;
   %>

   <%
      ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_REPORT);
   Collections.sort(data, new Comparator<ReportDTO>() {
	    public int compare(ReportDTO s1, ReportDTO s2) {
	        return Long.compare(s2.iD, s1.iD);
	    }
	});
      try {
      
      	if (data != null) {
      		int size = data.size();
      		System.out.println("data not null and size = " + size + " data = " + data);
      		for (int ii = 0; ii < size; ii++) {
      			ReportDTO row = (ReportDTO) data.get(ii);
      
      			vehicleDTO = vehicleDAO.getVehicleDTOByID(row.vehicleId);
      			citizenDTO = citizenDAO.getCitizenDTOByID(row.reporterId);
      			ArrayList<JSONObject> imageList = (ArrayList<JSONObject>) reportDAO
      					.getAllImageByReportId(row.iD);
      
      			System.out.println(row.vehicleId + " -- " + row.reporterId);
      			if (vehicleDTO != null && citizenDTO != null) {
      				String deletedStyle = "color:red";
      				if (!row.isDeleted)
      					deletedStyle = "";
      %>
   <div class="postbox"
      style="display: flex; flex-direction: column; width: 100%;">
      <label style="font-size: 30px"> <%
         value = row.statusId + "";
         
         				value = ReportAnotherDBDAO.getName(Language, Integer.parseInt(value), "status");
         
         				out.println(value);
         
         				out.println("<td id = '" + i + "_typeId'>");
         				out.println(VehicleAnotherDBDAO.getName(Language, vehicleDTO.typeId, "vehicle_type"));
         
         				//out.println(row.iD);
         %>
      </label>
      <div class="imagesection"
         style="width: 100%; margin: auto; box-shadow: 0px 0px 50px lightblue;">
         <%
            value = row.image1 + "";
            				//out.println("<img src='img2/" + value +"' style=\" max-width:400px;\" >");
            				switch (1) {
            					case 0 :
            						out.println("<div style=\"display:grid;grid-template-columns:auto;height:100%;\">");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("</div>");
            
            						break;
            					case 1 :
            						out.println("<div style=\"display:grid;grid-template-columns:auto;height:100%;\">");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("</div>");
            						break;
            					case 2 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:auto auto;height:100%;\">");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("</div>");
            						break;
            					case 3 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:auto auto;height:100%;\">");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:1/3\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("</div>");
            						break;
            					case 4 :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:auto auto;height:100%;\">");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("<img src='img2/" + value + "' style=\" width:100%;\" >");
            						out.println("</div>");
            						break;
            					default :
            						out.println(
            								"<div style=\"display:grid;grid-template-columns:auto auto auto auto auto auto;height:100%;\">");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:1/ 4;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:4/ 7;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:1/ 3;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:3/ 5;\" >");
            						out.println(
            								"<img src='img2/" + value + "' style=\" width:100%;grid-column:5/ 7;\" >");
            						out.println("</div>");
            						break;
            				}
            %>
      </div>
      <style type="text/css">
      
		.detailBox {
		    width:320px;
		    border:1px solid #bbb;
		    margin:50px;
		}
		.titleBox {
		    background-color:#fdfdfd;
		    padding:10px;
		}
		.titleBox label{
		  color:#444;
		  margin:0;
		  display:inline-block;
		}
		
		.commentBox {
		    padding:10px;
		    border-top:1px dotted #bbb;
		}
		.commentBox .form-group:first-child, .actionBox .form-group:first-child {
		    width:80%;
		}
		.commentBox .form-group:nth-child(2), .actionBox .form-group:nth-child(2) {
		    width:18%;
		}
		.actionBox .form-group * {
		    width:100%;
		}
		.taskDescription {
		    margin-top:10px 0;
		}
		.commentList {
		    padding:0;
		    list-style:none;
		    overflow:auto;
		}
		.commentList li {
		    margin:0;
		    margin-top:10px;
		}
		.commentList li > div {
		    display:table-cell;
		   
		}
		.commenterImage {
		    width:30px;
		    margin-right:5px;
		    height:100%;
		    float:left;
		}
		.commenterImage img {
		    width:100%;
		    border-radius:50%;
		}
		.commentText p {
		    margin:0;
		}
		.sub-text {
		    color:#aaa;
		    font-family:verdana;
		    font-size:11px;
		}
		.actionBox {
		    border-top:1px dotted #bbb;
		    padding:10px;
		}
		.commentList p{
			font-size: 17px;
		}
      </style>
      <div class="commentsection" style="padding-left: 20px;
    padding-bottom: 30px;">
         <a href="#" style="font-size: 20px;" >Comments</a> 
         
          <div class="commentBox">
        
         <p class="taskDescription">I have lost my vehicle.plese find it </p> 
    </div>
   
    <div class="actionBox" style="zoom: 97%;
    padding-left: 20px;">
    <% 
       out.print("<ul id=\"commentList_"+row.iD+"\" class=\"commentList\">");
    SimpleDateFormat format_lostDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String[] monthNames = {"January", "February", "March", "April", "May", "June",
                      "July", "August", "September", "October", "November", "December"};
    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat outputFormat = new SimpleDateFormat("' 'KK:mm a");
    	ArrayList<CommentsDTO> commentDTOs = (ArrayList<CommentsDTO>)commentsDAO.getCommentsDTOByColumn("report_id", row.iD);
    	for(int j=0;j<commentDTOs.size();j++){
    		out.print("<li>");
    			out.print("<div class=\"commenterImage\">");
    				out.print("<img src=\"http://placekitten.com/50/50\" />");
    			out.print("</div>");
    			out.print("<div class=\"commentText\">");
    				Date date = new Date(commentDTOs.get(j).commentDate);
    				String formatted_lostDate = format_lostDate.format(commentDTOs.get(j).commentDate).toString();
    				out.print(" <p style=\"text-align:left;\">"+commentDTOs.get(j).comment+"</p> <span class=\"date sub-text\">on "+date.getDate()+" "+monthNames[date.getMonth()]+" "+(2000+date.getYear())+", "+new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)+outputFormat.format(inputFormat.parse(formatted_lostDate))+"</span>");
				out.print("</div>");
    		out.print("</li>");
    	}
    	out.print("</ul>");
    
    %>
     <!--        <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/50/50" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
            <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/45/45" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
            <li>
                <div class="commenterImage">
                  <img src="http://placekitten.com/40/40" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2014</span>

                </div>
            </li>
        </ul>-->
        <div class="form-inline" role="form">
            <div class="form-group">
            <%
                out.print("<input id=\"cmt_"+row.iD+"\" class=\"form-control\" type=\"text\" placeholder=\"Your comments\" />");
            %>
            </div>
            <div class="form-group">
            <% 
                out.print("<button type=\"button\" class=\"btn btn-default\" onclick=\"docomment("+row.iD+")\">Add</button>");
             %>
            </div>
        </div>
        </div>
    </div>
         
         
      
   </div>
   <%
      }
      		}
      
      		System.out.println("printing done");
      	} else {
      		System.out.println("data  null");
      	}
      } catch (Exception e) {
      	System.out.println("JSP exception " + e);
      }
      %>
</div>
<%
   String navigator2 = SessionConstants.NAV_REPORT;
   System.out.println("navigator2 = " + navigator2);
   RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
   System.out.println("rn2 = " + rn2);
   String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
   String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
   %>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
function convert(date) {
    if(date==null||date=='null'||date==undefined||date=='undefined'||date=='')return '';
var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];
var d = new Date(date);
var dayName = days[d.getDay()];
var Year = d.getFullYear();
var monthName = monthNames[d.getMonth()];
return d.getDate()+' '+monthName+' '+Year+', '+dayName+' '+formatAMPM(date);
}

function docomment(id){
	var data = {};
	data.reportId = id;
	data.comment = $('#cmt_'+id).val();
	data.commentDate = new Date().getTime(); 

	
	$.ajax({
        type:"POST",
        url :"ReportServlet?actionType=cmtadd",
        data : data,
        async: true,
        success : function(response) {
          // alert('success');
           
           var temp = $('<li>  ' +    
           '  <div class="commenterImage"> '+
           '          <img src="http://placekitten.com/40/40" />   '+
           '  </div> '+
           '  <div class="commentText">  '+
           '             <p class="" style="text-align:left;">'+data.comment+'</p> <span class="date sub-text">on '+convert(new Date(data.commentDate))+'</span> '+
           '  </div>'+
           '</li>');
           
           $('#commentList_'+id).append(temp);
        },
        error: function(response) {
            alert('error');
        }
    });
}


   function PreprocessBeforeSubmiting(row, validate) {
   	console
   			.log("found date = "
   					+ document.getElementById('reportingDate_date_Date_'
   							+ row).value);
   	document.getElementById('reportingDate_date_' + row).value = new Date(
   			document.getElementById('reportingDate_date_Date_' + row).value)
   			.getTime();
   	console.log("found date = "
   			+ document.getElementById('lostDate_date_Date_' + row).value);
   	document.getElementById('lostDate_date_' + row).value = new Date(
   			document.getElementById('lostDate_date_Date_' + row).value)
   			.getTime();
   	console.log("found date = "
   			+ document.getElementById('foundDate_date_Date_' + row).value);
   	document.getElementById('foundDate_date_' + row).value = new Date(
   			document.getElementById('foundDate_date_Date_' + row).value)
   			.getTime();
   	if (validate) {
   		var empty_fields = "";
   		var i = 0;
   		if (document.getElementById('name_text_' + row).value == "") {
   			empty_fields += "'name'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('phoneNumber_text_' + row).value == "") {
   			empty_fields += "'phoneNumber'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('address_geolocation_' + row).value == "") {
   			empty_fields += "'address'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('addressDetails_textarea_' + row).value == "") {
   			empty_fields += "'addressDetails'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('mailId_text_' + row).value == "") {
   			empty_fields += "'mailId'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('modelName_text_' + row).value == "") {
   			empty_fields += "'modelName'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (document.getElementById('thanaAddress_geolocation_' + row).value == "") {
   			empty_fields += "'thanaAddress'";
   			if (i > 0) {
   				empty_fields += ", ";
   			}
   			i++;
   		}
   		if (empty_fields != "") {
   			alert(empty_fields + " cannot be empty.");
   			return false;
   		}
   	}
   	return true;
   }
   function addrselected(value, htmlID, selectedIndex, tagname, geodiv,
   		hiddenfield) {
   	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
   	try {
   		document.getElementById(hiddenfield).value = value;
   		var elements = document.getElementById(geodiv).children;
   		var ids = '';
   		for (var i = elements.length - 1; i >= 0; i--) {
   			var elemID = elements[i].id;
   			if (elemID.includes(htmlID) && elemID > htmlID) {
   				ids += elements[i].id + ' ';
   
   				for (var j = elements[i].options.length - 1; j >= 0; j--) {
   
   					elements[i].options[j].remove();
   				}
   				elements[i].remove();
   
   			}
   		}
   
   		var newid = htmlID + '_1';
   
   		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='"
   				+ tagname
   				+ "' id = '"
   				+ newid
   				+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '"
   				+ geodiv + "', '" + hiddenfield + "')\"></select>";
   		console.log('innerHTML= '
   				+ document.getElementById(geodiv).innerHTML);
   		document.getElementById(htmlID).options[0].innerHTML = document
   				.getElementById(htmlID).options[selectedIndex].innerHTML;
   		document.getElementById(htmlID).options[0].value = document
   				.getElementById(htmlID).options[selectedIndex].value;
   		console.log('innerHTML again = '
   				+ document.getElementById(geodiv).innerHTML);
   
   		var xhttp = new XMLHttpRequest();
   		xhttp.onreadystatechange = function() {
   			if (this.readyState == 4 && this.status == 200) {
   				if (!this.responseText.includes('option')) {
   					document.getElementById(newid).remove();
   				} else {
   					document.getElementById(newid).innerHTML = this.responseText;
   				}
   
   			} else if (this.readyState == 4 && this.status != 200) {
   				alert('failed ' + this.status);
   			}
   		};
   
   		var redirect = "geolocation/geoloc.jsp?myID=" + value;
   
   		xhttp.open("GET", redirect, true);
   		xhttp.send();
   	} catch (err) {
   		alert("got error: " + err);
   	}
   	return;
   }
   
   function addHTML(id, HTML) {
   	document.getElementById(id).innerHTML += HTML;
   }
   
   function getRequests() {
   	var s1 = location.search.substring(1, location.search.length)
   			.split('&'), r = {}, s2, i;
   	for (i = 0; i < s1.length; i += 1) {
   		s2 = s1[i].split('=');
   		r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
   	}
   	return r;
   }
   
   function Request(name) {
   	return getRequests()[name.toLowerCase()];
   }
   
   function ShowExcelParsingResult(suffix) {
   	var failureMessage = document
   			.getElementById("failureMessage_" + suffix);
   	if (failureMessage == null) {
   		console.log("failureMessage_" + suffix + " not found");
   	}
   	console.log("value = " + failureMessage.value);
   	if (failureMessage != null && failureMessage.value != "") {
   		alert("Excel uploading result:" + failureMessage.value);
   	}
   }
   
   function init(row) {
   
   	var xhttp = new XMLHttpRequest();
   	xhttp.onreadystatechange = function() {
   		if (this.readyState == 4 && this.status == 200) {
   			document.getElementById('address_geoSelectField_' + row).innerHTML = this.responseText;
   			document.getElementById('thanaAddress_geoSelectField_' + row).innerHTML = this.responseText;
   		} else if (this.readyState == 4 && this.status != 200) {
   			alert('failed ' + this.status);
   		}
   	};
   	xhttp.open("GET", "geolocation/geoloc.jsp?myID=1", true);
   	xhttp.send();
   }
   function doEdit(params, i, id, deletedStyle) {
   	var xhttp = new XMLHttpRequest();
   	xhttp.onreadystatechange = function() {
   		if (this.readyState == 4 && this.status == 200) {
   			if (this.responseText != '') {
   				var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle
   						+ "')";
   				document.getElementById('tr_' + i).innerHTML = this.responseText;
   				document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
   				document.getElementById(i + '_Submit').innerHTML += "<a onclick=\""+ onclickFunc +"\">Submit</a>";
   				document.getElementById('tr_' + i).innerHTML += "<td>"
   						+ "<input type='checkbox' name='ID' value='" + id + "'/>"
   						+ "</td>";
   				init(i);
   			} else {
   				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
   			}
   		} else if (this.readyState == 4 && this.status != 200) {
   			alert('failed ' + this.status);
   		}
   	};
   
   	xhttp
   			.open("Get", "ReportServlet?actionType=getEditPage" + params,
   					true);
   	xhttp.send();
   }
   
   function fixedToEditable(i, deletedStyle, id) {
   	console.log('fixedToEditable called');
   	var params = '&identity=' + id + '&inplaceedit=true' + '&deletedStyle='
   			+ deletedStyle + '&ID=' + id + '&rownum=' + i + '&dummy=dummy';
   	console.log('fixedToEditable i = ' + i + ' id = ' + id);
   	doEdit(params, i, id, deletedStyle);
   
   }
   window.onload = function() {
   	ShowExcelParsingResult('general');
   }
</script>