<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="super_hero_table.Super_hero_tableDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
Super_hero_tableDTO super_hero_tableDTO = (Super_hero_tableDTO)request.getAttribute("super_hero_tableDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(super_hero_tableDTO == null)
{
	super_hero_tableDTO = new Super_hero_tableDTO();
	
}
System.out.println("super_hero_tableDTO = " + super_hero_tableDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");
%>








	

























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="super_hero_table.Super_hero_tableAnotherDBDAO"%>
<%
String Language = LM.getText(LC.SUPER_HERO_TABLE_EDIT_LANGUAGE, loginDTO);
String Options;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_password" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='password' id = 'password_hiddenpass_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.password + "'"):("'" + UUID.randomUUID().toString() + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_firstName'>")%>
			
	
	<div class="form-inline" id = 'firstName_div_<%=i%>'>
		<input type='text' class='form-control'  name='firstName' id = 'firstName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.firstName + "'"):("''")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastName'>")%>
			
	
	<div class="form-inline" id = 'lastName_div_<%=i%>'>
		<input type='text' class='form-control'  name='lastName' id = 'lastName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.lastName + "'"):("''")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gender'>")%>
			
	
	<div class="form-inline" id = 'gender_div_<%=i%>'>
		<select class='form-control'  name='gender' id = 'gender_select_<%=i%>'>
			<option class='form-control'  value='Male' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Male"))?("selected"):""%>>Male<br>
			<option class='form-control'  value='Female' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Female"))?("selected"):""%>>Female<br>
			<option class='form-control'  value='Other' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Other"))?("selected"):""%>>Other<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_religion'>")%>
			
	
	<div class="form-inline" id = 'religion_div_<%=i%>'>
		<select class='form-control'  name='religion' id = 'religion_select_<%=i%>'>
<%
if(actionName.equals("edit"))
{
			Options = Super_hero_tableAnotherDBDAO.getOptions(Language, "select", "religion", "religion_select_" + i, "form-control", "religion", super_hero_tableDTO.religion);
}
else
{			
			Options = Super_hero_tableAnotherDBDAO.getOptions(Language, "select", "religion", "religion_select_" + i, "form-control", "religion" );			
}
out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_dateOfBirth'>")%>
			
	
	<div class="form-inline" id = 'dateOfBirth_div_<%=i%>'>
		<input type='date' class='form-control'  name='dateOfBirth_Date_<%=i%>' id = 'dateOfBirth_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_dateOfBirth = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_dateOfBirth = format_dateOfBirth.format(new Date(super_hero_tableDTO.dateOfBirth)).toString();
	out.println("'" + formatted_dateOfBirth + "'");
}
else
{
	out.println("'" + "1970-01-01" + "'");
}
%>
>
		<input type='hidden' class='form-control'  name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.dateOfBirth + "'"):("'" + "0" + "'")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_address'>")%>
			
	
	<div class="form-inline" id = 'address_div_<%=i%>'>
		<div id ='address_geoDIV_<%=i%>'>
			<select class='form-control' name='address_active' id = 'address_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'address_geoDIV_<%=i%>', 'address_geolocation_<%=i%>')"></select>
		</div>
		<input type='text' class='form-control' name='address_text' id = 'address_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(super_hero_tableDTO.address)  + "'"):("''")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='address' id = 'address_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(super_hero_tableDTO.address)  + "'"):("''")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_addressDetails'>")%>
			
	
	<div class="form-inline" id = 'addressDetails_div_<%=i%>'>
		<textarea class='form-control'  name='addressDetails' id = 'addressDetails_textarea_<%=i%>'><%=actionName.equals("edit")?(super_hero_tableDTO.addressDetails):("")%></textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_image'>")%>
			
	
	<div class="form-inline" id = 'image_div_<%=i%>'>
		<input type='file' class='form-control'  name='image' id = 'image_image_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.image + "'"):("'" + " " + "'")%>/>	
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_bioDataFile'>")%>
			
	
	<div class="form-inline" id = 'bioDataFile_div_<%=i%>'>
		<input type='file' class='form-control'  name='bioDataFile' id = 'bioDataFile_file_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.bioDataFile + "'"):("'" + " " + "'")%>/>	
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_otherDatabase'>")%>
			
	
	<div class="form-inline" id = 'otherDatabase_div_<%=i%>'>
		<input type='file' class='form-control'  name='otherDatabase' id = 'otherDatabase_database_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.otherDatabase + "'"):("'" + " " + "'")%>/>	
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_education'>")%>
			
	
	<div class="form-inline" id = 'education_div_<%=i%>'>
		<select class='form-control'  name='education' id = 'education_select_<%=i%>'>
			<option class='form-control'  value='SSC' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("SSC"))?("selected"):""%>>SSC<br>
			<option class='form-control'  value='HSC' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("HSC"))?("selected"):""%>>HSC<br>
			<option class='form-control'  value='Undergraduate' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Undergraduate"))?("selected"):""%>>Undergraduate<br>
			<option class='form-control'  value='Graduate' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Graduate"))?("selected"):""%>>Graduate<br>
			<option class='form-control'  value='Diploma' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Diploma"))?("selected"):""%>>Diploma<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_passingYear'>")%>
			
	
	<div class="form-inline" id = 'passingYear_div_<%=i%>'>
		<input type='number' class='form-control'  name='passingYear' id = 'passingYear_number_<%=i%>' min='[1900, 2018].get(1)' max='[1900, 2018].get(2)' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.passingYear + "'"):("'" + "${col.getDefaultValue()}.get(1)" + "'")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isStrong'>")%>
			
	
	<div class="form-inline" id = 'isStrong_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isStrong' id = 'isStrong_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.isStrong).equals("true"))?("checked"):""%>><br>						


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_eyeColor'>")%>
			
	
	<div class="form-inline" id = 'eyeColor_div_<%=i%>'>
		<input type='color' class='form-control'  name='eyeColor' id = 'eyeColor_color_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.eyeColor + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_hasARadio'>")%>
			
	
	<div class="form-inline" id = 'hasARadio_div_<%=i%>'>
		<input type='hidden' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.hasARadio + "'"):("'" + "" + "'")%>>
		<input type='radio' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>_true' value=true <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.hasARadio).equals("true"))?("checked"):""%>>true<br>		
		<input type='radio' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>_false' value=false <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.hasARadio).equals("false"))?("checked"):""%>>false<br>		

	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_preciseWeight'>")%>
			
	
	<div class="form-inline" id = 'preciseWeight_div_<%=i%>'>
		<input type='text' class='form-control'  name='preciseWeight' id = 'preciseWeight_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.preciseWeight + "'"):("'" + "0.0" + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_mailRichtext'>")%>
			
	
	<div class="form-inline" id = 'mailRichtext_div_<%=i%>'>
		<textarea class='form-control'  name='mailRichtext' id = 'mailRichtext_richtext_<%=i%>'><%=actionName.equals("edit")?(super_hero_tableDTO.mailRichtext):("")%></textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + super_hero_tableDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
					
	