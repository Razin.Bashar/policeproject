
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="super_hero_table.Super_hero_tableDTO"%>
<%@page import="java.util.ArrayList"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
Super_hero_tableDTO super_hero_tableDTO;
super_hero_tableDTO = (Super_hero_tableDTO)request.getAttribute("super_hero_tableDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(super_hero_tableDTO == null)
{
	super_hero_tableDTO = new Super_hero_tableDTO();
	
}
System.out.println("super_hero_tableDTO = " + super_hero_tableDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.SUPER_HERO_TABLE_EDIT_SUPER_HERO_TABLE_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.SUPER_HERO_TABLE_ADD_SUPER_HERO_TABLE_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;
%>



<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i><%=formTitle%>
		</div>

	</div>
	<div class="portlet-body form">
		<form class="form-horizontal" action="Super_hero_tableServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,<%=actionName.equals("edit")?false:true%>)">
			<div class="form-body">
				
				
				








	

























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="super_hero_table.Super_hero_tableAnotherDBDAO"%>
<%
String Language = LM.getText(LC.SUPER_HERO_TABLE_EDIT_LANGUAGE, loginDTO);
String Options;
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='password' id = 'password_hiddenpass_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.password + "'"):("'" + UUID.randomUUID().toString() + "'")%>/>
											
												
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_FIRSTNAME, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_FIRSTNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'firstName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='firstName' id = 'firstName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.firstName + "'"):("''")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_LASTNAME, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_LASTNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'lastName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='lastName' id = 'lastName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.lastName + "'"):("''")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_GENDER, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_GENDER, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'gender_div_<%=i%>'>	
		<select class='form-control'  name='gender' id = 'gender_select_<%=i%>'>
			<option class='form-control'  value='Male' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Male"))?("selected"):""%>>Male<br>
			<option class='form-control'  value='Female' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Female"))?("selected"):""%>>Female<br>
			<option class='form-control'  value='Other' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.gender).equals("Other"))?("selected"):""%>>Other<br>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_RELIGION, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_RELIGION, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'religion_div_<%=i%>'>	
		<select class='form-control'  name='religion' id = 'religion_select_<%=i%>'>
<%
if(actionName.equals("edit"))
{
			Options = Super_hero_tableAnotherDBDAO.getOptions(Language, "select", "religion", "religion_select_" + i, "form-control", "religion", super_hero_tableDTO.religion);
}
else
{			
			Options = Super_hero_tableAnotherDBDAO.getOptions(Language, "select", "religion", "religion_select_" + i, "form-control", "religion" );			
}
out.print(Options);
%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_DATEOFBIRTH, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_DATEOFBIRTH, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'dateOfBirth_div_<%=i%>'>	
		<input type='date' class='form-control'  name='dateOfBirth_Date_<%=i%>' id = 'dateOfBirth_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_dateOfBirth = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_dateOfBirth = format_dateOfBirth.format(new Date(super_hero_tableDTO.dateOfBirth)).toString();
	out.println("'" + formatted_dateOfBirth + "'");
}
else
{
	out.println("'" + "1970-01-01" + "'");
}
%>
>
		<input type='hidden' class='form-control'  name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.dateOfBirth + "'"):("'" + "0" + "'")%>>
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_ADDRESS, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_ADDRESS, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'address_div_<%=i%>'>	
		<div id ='address_geoDIV_<%=i%>'>
			<select class='form-control' name='address_active' id = 'address_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'address_geoDIV_<%=i%>', 'address_geolocation_<%=i%>')"></select>
		</div>
		<input type='text' class='form-control' name='address_text' id = 'address_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(super_hero_tableDTO.address)  + "'"):("''")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='address' id = 'address_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(super_hero_tableDTO.address)  + "'"):("''")%>>
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_ADDRESSDETAILS, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_ADDRESSDETAILS, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'addressDetails_div_<%=i%>'>	
		<textarea class='form-control'  name='addressDetails' id = 'addressDetails_textarea_<%=i%>'><%=actionName.equals("edit")?(super_hero_tableDTO.addressDetails):("")%></textarea>		
											
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_IMAGE, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_IMAGE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'image_div_<%=i%>'>	
		<input type='file' class='form-control'  name='image' id = 'image_image_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.image + "'"):("'" + " " + "'")%>/>	
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_BIODATAFILE, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_BIODATAFILE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'bioDataFile_div_<%=i%>'>	
		<input type='file' class='form-control'  name='bioDataFile' id = 'bioDataFile_file_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.bioDataFile + "'"):("'" + " " + "'")%>/>	
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_OTHERDATABASE, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_OTHERDATABASE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'otherDatabase_div_<%=i%>'>	
		<input type='file' class='form-control'  name='otherDatabase' id = 'otherDatabase_database_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.otherDatabase + "'"):("'" + " " + "'")%>/>	
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_EDUCATION, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_EDUCATION, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'education_div_<%=i%>'>	
		<select class='form-control'  name='education' id = 'education_select_<%=i%>'>
			<option class='form-control'  value='SSC' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("SSC"))?("selected"):""%>>SSC<br>
			<option class='form-control'  value='HSC' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("HSC"))?("selected"):""%>>HSC<br>
			<option class='form-control'  value='Undergraduate' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Undergraduate"))?("selected"):""%>>Undergraduate<br>
			<option class='form-control'  value='Graduate' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Graduate"))?("selected"):""%>>Graduate<br>
			<option class='form-control'  value='Diploma' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.education).equals("Diploma"))?("selected"):""%>>Diploma<br>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_PASSINGYEAR, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_PASSINGYEAR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'passingYear_div_<%=i%>'>	
		<input type='number' class='form-control'  name='passingYear' id = 'passingYear_number_<%=i%>' min='[1900, 2018].get(1)' max='[1900, 2018].get(2)' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.passingYear + "'"):("'" + "${col.getDefaultValue()}.get(1)" + "'")%>>
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_ISSTRONG, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_ISSTRONG, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'isStrong_div_<%=i%>'>	
		<input type='checkbox' class='form-control'  name='isStrong' id = 'isStrong_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.isStrong).equals("true"))?("checked"):""%>><br>						


	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_EYECOLOR, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_EYECOLOR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'eyeColor_div_<%=i%>'>	
		<input type='color' class='form-control'  name='eyeColor' id = 'eyeColor_color_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.eyeColor + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_HASARADIO, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_HASARADIO, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'hasARadio_div_<%=i%>'>	
		<input type='hidden' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.hasARadio + "'"):("'" + "" + "'")%>>
		<input type='radio' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>_true' value=true <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.hasARadio).equals("true"))?("checked"):""%>>true<br>		
		<input type='radio' class='form-control'  name='hasARadio' id = 'hasARadio_radio_<%=i%>_false' value=false <%=(actionName.equals("edit") && String.valueOf(super_hero_tableDTO.hasARadio).equals("false"))?("checked"):""%>>false<br>		

	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_PRECISEWEIGHT, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_PRECISEWEIGHT, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'preciseWeight_div_<%=i%>'>	
		<input type='text' class='form-control'  name='preciseWeight' id = 'preciseWeight_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + super_hero_tableDTO.preciseWeight + "'"):("'" + "0.0" + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SUPER_HERO_TABLE_EDIT_MAILRICHTEXT, loginDTO)):(LM.getText(LC.SUPER_HERO_TABLE_ADD_MAILRICHTEXT, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'mailRichtext_div_<%=i%>'>	
		<textarea class='form-control'  name='mailRichtext' id = 'mailRichtext_richtext_<%=i%>'><%=actionName.equals("edit")?(super_hero_tableDTO.mailRichtext):("")%></textarea>		
											
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + super_hero_tableDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
					
	
				<div class="form-actions text-center">
					<a class="btn btn-cancel-btcl" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.SUPER_HERO_TABLE_EDIT_SUPER_HERO_TABLE_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.SUPER_HERO_TABLE_ADD_SUPER_HERO_TABLE_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-submit-btcl" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.SUPER_HERO_TABLE_EDIT_SUPER_HERO_TABLE_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.SUPER_HERO_TABLE_ADD_SUPER_HERO_TABLE_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


function PreprocessBeforeSubmiting(row, validate)
{
	console.log("found date = " + document.getElementById('dateOfBirth_date_Date_' + row).value);
	document.getElementById('dateOfBirth_date_' + row).value = new Date(document.getElementById('dateOfBirth_date_Date_' + row).value).getTime();
	document.getElementById('address_geolocation_' + row).value = document.getElementById('address_geolocation_' + row).value + ":" + document.getElementById('address_geoTextField_' + row).value;
	if(document.getElementById('isStrong_checkbox_' + row).checked)
	{
		document.getElementById('isStrong_checkbox_' + row).value = "true";
	}
	else
	{
		document.getElementById('isStrong_checkbox_' + row).value = "false";
	}
	if(document.getElementById('hasARadio_radio_' + row + '_true').checked)
	{
		document.getElementById('hasARadio_radio_' + row).value = document.getElementById('hasARadio_radio_' + row + '_true').value;
	}
	if(document.getElementById('hasARadio_radio_' + row + '_false').checked)
	{
		document.getElementById('hasARadio_radio_' + row).value = document.getElementById('hasARadio_radio_' + row + '_false').value;
	}
	if(validate)
	{
		var empty_fields = "";
		var i = 0;
		if(document.getElementById('firstName_text_' + row).value == "")
		{
			empty_fields += "'firstName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('lastName_text_' + row).value == "")
		{
			empty_fields += "'lastName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('gender_select_' + row).value == "")
		{
			empty_fields += "'gender'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('address_geolocation_' + row).value == "")
		{
			empty_fields += "'address'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('addressDetails_textarea_' + row).value == "")
		{
			empty_fields += "'addressDetails'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('mailRichtext_richtext_' + row).value == "")
		{
			empty_fields += "'mailRichtext'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(empty_fields != "")
		{
			alert(empty_fields + " cannot be empty.");
			return false;
		}
	}
	return true;
}
function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		document.getElementById(hiddenfield).value = value;
		var elements = document.getElementById(geodiv).children;
		var ids = '';
		for(var i = elements.length - 1; i >= 0; i--) {
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}


		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		 var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() 
		  {
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		 
		  
		  xhttp.open("POST", "Super_hero_tableServlet?actionType=getGeo&myID="+value, true);
		  xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  
	return;
}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
		
	 var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) 
	    {
	    	document.getElementById('address_geoSelectField_' + row).innerHTML = this.responseText ;
	    }
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	  xhttp.open("POST", "Super_hero_tableServlet?actionType=getGeo&myID=1", true);
	  xhttp.send();
}var row = 0;
bkLib.onDomLoaded(function() 
{	
	new nicEditor({iconsPath : 'nicEditorIcons.gif'}).panelInstance('mailRichtext_richtext_' + row);
});
	
window.onload =function ()
{
	init(row);
}

</script>






