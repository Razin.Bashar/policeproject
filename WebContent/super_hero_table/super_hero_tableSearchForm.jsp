
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="super_hero_table.Super_hero_tableDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="super_hero_table.Super_hero_tableAnotherDBDAO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.SUPER_HERO_TABLE_EDIT_LANGUAGE, loginDTO);
%>				
				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_FIRSTNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_LASTNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_GENDER, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_RELIGION, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_DATEOFBIRTH, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_ADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_ADDRESSDETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_IMAGE, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_BIODATAFILE, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_OTHERDATABASE, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_EDUCATION, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_PASSINGYEAR, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_ISSTRONG, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_EYECOLOR, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_HASARADIO, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_PRECISEWEIGHT, loginDTO)%></th>
								<th><%=LM.getText(LC.SUPER_HERO_TABLE_EDIT_MAILRICHTEXT, loginDTO)%></th>
								<th><%out.print(LM.getText(LC.SUPER_HERO_TABLE_SEARCH_SUPER_HERO_TABLE_EDIT_BUTTON, loginDTO));%></th>
								<th><input type="submit" class="btn btn-xs btn-danger" value="
								<%out.print(LM.getText(LC.SUPER_HERO_TABLE_SEARCH_SUPER_HERO_TABLE_DELETE_BUTTON, loginDTO));%>
								" /></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_SUPER_HERO_TABLE);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Super_hero_tableDTO row = (Super_hero_tableDTO) data.get(i);
											String deletedStyle="color:red";
											if(!row.isDeleted)deletedStyle = "";
											out.println("<tr id = 'tr_" + i + "'>");
											

											
		
											
		
											
											out.println("<td id = '" + i + "_firstName'>");
											value = row.firstName + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_lastName'>");
											value = row.lastName + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_gender'>");
											value = row.gender + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_religion'>");
											value = row.religion + "";
											
											value = Super_hero_tableAnotherDBDAO.parseName (Language, value);
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_dateOfBirth'>");
											value = row.dateOfBirth + "";
											SimpleDateFormat format_dateOfBirth = new SimpleDateFormat("yyyy-MM-dd");
											String formatted_dateOfBirth = format_dateOfBirth.format(new Date(Long.parseLong(value))).toString();
											out.println(formatted_dateOfBirth);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_address'>");
											value = row.address + "";
											out.println(GeoLocationDAO2.parseText(value));
											{
												String addressdetails = GeoLocationDAO2.parseDetails(value);
												if(!addressdetails.equals(""))
												{
													out.println(", " + addressdetails);
												}
											}
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_addressDetails'>");
											value = row.addressDetails + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_image'>");
											value = row.image + "";
											out.println("<img src='img2/" + value +"' style='width:100px' >");
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_bioDataFile'>");
											value = row.bioDataFile + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_otherDatabase'>");
											value = row.otherDatabase + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_education'>");
											value = row.education + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_passingYear'>");
											value = row.passingYear + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_isStrong'>");
											value = row.isStrong + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_eyeColor'>");
											value = row.eyeColor + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_hasARadio'>");
											value = row.hasARadio + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_preciseWeight'>");
											value = row.preciseWeight + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_mailRichtext'>");
											value = row.mailRichtext + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
		
	



											
											String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.iD + "' )\"";										
	
											out.println("<td id = '" + i + "_Edit'>");										
											out.println("<a onclick="+ onclickFunc +">Edit</a>");
										
											out.println("</td>");
											
											
											
											out.println("<td>");
											out.println("<input type='checkbox' name='ID' value='" + row.iD + "'/>");
											out.println("</td>");
											out.println("</tr>");
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<%
	String navigator2 = SessionConstants.NAV_SUPER_HERO_TABLE;
	System.out.println("navigator2 = " + navigator2);
	RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
	System.out.println("rn2 = " + rn2);
	String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
	String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();

%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{
	console.log("found date = " + document.getElementById('dateOfBirth_date_Date_' + row).value);
	document.getElementById('dateOfBirth_date_' + row).value = new Date(document.getElementById('dateOfBirth_date_Date_' + row).value).getTime();
	document.getElementById('address_geolocation_' + row).value = document.getElementById('address_geolocation_' + row).value + ":" + document.getElementById('address_geoTextField_' + row).value;
	if(document.getElementById('isStrong_checkbox_' + row).checked)
	{
		document.getElementById('isStrong_checkbox_' + row).value = "true";
	}
	else
	{
		document.getElementById('isStrong_checkbox_' + row).value = "false";
	}
	if(document.getElementById('hasARadio_radio_' + row + '_true').checked)
	{
		document.getElementById('hasARadio_radio_' + row).value = document.getElementById('hasARadio_radio_' + row + '_true').value;
	}
	if(document.getElementById('hasARadio_radio_' + row + '_false').checked)
	{
		document.getElementById('hasARadio_radio_' + row).value = document.getElementById('hasARadio_radio_' + row + '_false').value;
	}
	if(validate)
	{
		var empty_fields = "";
		var i = 0;
		if(document.getElementById('firstName_text_' + row).value == "")
		{
			empty_fields += "'firstName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('lastName_text_' + row).value == "")
		{
			empty_fields += "'lastName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('gender_select_' + row).value == "")
		{
			empty_fields += "'gender'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('address_geolocation_' + row).value == "")
		{
			empty_fields += "'address'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('addressDetails_textarea_' + row).value == "")
		{
			empty_fields += "'addressDetails'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(document.getElementById('mailRichtext_richtext_' + row).value == "")
		{
			empty_fields += "'mailRichtext'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(empty_fields != "")
		{
			alert(empty_fields + " cannot be empty.");
			return false;
		}
	}
	return true;
}
function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		document.getElementById(hiddenfield).value = value;
		var elements = document.getElementById(geodiv).children;
		var ids = '';
		for(var i = elements.length - 1; i >= 0; i--) {
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}


		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		 var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() 
		  {
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		 
		  
		  xhttp.open("POST", "Super_hero_tableServlet?actionType=getGeo&myID="+value, true);
		  xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  
	return;
}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
		
	 var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) 
	    {
	    	document.getElementById('address_geoSelectField_' + row).innerHTML = this.responseText ;
	    }
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	  xhttp.open("POST", "Super_hero_tableServlet?actionType=getGeo&myID=1", true);
	  xhttp.send();
}
function doEdit(params, i, id, deletedStyle)
{
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{
				var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle + "')";
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
				document.getElementById(i + '_Submit').innerHTML += "<a onclick=\""+ onclickFunc +"\">Submit</a>";				
				document.getElementById('tr_' + i).innerHTML += "<td>"
				+ "<input type='checkbox' name='ID' value='" + id + "'/>"
				+ "</td>";
				init(i);
			}
			else
			{
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	  
	  xhttp.open("Get", "Super_hero_tableServlet?actionType=getEditPage" + params, true);
	  xhttp.send();	
}

function submitAjax(i, deletedStyle)
{
	console.log('submitAjax called');
	PreprocessBeforeSubmiting(i, false);
	var formData = new FormData();
	var value;
	value = document.getElementById('iD_text_' + i).value;
	console.log('submitAjax i = ' + i + ' id = ' + value);
	formData.append('iD', value);
	formData.append("identity", value);
	formData.append("ID", value);
	formData.append('password', document.getElementById('password_hiddenpass_' + i).value);
	formData.append('firstName', document.getElementById('firstName_text_' + i).value);
	formData.append('lastName', document.getElementById('lastName_text_' + i).value);
	formData.append('gender', document.getElementById('gender_select_' + i).value);
	formData.append('religion', document.getElementById('religion_select_' + i).value);
	formData.append('dateOfBirth', document.getElementById('dateOfBirth_date_' + i).value);
	formData.append('address', document.getElementById('address_geolocation_' + i).value);
	formData.append('addressDetails', document.getElementById('addressDetails_textarea_' + i).value);
	formData.append('image', document.getElementById('image_image_' + i).files[0]);
	formData.append('bioDataFile', document.getElementById('bioDataFile_file_' + i).files[0]);
	formData.append('otherDatabase', document.getElementById('otherDatabase_database_' + i).files[0]);
	formData.append('education', document.getElementById('education_select_' + i).value);
	formData.append('passingYear', document.getElementById('passingYear_number_' + i).value);
	formData.append('isStrong', document.getElementById('isStrong_checkbox_' + i).value);
	formData.append('eyeColor', document.getElementById('eyeColor_color_' + i).value);
	formData.append('hasARadio', document.getElementById('hasARadio_radio_' + i).value);
	formData.append('preciseWeight', document.getElementById('preciseWeight_text_' + i).value);
	formData.append('mailRichtext', document.getElementById('mailRichtext_richtext_' + i).value);
	formData.append('isDeleted', document.getElementById('isDeleted_checkbox_' + i).value);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'Super_hero_tableServlet?actionType=edit&inplacesubmit=true&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
	xhttp.send(formData);
}

function fixedToEditable(i, deletedStyle, id)
{
	console.log('fixedToEditable called');
	var params = '&identity=' + id + '&inplaceedit=true' +  '&deletedStyle=' + deletedStyle + '&ID=' + id + '&rownum=' + i
	+ '&dummy=dummy';
	console.log('fixedToEditable i = ' + i + ' id = ' + id);
	doEdit(params, i, id, deletedStyle);

}
window.onload =function ()
{
	ShowExcelParsingResult('general');
}	
</script>
			