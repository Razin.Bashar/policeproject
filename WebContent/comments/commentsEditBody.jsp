
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="comments.CommentsDTO"%>
<%@page import="java.util.ArrayList"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
CommentsDTO commentsDTO;
commentsDTO = (CommentsDTO)request.getAttribute("commentsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(commentsDTO == null)
{
	commentsDTO = new CommentsDTO();
	
}
System.out.println("commentsDTO = " + commentsDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.COMMENTS_EDIT_COMMENTS_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.COMMENTS_ADD_COMMENTS_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;
%>



<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i><%=formTitle%>
		</div>

	</div>
	<div class="portlet-body form">
		<form class="form-horizontal" action="CommentsServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,<%=actionName.equals("edit")?false:true%>)">
			<div class="form-body">
				
				
				








	

























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='reportId' id = 'reportId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + commentsDTO.reportId + "'"):("'" + "report_id" + "'")%>/>
												
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.COMMENTS_EDIT_COMMENT, loginDTO)):(LM.getText(LC.COMMENTS_ADD_COMMENT, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'comment_div_<%=i%>'>	
		<input type='text' class='form-control'  name='comment' id = 'comment_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + commentsDTO.comment + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.COMMENTS_EDIT_COMMENTDATE, loginDTO)):(LM.getText(LC.COMMENTS_ADD_COMMENTDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'commentDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='commentDate_Date_<%=i%>' id = 'commentDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_commentDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_commentDate = format_commentDate.format(new Date(commentsDTO.commentDate)).toString();
	out.println("'" + formatted_commentDate + "'");
}
else
{
	out.println("'" + "1970-01-01" + "'");
}
%>
>
		<input type='hidden' class='form-control'  name='commentDate' id = 'commentDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + commentsDTO.commentDate + "'"):("'" + "0" + "'")%>>
						
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + commentsDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
					
	
				<div class="form-actions text-center">
					<a class="btn btn-cancel-btcl" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.COMMENTS_EDIT_COMMENTS_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.COMMENTS_ADD_COMMENTS_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-submit-btcl" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.COMMENTS_EDIT_COMMENTS_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.COMMENTS_ADD_COMMENTS_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


function PreprocessBeforeSubmiting(row, validate)
{
	console.log("found date = " + document.getElementById('commentDate_date_Date_' + row).value);
	document.getElementById('commentDate_date_' + row).value = new Date(document.getElementById('commentDate_date_Date_' + row).value).getTime();
	if(validate)
	{
		var empty_fields = "";
		var i = 0;
		if(empty_fields != "")
		{
			alert(empty_fields + " cannot be empty.");
			return false;
		}
	}
	return true;
}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

</script>






