<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="vehicle.VehicleDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
VehicleDTO vehicleDTO = (VehicleDTO)request.getAttribute("vehicleDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(vehicleDTO == null)
{
	vehicleDTO = new VehicleDTO();
	
}
System.out.println("vehicleDTO = " + vehicleDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");
%>








	

























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="vehicle.VehicleAnotherDBDAO"%>
<%
String Language = LM.getText(LC.VEHICLE_EDIT_LANGUAGE, loginDTO);
String Options;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_typeId'>")%>
			
	
	<div id = 'typeId_div_<%=i%>'>
		<select class='form-control'  name='typeId' id = 'typeId_select_<%=i%>'>
<%			
			Options = VehicleAnotherDBDAO.getOptions(Language, "select", "vehicle_type", "typeId_select_" + i, "form-control", "typeId" );
			out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modelName'>")%>
			
	
	<div id = 'modelName_div_<%=i%>'>
		<input type='text' class='form-control'  name='modelName' id = 'modelName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.modelName + "'"):("''")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_color'>")%>
			
	
	<div id = 'color_div_<%=i%>'>
		<input type='color' class='form-control'  name='color' id = 'color_color_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.color + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_engineNumber'>")%>
			
	
	<div id = 'engineNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='engineNumber' id = 'engineNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.engineNumber + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_engineType'>")%>
			
	
	<div id = 'engineType_div_<%=i%>'>
		<select class='form-control'  name='engineType' id = 'engineType_select_<%=i%>'>
<%			
			Options = VehicleAnotherDBDAO.getOptions(Language, "select", "engine_type", "engineType_select_" + i, "form-control", "engineType" );
			out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_chassisNumber'>")%>
			
	
	<div id = 'chassisNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='chassisNumber' id = 'chassisNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.chassisNumber + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_engineCc'>")%>
			
	
	<div id = 'engineCc_div_<%=i%>'>
		<input type='text' class='form-control'  name='engineCc' id = 'engineCc_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.engineCc + "'"):("'" + "0" + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_registrationNumber'>")%>
			
	
	<div id = 'registrationNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='registrationNumber' id = 'registrationNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.registrationNumber + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_manufacturer'>")%>
			
	
	<div id = 'manufacturer_div_<%=i%>'>
		<input type='text' class='form-control'  name='manufacturer' id = 'manufacturer_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.manufacturer + "'"):("'" + " " + "'")%>/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_manufacturingYear'>")%>
			
	
	<div id = 'manufacturingYear_div_<%=i%>'>
		<input type='number' class='form-control'  name='manufacturingYear' id = 'manufacturingYear_number_<%=i%>' min='[1900, 2018].get(1)' max='[1900, 2018].get(2)' value=<%=actionName.equals("edit")?("'" + vehicleDTO.manufacturingYear + "'"):("'" + "${col.getDefaultValue()}.get(1)" + "'")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_moreDetails'>")%>
			
	
	<div id = 'moreDetails_div_<%=i%>'>
		<textarea class='form-control'  name='moreDetails' id = 'moreDetails_textarea_<%=i%>'><%=actionName.equals("edit")?(vehicleDTO.moreDetails):(" ")%></textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + vehicleDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
					
	