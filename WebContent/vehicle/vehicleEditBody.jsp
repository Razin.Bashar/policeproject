
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vehicle.VehicleDTO"%>
<%@page import="java.util.ArrayList"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
VehicleDTO vehicleDTO;
vehicleDTO = (VehicleDTO)request.getAttribute("vehicleDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(vehicleDTO == null)
{
	vehicleDTO = new VehicleDTO();
	
}
System.out.println("vehicleDTO = " + vehicleDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.VEHICLE_EDIT_VEHICLE_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.VEHICLE_ADD_VEHICLE_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;
%>



<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i><%=formTitle%>
		</div>

	</div>
	<div class="portlet-body form">
		<form class="form-horizontal" action="VehicleServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,<%=actionName.equals("edit")?false:true%>)">
			<div class="form-body">
				
				
				








	

























<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="vehicle.VehicleAnotherDBDAO"%>
<%
String Language = LM.getText(LC.VEHICLE_EDIT_LANGUAGE, loginDTO);
String Options;
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_text_<%=i%>' value='<%=ID%>'/>
	
												
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_TYPEID, loginDTO)):(LM.getText(LC.VEHICLE_ADD_TYPEID, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'typeId_div_<%=i%>'>	
		<select class='form-control'  name='typeId' id = 'typeId_select_<%=i%>'>
<%			
			Options = VehicleAnotherDBDAO.getOptions(Language, "select", "vehicle_type", "typeId_select_" + i, "form-control", "typeId" );
			out.print(Options);
%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_MODELNAME, loginDTO)):(LM.getText(LC.VEHICLE_ADD_MODELNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'modelName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='modelName' id = 'modelName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.modelName + "'"):("''")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_COLOR, loginDTO)):(LM.getText(LC.VEHICLE_ADD_COLOR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'color_div_<%=i%>'>	
		<input type='color' class='form-control'  name='color' id = 'color_color_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.color + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_ENGINENUMBER, loginDTO)):(LM.getText(LC.VEHICLE_ADD_ENGINENUMBER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'engineNumber_div_<%=i%>'>	
		<input type='text' class='form-control'  name='engineNumber' id = 'engineNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.engineNumber + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_ENGINETYPE, loginDTO)):(LM.getText(LC.VEHICLE_ADD_ENGINETYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'engineType_div_<%=i%>'>	
		<select class='form-control'  name='engineType' id = 'engineType_select_<%=i%>'>
<%			
			Options = VehicleAnotherDBDAO.getOptions(Language, "select", "engine_type", "engineType_select_" + i, "form-control", "engineType" );
			out.print(Options);
%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_CHASSISNUMBER, loginDTO)):(LM.getText(LC.VEHICLE_ADD_CHASSISNUMBER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'chassisNumber_div_<%=i%>'>	
		<input type='text' class='form-control'  name='chassisNumber' id = 'chassisNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.chassisNumber + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_ENGINECC, loginDTO)):(LM.getText(LC.VEHICLE_ADD_ENGINECC, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'engineCc_div_<%=i%>'>	
		<input type='text' class='form-control'  name='engineCc' id = 'engineCc_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.engineCc + "'"):("'" + "0" + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_REGISTRATIONNUMBER, loginDTO)):(LM.getText(LC.VEHICLE_ADD_REGISTRATIONNUMBER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'registrationNumber_div_<%=i%>'>	
		<input type='text' class='form-control'  name='registrationNumber' id = 'registrationNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.registrationNumber + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_MANUFACTURER, loginDTO)):(LM.getText(LC.VEHICLE_ADD_MANUFACTURER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'manufacturer_div_<%=i%>'>	
		<input type='text' class='form-control'  name='manufacturer' id = 'manufacturer_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + vehicleDTO.manufacturer + "'"):("'" + " " + "'")%>/>					
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_MANUFACTURINGYEAR, loginDTO)):(LM.getText(LC.VEHICLE_ADD_MANUFACTURINGYEAR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'manufacturingYear_div_<%=i%>'>	
		<input type='number' class='form-control'  name='manufacturingYear' id = 'manufacturingYear_number_<%=i%>' min='[1900, 2018].get(1)' max='[1900, 2018].get(2)' value=<%=actionName.equals("edit")?("'" + vehicleDTO.manufacturingYear + "'"):("'" + "${col.getDefaultValue()}.get(1)" + "'")%>>
						
	</div>
</div>			
				
	
<label class="col-sm-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VEHICLE_EDIT_MOREDETAILS, loginDTO)):(LM.getText(LC.VEHICLE_ADD_MOREDETAILS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-sm-6 " id = 'moreDetails_div_<%=i%>'>	
		<textarea class='form-control'  name='moreDetails' id = 'moreDetails_textarea_<%=i%>'><%=actionName.equals("edit")?(vehicleDTO.moreDetails):(" ")%></textarea>		
											
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_checkbox_<%=i%>' value= <%=actionName.equals("edit")?("'" + vehicleDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
					
	
				<div class="form-actions text-center">
					<a class="btn btn-cancel-btcl" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.VEHICLE_EDIT_VEHICLE_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.VEHICLE_ADD_VEHICLE_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-submit-btcl" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.VEHICLE_EDIT_VEHICLE_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.VEHICLE_ADD_VEHICLE_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


function PreprocessBeforeSubmiting(row, validate)
{
	if(validate)
	{
		var empty_fields = "";
		var i = 0;
		if(document.getElementById('modelName_text_' + row).value == "")
		{
			empty_fields += "'modelName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(empty_fields != "")
		{
			alert(empty_fields + " cannot be empty.");
			return false;
		}
	}
	return true;
}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

</script>






