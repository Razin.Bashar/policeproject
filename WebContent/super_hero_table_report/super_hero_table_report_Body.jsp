<%@page import="util.ActionTypeConstant"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<html:base />



<form class="form-horizontal" id="ReportForm"
			action="<%=request.getContextPath()%>/Super_hero_table_report_Servlet?actionType=<%=ActionTypeConstant.REPORT_RESULT%>">
<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cubes"></i><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_REPORT_GENERATION, loginDTO)%>
		</div>
	</div>
	<div class="portlet-body form">
		
			<input type="hidden" id="countURL" name="countURL" value="/Super_hero_table_report_Servlet?actionType=<%=ActionTypeConstant.REPORT_COUNT%>">
			<div class="form-body">
				<div class="row">
<!--  					<div class="col-md-6">
						<div class="col-md-8">
							<select class="form-control load-template"></select>
						</div>
						<div class="col-md-4">
							<button id="load-template-button" type="button"
								class="btn blue-hoki" disabled>
								<i class="fa fa-upload"></i> Load
							</button>
						</div>
					</div> -->
					<div class="col-md-6">
						<!-- <div class="col-md-8">
							<input class="form-control save-template" type="text"
								placeholder="Save this Template">
						</div> -->
						<div class="col-md-4">
							<button id="save-template-button" type="button" class="btn blue">
								<i class="fa fa-save"></i> <%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_SAVE, loginDTO)%>
							</button>
						</div>
					</div>
 				</div>
				<div class="row">
					<hr>
				</div>
				<div  class="row">
						<div class="col-md-3" id="criteria"><%@include file="super_hero_table_report_CriteriaDiv.jsp"%></div>
						<div class="col-md-6" id="display"><%@include file="super_hero_table_report_DisplayDiv.jsp"%></div>
						<div class="col-md-3" id="orderby"><%@include file="super_hero_table_report_OrderByDiv.jsp"%></div>
				</div>
				<div id="searchCriteria"></div>


				<div class="custom-form-action">
					<div>
						<div class="text-center">
							<button type="reset" class="btn blue-hoki"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_RESET, loginDTO)%></button>
							<button id="defaultLoad" type="submit" class="btn blue"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_SUBMIT, loginDTO)%></button>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

<div class="portlet box portlet-btcl light navigator">
	<div class="portlet-body">	
		<div class="row text-center">
				<nav aria-label="Page navigation">
					<ul class="pagination" style="margin: 0px;">
						<li style="float:left;"><i class="hidden-xs"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_RECORD_PER_PAGE, loginDTO)%></i>&nbsp;&nbsp;
							<input type="text" class="custom-form-control" name="RECORDS_PER_PAGE" style="height: 34px;" placeholder="" value="100" />&nbsp;&nbsp;&nbsp;&nbsp;
						</li>
						<li class="page-item"><a class="page-link" href="" id="firstLoad"
							aria-label="First" title="Left"> <i
								class="fa fa-angle-double-left" aria-hidden="true"></i> <span
								class="sr-only"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_FIRST, loginDTO)%></span>
						</a></li>
						<li class="page-item"><a class="page-link" id="previousLoad"
							href="" aria-label="Previous" title="Previous"> <i
								class="fa fa-angle-left" aria-hidden="true"></i> <span
								class="sr-only"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_PREVIOUS, loginDTO)%></span>
						</a></li>

						<li class="page-item"><a class="page-link" href="" id="nextLoad"
							aria-label="Next" title="Next"> <i class="fa fa-angle-right"
								aria-hidden="true"></i> <span class="sr-only">Next</span>
						</a></li>
						<li class="page-item"><a class="page-link" href="" id="lastLoad"
							aria-label="Last" title="Last"> <i
								class="fa fa-angle-double-right" aria-hidden="true"></i> <span
								class="sr-only"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_LAST, loginDTO)%></span>
						</a></li>
						<li>&nbsp;&nbsp;<i class="hidden-xs"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_PAGE, loginDTO)%> </i><input
							type="text" class="custom-form-control " name="pageno" value='' style="height: 34px;"
							size="15"> <i class="hidden-xs"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_OF, loginDTO)%></i>&nbsp;&nbsp;<input
							type="text" class="custom-form-control " name="tatalPageNo" value='' style="height: 34px;"
							size="15" disabled> <input type="hidden" name="totalRecord"><input type="submit" id="forceLoad"
							class="btn btn-circle  btn-sm green-haze btn-outline sbold uppercase"
							value="<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_GO, loginDTO)%>" />
						</li>
					</ul>
				</nav>
		</div>
	</div>
</div>
</form>
<div class="portlet box portlet-btcl light">
	<div class="portlet-body">
		<!-- Dynamically loaded report table -->
		<div class="row" id="report-div">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="icon-settings font-dark"></i> <span
								class="caption-subject bold uppercase"><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_REPORT, loginDTO)%></span>
						</div>
						<div class="tools"></div>
					</div>
					<div class="portlet-body">
						<div class="table">
							<table class="table table-striped " id="reportTable"
								style="width: 100%">

								<thead>
									<tr>
										<th></th>
									</tr>
								</thead>

								<tbody>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Dynamically loaded report table -->
	</div>
</div>

