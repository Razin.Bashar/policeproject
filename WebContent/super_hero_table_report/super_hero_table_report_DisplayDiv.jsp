<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-tv"></i><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_DISPLAY, loginDTO)%>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="portlet-body form" style="height: 30vh; overflow-x: hidden; overflow-y:  scroll;">
		<div class="form-body">
			<div class="form-group">
				<div class="col-md-5">
					<label class="checkbox"><span><input type="checkbox" class="input-checkbox-display" value="1"></span><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_FIRSTNAME, loginDTO)%></label>
				</div>
				<div class="col-md-5">
					<input  type="text" class="display-input" name="display.Super_hero_table_report_Descriptor.firstName" value="<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_FIRSTNAME, loginDTO)%>" disabled>
				</div>
				<div class="col-md-2" style="position: relative;"><%@include file="super_hero_table_report_upDownArrow.jsp"%></div>
			</div>
			<div class="form-group">
				<div class="col-md-5">
					<label class="checkbox"><span><input type="checkbox" class="input-checkbox-display" value="2"></span><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_LASTNAME, loginDTO)%></label>
				</div>
				<div class="col-md-5">
					<input  type="text" class="display-input" name="display.Super_hero_table_report_Descriptor.lastName" value="<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_LASTNAME, loginDTO)%>" disabled>
				</div>
				<div class="col-md-2" style="position: relative;"><%@include file="super_hero_table_report_upDownArrow.jsp"%></div>
			</div>
			<div class="form-group">
				<div class="col-md-5">
					<label class="checkbox"><span><input type="checkbox" class="input-checkbox-display" value="3"></span><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_ADDRESS, loginDTO)%></label>
				</div>
				<div class="col-md-5">
					<input  type="text" class="display-input" name="display.Super_hero_table_report_Descriptor.address" value="<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_ADDRESS, loginDTO)%>" disabled>
				</div>
				<div class="col-md-2" style="position: relative;"><%@include file="super_hero_table_report_upDownArrow.jsp"%></div>
			</div>
			<div class="form-group">
				<div class="col-md-5">
					<label class="checkbox"><span><input type="checkbox" class="input-checkbox-display" value="4"></span><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_STRENGTHNAME, loginDTO)%></label>
				</div>
				<div class="col-md-5">
					<input  type="text" class="display-input" name="display.Super_hero_table_report_Descriptor.strengthName" value="<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_SELECT_STRENGTHNAME, loginDTO)%>" disabled>
				</div>
				<div class="col-md-2" style="position: relative;"><%@include file="super_hero_table_report_upDownArrow.jsp"%></div>
			</div>
			
		</div>
	</div>
	<!-- /.box-body -->
</div>
