
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-tv"></i><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_CRITERIA, loginDTO)%>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="portlet-body form" style="height: 30vh; overflow-x: hidden; overflow-y:  scroll;">
		<div class="form-body">
		

			<div class="form-group">
                    <div class="col-md-9">
                    	<label class="checkbox"><span><input type="checkbox" delimeter = "" class="input-checkbox-criteria" data-operator="like" data-comment="" value="Super_hero_table_report_Descriptor.firstName" name="Super_hero_table_report_Descriptor.firstName" ></span>
                    		<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_WHERE_FIRSTNAME, loginDTO)%>
                    	</label>
                    </div>
             </div>

			<div class="form-group">
                    <div class="col-md-9">
                    	<label class="checkbox"><span><input type="checkbox" delimeter = "OR" class="input-checkbox-criteria" data-operator="like" data-comment="" value="Super_hero_table_report_Descriptor.gender" name="Super_hero_table_report_Descriptor.gender" ></span>
                    		<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_WHERE_GENDER, loginDTO)%>
                    	</label>
                    </div>
             </div>

			<div class="form-group">
                    <div class="col-md-9">
                    	<label class="checkbox"><span><input type="checkbox" delimeter = "AND" class="input-checkbox-criteria" data-operator="like" data-comment="" value="Super_hero_table_report_Descriptor.religion" name="Super_hero_table_report_Descriptor.religion" ></span>
                    		<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_WHERE_RELIGION, loginDTO)%>
                    	</label>
                    </div>
             </div>

		</div>
	</div>
	<!-- /.box-body -->
</div>