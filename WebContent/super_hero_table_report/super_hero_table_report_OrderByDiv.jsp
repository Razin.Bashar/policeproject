
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-tv"></i><%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_OTHER_ORDER_BY, loginDTO)%>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="portlet-body form" style="height: 30vh; overflow-x: hidden; overflow-y:  scroll;">
		<div class="form-body">
			<div class="form-group">
                    <div class="col-md-9">
                    	<label class="checkbox"><span><input type="checkbox" class="input-checkbox-orderby" value="Super_hero_table_report_Descriptor.firstName" name="orderByColumns" data-sequenceno="1" checked></span>
                    		<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_ORDER_BY_FIRSTNAME, loginDTO)%>
                    	</label>
                    </div>
             </div>
			<div class="form-group">
                    <div class="col-md-9">
                    	<label class="checkbox"><span><input type="checkbox" class="input-checkbox-orderby" value="Super_hero_table_report_Descriptor.lastName" name="orderByColumns" data-sequenceno="2" checked></span>
                    		<%=LM.getText(LC.SUPER_HERO_TABLE_REPORT_ORDER_BY_LASTNAME, loginDTO)%>
                    	</label>
                    </div>
             </div>

		</div>
	</div>
	<!-- /.box-body -->
</div>