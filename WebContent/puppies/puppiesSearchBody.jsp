Searching.....

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="puppies.PuppiesDTO"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "PuppiesServlet?actionType=search";
String navigator = SessionConstants.NAV_PUPPIES;
%>
	<jsp:include page="../includes/nav.jsp" flush="true">
		<jsp:param name="url" value="<%=url%>" />
		<jsp:param name="navigator" value="<%=navigator%>" />
	</jsp:include>
<%String context = "../../.."  + request.getContextPath() + "/";%>

<div class="portlet box">
	<div class="portlet-body">
		<form action="PuppiesServlet?actionType=delete" method="POST" id="tableForm">
			<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>PuppyId</th>
								<th>Name</th>
								<th>Age</th>
								<th>Address</th>
								<th>Gender</th>
								<th>Color</th>
								<th>IsWellBehaved</th>
								<th>IsDeleted</th>
								
								<th><input type="submit" class="btn btn-xs btn-danger" value="Delete" /></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PUPPIES);

									if (data != null) {
										int size = data.size();
										for (int i = 0; i < size; i++) 
										{
											PuppiesDTO row = (PuppiesDTO) data.get(i);
											String deletedStyle="color:red";
											if(!row.isDeleted)deletedStyle = "";
											out.println("<tr>");
											out.println("<td style='"+ deletedStyle +"'>" + row.puppyId + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.name + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.age + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.address + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.gender + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.color + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.isWellBehaved + "</td>");
											out.println("<td style='"+ deletedStyle +"'>" + row.isDeleted + "</td>");
											out.println("<td>");
											out.println("<input type='checkbox' name='ID' value='" + row.puppyId + "'/>");
											out.println("</td>");
											out.println("</tr>");
										}
							%>



						</tbody>
						<%
							}
						%>

					</table>
				</div>			
		</form>
	</div>
</div>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tableForm').submit(function(e) {
	    var currentForm = this;
	    var selected=false;
	    e.preventDefault();
	    var set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
	    $(set).each(function() {
	    	if($(this).prop('checked')){
	    		selected=true;
	    	}
	    });
	    if(!selected){
	    	 bootbox.alert("Select user to delete!", function() { });
	    }else{
	    	 bootbox.confirm("Are you sure you want to delete the user (s)?", function(result) {
	             if (result) {
	                 currentForm.submit();
	             }
	         });
	    }
	});
})

</script>


