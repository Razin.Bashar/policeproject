
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="puppies.PuppiesDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="puppies.PuppiesDTO"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>


<%
PuppiesDTO puppiesDTO;
puppiesDTO = (PuppiesDTO)request.getAttribute("puppiesDTO");
if(puppiesDTO == null)
{
	puppiesDTO = new PuppiesDTO();
	
}
System.out.println("puppiesDTO = " + puppiesDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = "PUPPIES";
String fieldError = "";
String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
%>


<style>
	.ul-box{
		width:65%;
		margin-left: 17%;
		margin-bottom: 1%;
		background-color: #eee;
		padding-top: 40px;
		padding-bottom: 30px;
	}
</style>

<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i><%=formTitle%>
		</div>

	</div>
	<div class="portlet-body form">
		<form class="form-horizontal" action="PuppiesServlet?actionType=<%=actionName%>" id="bigform" name="bigform"  method="POST" onsubmit="return DoSubmit()">
			<div class="form-body">
				
				
				
				<div class="form-group ">
						<div class="col-sm-6 ">
						<input type='hidden' class='form-control'  name='puppyId' value='<%=ID%>'/>
					</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					NAME
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='name' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.name);
						}
						else
						{
							out.print("Option1, Option2, Option3");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					AGE
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='age' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.age);
						}
						else
						{
							out.print("0");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					ADDRESS
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='address' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.address);
						}
						else
						{
							out.print("Option1, Option2, Option3");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					GENDER
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='gender' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.gender);
						}
						else
						{
							out.print("Option1, Option2, Option3");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					COLOR
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='color' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.color);
						}
						else
						{
							out.print("Option1, Option2, Option3");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<label class="col-sm-3 control-label">					
					ISWELLBEHAVED
							<span class="required"> * </span>
							</label>

							<div class="col-sm-6 ">
										<input type='text' class='form-control'  name='isWellBehaved' value='<%
						if(actionName.equals("edit"))
						{
							out.print(puppiesDTO.isWellBehaved);
						}
						else
						{
							out.print("false");
						}
						%>'/>
							</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-group ">
						<div class="col-sm-6 ">
						<input type='hidden' class='form-control'  name='isDeleted' value='<%=puppiesDTO.isDeleted%>'/>
					</div>
						<label id='availability'  style="padding-top: 7px;margin-top: 1px; margin-bottom: 0px; text-align: left; font-weight: 400"></label>
					
				</div>			
				<div class="form-actions text-center">
					<a class="btn btn-cancel-btcl" href="<%=request.getHeader("referer")%>">Cancel</a>
					<button class="btn btn-submit-btcl" type="submit">Submit</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

var geo_lastClickedValue = [];

function DoSubmit()
{
									return true;
}

function addrselected(value, htmlID, name, selectedIndex, tagname)
{
	try 
	{
		geo_lastClickedValue[tagname] = value;
		var elements = document.getElementsByClassName("form-control");
		var ids = '';
		for(var i = elements.length - 1; i >= 0; i--) {
			var elemID = elements[i].id;
			if(elemID.includes(tagname + "_geoSelectField") && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}


		var newid = htmlID + '_1';

		document.getElementById(tagname + "_geodiv").innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange='addrselected(this.value, this.id, this.name, this.selectedIndex, this.name)'></select>";
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		
		 var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() 
		  {
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		 
		  var redirect = "puppies/geoloc.jsp?myID="+value;
		  
		  xhttp.open("GET", redirect, true);
		  xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  
	return;
}


window.onload =function ()
{
	 var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) 
	    {
									    }
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	  xhttp.open("GET", "puppies/geoloc.jsp?myID=1", true);
	  xhttp.send();
	
}
bkLib.onDomLoaded(function() 
{
								});
</script>






