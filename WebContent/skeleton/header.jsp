<%@page import="role.PermissionRepository"%>
<%@page import="permission.MenuConstants"%>
<%@page import="util.JSPConstant"%>
<%@page import="config.GlobalConfigConstants"%>
<%@page import="config.GlobalConfigurationRepository"%>
<%@page import="user.UserDTO"%>
<%@page import="java.util.*"%>
<%@page import="user.UserRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>


<%
	LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
	UserDTO localUserDTO = UserRepository.getUserDTOByUserID(localLoginDTO.userID);
	System.out.println("localLoginDTO " + localLoginDTO);

%>
<div class="page-header-inner ">

	<div class="page-logo">
       	<!-- <a href= "<%=context%>Welcome.do"><img src="<%=context%>images/small-logo.png" alt="logo" class="logo-default" height=42 style="margin: 12px 10px 0 !important"/> </a> -->
       	<a id="photo" href="<%=request.getContextPath()%>/ReportServlet?actionType=getLandingPage"><img
						src="<%=request.getContextPath()%>/assets/r/pcc/500/files/static/v38/en.png"
						alt="logo" class="logo-default" style="height: 70px;"> </a>
       	<div class="menu-toggler sidebar-toggler"></div>
   	</div>
   	<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
	
	
	<div class="page-top">
		<div class="top-menu">
		
		<ul class="nav navbar-nav pull-right">
		
		<%if( PermissionRepository.checkPermissionByRoleIDAndMenuID(localUserDTO.roleID, MenuConstants.LANGUAGE_TEXT_EDIT)){ %>
		<li>
		<%
		
			List<Integer> menuIDPathList = (List<Integer>)request.getAttribute("menuIDPath");
			int currentMenuID = ((menuIDPathList!=null && menuIDPathList.size()>0)?menuIDPathList.get(menuIDPathList.size()-1):0);
			
			
		
		%>
			<a href="<%=context%>LanguageServlet?actionType=search&menuID=<%=currentMenuID%>&backLinkEnabled=<%=currentMenuID%>&<%=SessionConstants.SEARCH_CHECK_FIELD%>=true&&<%=SessionConstants.HTML_SEARCH_CHECK_FIELD%>=true&<%=SessionConstants.RECORDS_PER_PAGE%>=10000"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_CHANGE_LABEL,localLoginDTO) %></a>
		</li>
		<%} %>


			
			<%if(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value!=localLoginDTO.userID){ %>
			
				<li>
					<a href="<%=context%>languageChangeServlet"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_CHANGE_LANGUAGE, localLoginDTO) %></a>
				<li>
					<a href=""> <i class="fa fa-circle"></i><%=localUserDTO.userName %></a>
				</li>
				<li class="separator hide"></li>
				<li>
					<a href="<%=context%>LogoutServlet"> <i class="fa fa-arrow-right"></i><%=LM.getText(LC.GLOBAL_LOGOUT, localLoginDTO) %></a>
				</li>
			<%}else{ %>
				<li>
					<a href="<%=context%>languageChangeServlet"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_CHANGE_LANGUAGE, localLoginDTO) %></a>
				<li>
				<li>
					<a href="<%=context%>LoginServlet"> <i class="fa fa-arrow-right"></i><%=LM.getText(LC.GLOBAL_LOG_IN, localLoginDTO) %></a>
				</li>
			
			<%} %>	
			</ul>
		</div>
	</div>
</div>