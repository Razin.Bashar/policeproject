<!DOCTYPE html>
<%@page import="vehicle.VehicleAnotherDBDAO"%>
<%@page import="vehicle_type.Vehicle_typeDAO"%>
<%@page import="vehicle_type.Vehicle_typeDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String context = "../../.."  + request.getContextPath() + "/";
	String pluginsContext = context +"assets/global/plugins/";   
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext",pluginsContext);   

	List<Integer> menuIDPath = (List<Integer>)request.getAttribute("menuIDPath");
	
	
	LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getInstance().getUserDTOByUserID(loginDTO.userID);
	
	String pageTitle="";
	
	String Language = LM.getText(LC.REPORT_EDIT_LANGUAGE, loginDTO);
	
%>

<html lang="en" style="zoom:80%;height: 100%">


<head>

<link rel="icon" href="<%=request.getContextPath()%>/images/favicon-icon.png">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" type="image/x-icon" href="${context}favicon.ico" />
<title><%=pageTitle %></title>
<style type="text/css">
.filter {
	padding: 0px 10px;
}

.page-content {
	margin-left: 0px !important;
}

#photo:hover{
	background-color: transparent;
}
.abs{
	position:absolute;
	bottom: 0px;
	width: 100%;
}

@media (max-width:500px) {
	.page-container{
		padding:0px!important;
	}
	.page-content-wrapper{
		width:100%!important;
	}
	
	#sendComplainbtn{
	margin-top: 5px!important;
	position: static!important;
	}

	#moreDetails_textarea_0{
		margin-top: 5px!important;
	}
	#buttonContainer{
	    flex-direction: column;
	}
}
</style>
<%@ include file="../skeleton/head.jsp"%>
<%
String[] cssStr = request.getParameterValues("css");
for(int i = 0; ArrayUtils.isNotEmpty(cssStr) &&i < cssStr.length;i++)
{
	%>
<link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css" />
	<%
}
%>

<script type="text/javascript">
 var context = '${context}';
 var pluginsContext = '${pluginsContext}';
</script>
</head>
<!-- END HEAD -->
<%
String  fullMenu="'";


if(menuIDPath != null)
{

	for(int i = 0;i<menuIDPath.size();i++){
		
		int menuID = menuIDPath.get(i);
		if(i!=0){
			fullMenu+="','";
		}
		fullMenu+=menuID;
	}
}

	fullMenu+="'";

%>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo"  style="background: #e8fff3;min-height:100%;
    background-image: url(<%=request.getContextPath()%>/assets/r/pcc/500/files/static/v38/body_bg.png_%3b%20filename_%3dutf-8%27%27body_bg.png);background-repeat: repeat;display:flex;flex-direction:column;" onload="activateMenu(<%=fullMenu%>)">
	<!-- BEGIN HEADER -->
	<div id="fakeLoader"></div>
	
	<div class="page-header navbar navbar-fixed-top" style="    background: linear-gradient(-145deg, rgba(255,255,255,0.0)10%, rgba(255,255,255,0.2)20%, rgba(19,27,67,1)70%), linear-gradient(145deg, rgba(150,150,255,0.0)80%, rgba(60,111,162,0.8)20%), linear-gradient(125deg, rgba(255,255,255,0.0)70%, rgba(60,111,190,0.7)30%), linear-gradient(-200deg, rgba(60,111,180,0.9)60%, rgba(60,111,190,0.8)40%);">
		<!-- BEGIN HEADER INNER -->
		<%@page import="user.UserDTO"%>
		<%@page import="user.UserRepository"%>
		<%@page import="language.LC"%>
		<%@page import="language.LM"%>
		<%@page import="login.LoginDTO"%>
		<%@page import="sessionmanager.SessionConstants"%>
		<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>


		<%
			LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
			UserDTO localUserDTO = UserRepository.getUserDTOByUserID(localLoginDTO.userID);
			System.out.println("localLoginDTO " + localLoginDTO);
		%>
		<div class="page-header-inner ">
			<div>
				<div class="page-logo">
				
					<a id="photo" href="<%=request.getContextPath()%>/ReportServlet?actionType=getLandingPage"><img
						src="<%=request.getContextPath()%>/assets/r/pcc/500/files/static/v38/en.png"
						alt="logo" class="logo-default" style="height: 70px;"> </a>
					<div class="menu-toggler sidebar-toggler"></div>

				</div>
				<a href="javascript:;" class="menu-toggler responsive-toggler"
					data-toggle="collapse" data-target=".navbar-collapse"> </a>

				<div class="page-top">
					<div class="top-menu">
<!-- 
						<ul class="nav navbar-nav pull-right">
							<li><a style='    padding: 0px;
    padding-right: 15px;
    padding-top: 5px;' href="<%=request.getContextPath()%>/languageChangeServlet">
									<i class="fa fa-circle"></i><%=LM.getText(LC.CHANGE_LANGUAGE, loginDTO)%>
							</a></li>
							<li>
							<li><input type="text" placeholder="username"></li>
							<li><input type="password" placeholder="password"></li>
							<li>
								<button>login</button>
							</li>
						</ul>
						 -->
						 
						<ul class="nav navbar-nav pull-right">
							<li >
								<a href="" style="color:#ffa700" > <i class="fa fa-circle"></i><%=userDTO.userName %></a>
							</li>
							<li ><%
								if(userDTO.ID==12001){
							%>
								<a href="<%=context%>LoginServlet" style="color:#ffa700"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_LOG_IN, localLoginDTO) %></a>
							<%
								}else{%>
									<a href="<%=context%>LogoutServlet" style="color:#ffa700"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_LOGOUT, localLoginDTO) %></a>
							<%	}
							%>
							</li >
							<%
								if(userDTO.ID!=12001){
									out.print("<li>");
									%>
									<a href="<%=context%>" style="color:#ffa700"> <i class="fa fa-circle"></i><%=LM.getText(LC.GLOBAL_DASHBOARD, localLoginDTO) %></a>
									
									<%
									out.print("</li>");
								}
							%>
							<li class="separator hide"></li>
							
						</ul>
					</div>
				</div>
			</div>

		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container" style="display: flex; flex-direction: column; align-items: center;flex-grow:1;justify-content: center;">
		<!-- BEGIN SIDEBAR -->
		
		
		

		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper" style="width: 80%;">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content" style="padding:0px;">
			
				<%-- <jsp:include page='flushActionStatus.jsp' /> --%>
				<jsp:include page='<%=request.getParameter("body")%>' />
				<!-- END PAGE BASE CONTENT -->
			</div>
			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->
	</div>

	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<%@ include file="../skeleton/footer.jsp"%>
	<!-- END FOOTER -->
	<script>
		function footerset(){
			var height1 = $(window).height();
			var height2 = $('.page-content-wrapper').outerHeight(true);
			var ratio = height1/height2;
			if(ratio>=.896749){
				$('.page-footer').addClass('abs');
			}
			else{
				$('.page-footer').removeClass('abs');
			}
		}
		$('.portlet-body').resize(function () {
			//footerset();
		});
		$(window).resize(function () {
			//footerset();
		});
		$(function (){	
			//footerset();
		});
	</script>
	<%@ include file="../skeleton/includes.jsp"%>
	<%
	String[] helpers = request.getParameterValues("helpers");
	for(int i = 0; ArrayUtils.isNotEmpty(helpers)&& i < helpers.length;i++)
	{
	%>
		<jsp:include page="<%=helpers[i] %>" flush="true">
			<jsp:param name="helper" value="<%=i %>" />
		</jsp:include>
	<% } %>
<%
String[] jsStr = request.getParameterValues("js");
for(int i = 0; ArrayUtils.isNotEmpty(jsStr)&& i < jsStr.length;i++)
{
%>
<script src="${context}<%=jsStr[i]%>" type="text/javascript"></script>
<%
}
%>
</body>
</html>