<!DOCTYPE html>
<%@page import="user.*"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@page contentType="text/html;charset=utf-8" %>

<%
	String context = "../../.."  + request.getContextPath() + "/";
	String pluginsContext = context +"assets/global/plugins/";   
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext",pluginsContext);   

	List<Integer> menuIDPath = (List<Integer>)request.getAttribute("menuIDPath");
	if(menuIDPath == null){
		menuIDPath = new ArrayList<Integer>();
	}
	
	LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getInstance().getUserDTOByUserID(loginDTO.userID);
	
	String pageTitle="";
	
	if(menuIDPath.isEmpty()){
		pageTitle = ((HttpServletRequest)request).getRequestURL().toString();
		pageTitle=(userDTO==null||userDTO.languageID==CommonConstant.Language_ID_English?"Dashboard":"ড্যাশবোর্ড");;
	}else{

		int currentMenuID = menuIDPath.get(menuIDPath.size()-1);
		MenuDTO currentMenu = MenuRepository.getMenuDTOByMenuID(currentMenuID);
		pageTitle=(userDTO==null||userDTO.languageID==CommonConstant.Language_ID_English?currentMenu.menuName:currentMenu.menuNameBangla);
	}
	
%>

<html lang="en" style="zoom:80%;height: 100%">


<head>

<link rel="icon" href="<%=request.getContextPath()%>/images/favicon-icon.png">
<!-- <html:base /> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" type="image/x-icon" href="${context}favicon.ico" />
<title><%=pageTitle %></title>
<%@ include file="../skeleton/head.jsp"%>
<%
String[] cssStr = request.getParameterValues("css");
for(int i = 0; ArrayUtils.isNotEmpty(cssStr) &&i < cssStr.length;i++)
{
	%>
<link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css" />
	<%
}
%>

<script type="text/javascript">
 var context = '${context}';
 var pluginsContext = '${pluginsContext}';
</script>
</head>
<!-- END HEAD -->
<%
String  fullMenu="'";


		

for(int i = 0;i<menuIDPath.size();i++){
	
	int menuID = menuIDPath.get(i);
	if(i!=0){
		fullMenu+="','";
	}
	fullMenu+=menuID;
}

	fullMenu+="'";
	int j;
%>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo"  style="background: #e8fff3;min-height:100%;
    background-image: url(<%=request.getContextPath()%>/assets/r/pcc/500/files/static/v38/body_bg.png_%3b%20filename_%3dutf-8%27%27body_bg.png);background-repeat: repeat;display:flex;flex-direction:column;" onload="activateMenu(<%=fullMenu%>)">
	<!-- BEGIN HEADER -->
	<div id="fakeLoader"></div>
	
	<div class="page-header navbar navbar-fixed-top" style="    background: linear-gradient(-145deg, rgba(255,255,255,0.0)10%, rgba(255,255,255,0.2)20%, rgba(19,27,67,1)70%), linear-gradient(145deg, rgba(150,150,255,0.0)80%, rgba(60,111,162,0.8)20%), linear-gradient(125deg, rgba(255,255,255,0.0)70%, rgba(60,111,190,0.7)30%), linear-gradient(-200deg, rgba(60,111,180,0.9)60%, rgba(60,111,190,0.8)40%);">
		<!-- BEGIN HEADER INNER -->
		<%@ include file="../skeleton/header.jsp"%>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container" style="flex-grow: 1">
		<!-- BEGIN SIDEBAR -->
		
			<%@ include file="../skeleton/menu.jsp"%>
		

		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<%-- <jsp:include page='flushActionStatus.jsp' /> --%>
				<jsp:include page='<%=request.getParameter("body")%>' />
				<!-- END PAGE BASE CONTENT -->
			</div>
			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->
	</div>

	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<%@ include file="../skeleton/footer.jsp"%>
	<!-- END FOOTER -->
	<%@ include file="../skeleton/includes.jsp"%>
	<%
	String[] helpers = request.getParameterValues("helpers");
	for(int i = 0; ArrayUtils.isNotEmpty(helpers)&& i < helpers.length;i++)
	{
	%>
		<jsp:include page="<%=helpers[i] %>" flush="true">
			<jsp:param name="helper" value="<%=i %>" />
		</jsp:include>
	<% } %>
<%
String[] jsStr = request.getParameterValues("js");
for(int i = 0; ArrayUtils.isNotEmpty(jsStr)&& i < jsStr.length;i++)
{
%>
<script src="${context}<%=jsStr[i]%>" type="text/javascript"></script>
<%
}
%>
</body>
</html>